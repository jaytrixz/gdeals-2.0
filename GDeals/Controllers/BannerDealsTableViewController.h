//
//  BannerDealsTableViewController.h
//  GDeals
//
//  Created by Jay Santos on 2/10/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Banner.h"

@interface BannerDealsTableViewController : UITableViewController

@property (strong, nonatomic) Banner *bannerModel;

@end
