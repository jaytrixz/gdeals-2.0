//
//  PreferencesViewController.m
//  GDeals
//
//  Created by Jay Santos on 1/28/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "PreferencesViewController.h"

#import "Constants.h"
#import "UIManager.h"
#import "APIManager.h"
#import "InfoManager.h"
#import "SVProgressHUD.h"
#import "InterestCollectionViewCell.h"

@interface PreferencesViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuBarButtonItem;
@property (weak, nonatomic) IBOutlet UICollectionView *interestsCollection;

@property (strong, nonatomic) NSArray *interestsArray;
@property (strong, nonatomic) NSArray *interestNormalImagesArray;
@property (strong, nonatomic) NSArray *interestSelectedImagesArray;

@property (strong, nonatomic) NSMutableArray *defaultInterestsArray;
@property (strong, nonatomic) NSMutableArray *updatedInterestsArray;

@end

@implementation PreferencesViewController

static NSString * const reuseIdentifier = @"InterestCell";

- (void)viewWillAppear:(BOOL)animated {
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configureView];
    
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    self.navigationItem.titleView = [[UIManager sharedManager] formatNavigationBarWithNavigationBar:self.navigationController.navigationBar andTitleText:self.navigationController.title];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.delegate = self;
    if (revealViewController) {
        [_menuBarButtonItem setTarget:self.revealViewController];
        [_menuBarButtonItem setAction:@selector(revealToggle:)];
        [self.navigationController.navigationBar addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}
- (void)customSetup {
    self.interestsArray = [[UIManager sharedManager] loadDefaultInterests];
    self.interestNormalImagesArray = [[UIManager sharedManager] loadInterestsNormalButtonImages];
    self.interestSelectedImagesArray = [[UIManager sharedManager] loadInterestsSelectedButtonImages];
    
    [SVProgressHUD setFont:kGlobeFontRegular14];
    [SVProgressHUD setForegroundColor:kGlobeBlueColor];
    [SVProgressHUD showWithStatus:kHUDLoadingPreferences maskType:SVProgressHUDMaskTypeGradient];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        self.updatedInterestsArray = [NSMutableArray array];
        
        self.defaultInterestsArray = [[InfoManager sharedManager] getDefaultInterests];
        [[APIManager sharedManager] getUserInterests:^(NSArray *userInterestsArray) {
            for (NSString *defaultInterestName in userInterestsArray) {
                if ([self.defaultInterestsArray containsObject:defaultInterestName]) {
                    [self.updatedInterestsArray addObject:defaultInterestName];
                }
            }
            
            [_interestsCollection reloadData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        } failure:^(NSError *error) {
            NSLog(@"Error getting user interests with error: %@", error);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
            });
        }];
    });
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)updateSelectedInterests {
    NSString *joinedInterests = [_updatedInterestsArray componentsJoinedByString:@","];
    
    [[APIManager sharedManager] updateUserInterests:joinedInterests success:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showSuccessWithStatus:@"Preferences Saved" maskType:SVProgressHUDMaskTypeGradient];
        });
    } failure:^(NSError *error) {
        NSLog(@"Error: %@", error);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
        });
    }];
}

#pragma mark - Button methods

- (IBAction)doneButtonTapped:(UIBarButtonItem *)sender {
    [SVProgressHUD setFont:kGlobeFontRegular14];
    [SVProgressHUD setForegroundColor:kGlobeBlueColor];
    [SVProgressHUD showWithStatus:kHUDUpdatingPreferences maskType:SVProgressHUDMaskTypeGradient];
    
    [self updateSelectedInterests];
}

- (void)interestButtonTapped:(UIButton *)button {
    NSIndexPath *indexPath = [_interestsCollection indexPathForCell:(UICollectionViewCell *)[button superview]];
    
    if (button.selected) {
        button.selected = NO;
        
        NSLog(@"%@ interest is not tapped", _interestsArray[indexPath.row]);
        [self.updatedInterestsArray removeObject:[_interestsArray[indexPath.row] lowercaseString]];
    } else {
        button.selected = YES;
        
        NSLog(@"%@ interest is tapped", _interestsArray[indexPath.row]);
        [self.updatedInterestsArray addObject:[_interestsArray[indexPath.row] lowercaseString]];
    }
}

#pragma mark - UICollectionView methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_interestsArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    InterestCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    cell.interestButton.layer.cornerRadius = 45;
    cell.interestButton.clipsToBounds = YES;
    
    NSLog(@"Loaded interest: %@", _interestNormalImagesArray[indexPath.row]);
    [cell.interestButton setBackgroundImage:[UIImage imageNamed:_interestNormalImagesArray[indexPath.row]] forState:UIControlStateNormal];
    [cell.interestButton setBackgroundImage:[UIImage imageNamed:_interestSelectedImagesArray[indexPath.row]] forState:UIControlStateHighlighted];
    [cell.interestButton setBackgroundImage:[UIImage imageNamed:_interestSelectedImagesArray[indexPath.row]] forState:UIControlStateSelected];
    
    [cell.interestButton addTarget:self action:@selector(interestButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if ([_updatedInterestsArray containsObject:_defaultInterestsArray[indexPath.row]]) {
        cell.interestButton.selected = YES;
    } else {
        cell.interestButton.selected = NO;
    }
    
    return cell;
}

#pragma mark - SWRevealViewController methods

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:    (FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:    (FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
