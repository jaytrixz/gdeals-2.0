//
//  VoucherTableViewCell.h
//  GDeals
//
//  Created by Jay Santos on 1/28/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoucherTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *countView;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@property (weak, nonatomic) IBOutlet UIImageView *iconImage;

@end
