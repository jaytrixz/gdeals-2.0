//
//  PaymentOverviewTableViewCell.h
//  GDeals
//
//  Created by Jay Santos on 2/6/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentOverviewTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIImageView *dealImage;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;

@end
