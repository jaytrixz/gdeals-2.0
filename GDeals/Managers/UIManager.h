//
//  UIManager.h
//  GDeals
//
//  Created by Jay Santos on 11/6/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIManager : NSObject

+ (instancetype)sharedManager;

- (UITextView *)formatTermsAndConditionsWithTextView:(UITextView *)textView;

- (UIView *)formatNavigationBarWithLogoAndNavigationBar:(UINavigationBar *)navigationBar
                                           andTitleText:(NSString *)titleText;

- (UIView *)formatNavigationBarWithNavigationBar:(UINavigationBar *)navigationBar
                                    andTitleText:(NSString *)titleText;

- (NSArray *)loadInterestsNormalButtonImages;

- (NSArray *)loadInterestsSelectedButtonImages;

- (NSArray *)loadDefaultInterests;

- (UIImageView *)addGradientToImage:(UIImageView *)image;

- (UIImage *)createWhitePlaceholderImageWithImage:(UIImageView *)imageView;

- (UIImage *)applyIconImageWithVoucherName:(NSString *)name;

- (UIImage *)getCategoryIconWithCategoryIdentifier:(NSString *)identifier;

@end
