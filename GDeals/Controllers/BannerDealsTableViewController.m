//
//  BannerDealsTableViewController.m
//  GDeals
//
//  Created by Jay Santos on 2/10/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "BannerDealsTableViewController.h"

#import "Deal.h"
#import "UIManager.h"
#import "Constants.h"
#import "APIManager.h"
#import "InfoManager.h"
#import "SVProgressHUD.h"
#import "SVPullToRefresh.h"
#import "MainTableViewCell.h"
#import "EmptyTableViewCell.h"
#import "DealDetailsViewController.h"

@interface BannerDealsTableViewController () {
    BOOL isTableEmpty;
}

@property (strong, nonatomic) Deal *dealModel;

@property (strong, nonatomic) NSMutableArray *seasonalBannerDealsArray;

@end

@implementation BannerDealsTableViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configureView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    NSLog(@"Banner deals loaded");
    
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:169/255.0 green:220/255.0 blue:247/255.0 alpha:1.0];
}

- (void)customSetup {
    self.seasonalBannerDealsArray = [NSMutableArray array];
    
    isTableEmpty = NO;
    
    // Needed to call private methods inside of blocks
    __weak BannerDealsTableViewController *weakSelf = self;
    
    [self.tableView addPullToRefreshWithActionHandler:^{
        [weakSelf loadAllBannerDeals];
    }];
    
    [self.tableView.pullToRefreshView setTitle:@"Loading Deals" forState:SVPullToRefreshStateLoading];
    [self.tableView.pullToRefreshView setTitle:@"Pull to Refresh Deals" forState:SVPullToRefreshStateStopped];
    [self.tableView.pullToRefreshView setTitle:@"Release to Refresh Deals" forState:SVPullToRefreshStateTriggered];
    [self.tableView.pullToRefreshView setTextColor:kGlobeBlueColor];
    [self.tableView.pullToRefreshView setActivityIndicatorViewColor:kGlobeBlueColor];
    [self.tableView.pullToRefreshView setArrowColor:kGlobeBlueColor];
    
    [self.tableView triggerPullToRefresh];
}

- (void)loadAllBannerDeals {
    NSDictionary *parameters = @{@"action": @"00", @"page_legend": @"LP", @"strip_html_tags": @"1", @"page": @"0", @"flag": self.bannerModel.name};
    [[APIManager sharedManager] getSeasonalBannerDealsWithParameters:parameters success:^(NSDictionary *dealsDictionary) {
        NSLog(@"Deals dictionary: %@", dealsDictionary);
        
        isTableEmpty = NO;
        
        self.seasonalBannerDealsArray = [[InfoManager sharedManager] setupDealsArrayWithDealDictionary:dealsDictionary];
        [self.tableView reloadData];
        [self.tableView.pullToRefreshView stopAnimating];
    } failure:^(NSError *error) {
        [self.tableView.pullToRefreshView stopAnimating];
        
        isTableEmpty = YES;
        
        [self.tableView reloadData];
        
        NSLog(@"Error loading seasonal deals with error: %@", error);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
        });
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (isTableEmpty) {
        return 1;
    } else {
        return [self.seasonalBannerDealsArray count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isTableEmpty) {
        EmptyTableViewCell *cell = (EmptyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"EmptyCell" forIndexPath:indexPath];
        [self configureEmptyCell:cell atIndexPath:indexPath];
        
        return cell;
    } else {
        MainTableViewCell *cell = (MainTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MainCell" forIndexPath:indexPath];
        [self configureMainTableCell:cell atIndexPath:indexPath];
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isTableEmpty) {
        return 500;
    } else {
        return kMainBannerRowHeight;
    }
}

#pragma mark - Cell methods

- (void)configureEmptyCell:(EmptyTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.titleLabel.text = kErrorNoInternet;
    
    // Animates the table cell
    dispatch_async(dispatch_get_main_queue(), ^{
        CALayer *animationLayer = cell.layer;
        animationLayer.transform = CATransform3DMakeTranslation(0, animationLayer.bounds.size.height/3, 0.0f);
        
        NSTimeInterval animationDuration = 0.5;
        [UIView beginAnimations:nil
                        context:nil];
        
        [UIView setAnimationDuration:animationDuration];
        cell.layer.transform = CATransform3DIdentity;
        cell.layer.opacity = 1.0f;
        
        [UIView commitAnimations];
    });
}

- (void)configureMainTableCell:(MainTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    self.dealModel = [[Deal alloc] init];
    self.dealModel = self.seasonalBannerDealsArray[indexPath.row];
    
    if ([self.dealModel.source isEqualToString:@"globe_rewards"]) {
        self.dealModel.originalPrice = @"Min. Globe Rewards";
        self.dealModel.discountedPrice = [NSString stringWithFormat:@"%@ Points", self.dealModel.minimumRewardsPoints];
        
        cell.originalPriceLabel.text = self.dealModel.originalPrice;
        cell.discountedPriceLabel.text = self.dealModel.discountedPrice;
    } else {
        self.dealModel.originalPrice = [[InfoManager sharedManager] formatStringToPrice:self.dealModel.originalPrice];
        self.dealModel.discountedPrice = [[InfoManager sharedManager] formatStringToPrice:self.dealModel.discountedPrice];
        
        cell.originalPriceLabel.attributedText = [[InfoManager sharedManager] formatOriginalPrice:self.dealModel.originalPrice withLineColor:[UIColor whiteColor]];
        cell.discountedPriceLabel.text = [NSString stringWithFormat:@"₱ %@", self.dealModel.discountedPrice];
    }
    
    
//    // removes gradient sublayer to prevent overlapping
//    cell.dealImage.layer.sublayers = nil;
    
    // create white image as placeholder image
    cell.dealImage.image = [[UIManager sharedManager] createWhitePlaceholderImageWithImage:cell.dealImage];
    cell.dealImage.tag = indexPath.row;
    
    [[APIManager sharedManager] loadImageWithImage:cell.dealImage andImageURL:self.dealModel.imageURL success:^(id image) {
        if ([image isKindOfClass:[UIImage class]]) {
            self.dealModel.cachedImage = image;
            cell.dealImage.image = self.dealModel.cachedImage;
        }
    } failure:^(NSError *error) {
        NSLog(@"Error loading image with error: %@", error);
    }];
    
    if ([self.dealModel.source isEqualToString:@"globe_rewards"]) {
        cell.discountLabel.hidden = YES;
        cell.ribbonImage.hidden = YES;
    } else {
        cell.discountLabel.hidden = NO;
        cell.ribbonImage.hidden = NO;
        cell.discountLabel.text = [NSString stringWithFormat:@"save %@%@", self.dealModel.discount, @"%"];
    }
    
    cell.titleLabel.text = self.dealModel.name;
    cell.subtitleLabel.text = self.dealModel.sourceText;
    
    // Animates the table cell
    dispatch_async(dispatch_get_main_queue(), ^{
        CALayer *animationLayer = cell.layer;
        animationLayer.transform = CATransform3DMakeTranslation(0, animationLayer.bounds.size.height/3, 0.0f);
        
        NSTimeInterval animationDuration = 0.5;
        [UIView beginAnimations:nil
                        context:nil];
        
        [UIView setAnimationDuration:animationDuration];
        cell.layer.transform = CATransform3DIdentity;
        cell.layer.opacity = 1.0f;
        
        [UIView commitAnimations];
    });
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"ShowBannerDealDetails"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        MainTableViewCell *cell = sender;
        
        self.dealModel = [[Deal alloc] init];
        self.dealModel = self.seasonalBannerDealsArray[indexPath.row];
        self.dealModel.cachedImage = cell.dealImage.image;
        
        [[segue destinationViewController] setDealModel:self.dealModel];
    }
}

@end
