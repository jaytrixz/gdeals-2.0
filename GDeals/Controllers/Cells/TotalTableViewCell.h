//
//  TotalTableViewCell.h
//  GDeals
//
//  Created by Jay Santos on 2/6/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TotalTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

@end
