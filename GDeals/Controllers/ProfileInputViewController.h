//
//  ProfileInputViewController.h
//  GDeals
//
//  Created by Jay Santos on 10/15/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface ProfileInputViewController : UIViewController <FBLoginViewDelegate, UITextFieldDelegate>

@property (assign, nonatomic) BOOL isLogged;

@property (strong, nonatomic) NSString *mobileNumberString;


@end
