//
//  GlobeRewardsThankYouViewController.h
//  GDeals
//
//  Created by Jay Santos on 2/25/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Deal.h"

@interface GlobeRewardsThankYouViewController : UIViewController

@property (strong, nonatomic) Deal *dealModel;

@end
