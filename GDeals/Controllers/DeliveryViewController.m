//
//  DeliveryViewController.m
//  GDeals
//
//  Created by Jay Santos on 2/5/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "DeliveryViewController.h"

#import "Flurry.h"
#import "Payment.h"
#import "Constants.h"
#import "APIManager.h"
#import "SVProgressHUD.h"
#import "SVPullToRefresh.h"
#import "CoreDataManager.h"
#import "EmptyTableViewCell.h"
#import "DeliveryTableViewCell.h"
#import "BrowserViewController.h"
#import "NoAddressTableViewCell.h"

@interface DeliveryViewController () {
    BOOL isTableEmpty;
    BOOL isAddressEmpty;
}

@property (weak, nonatomic) IBOutlet UITableView *addressTable;

@property (strong, nonatomic) NSString *fullnameString;

@end

@implementation DeliveryViewController

- (void)viewWillAppear:(BOOL)animated {
    [self.addressTable reloadData];
    
    [self customSetup];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    // Needed to call private methods inside of blocks
    __weak DeliveryViewController *weakSelf = self;
    
    [self.addressTable addPullToRefreshWithActionHandler:^{
        [weakSelf loadDeliveryAdresses];
    }];
    
    [self.addressTable.pullToRefreshView setTitle:@"Loading Addresses" forState:SVPullToRefreshStateLoading];
    [self.addressTable.pullToRefreshView setTitle:@"Pull to Refresh Addresses" forState:SVPullToRefreshStateStopped];
    [self.addressTable.pullToRefreshView setTitle:@"Release to Refresh Addresses" forState:SVPullToRefreshStateTriggered];
    [self.addressTable.pullToRefreshView setTextColor:kGlobeBlueColor];
    [self.addressTable.pullToRefreshView setActivityIndicatorViewColor:kGlobeBlueColor];
    [self.addressTable.pullToRefreshView setArrowColor:kGlobeBlueColor];
}
- (void)customSetup {
    NSDictionary *userDictionary = [[CoreDataManager sharedManager] fetchCurrentUserDictionary];
//    NSLog(@"userDictionary: %@", userDictionary);
    
    isTableEmpty = NO;
    
    self.fullnameString = [NSString stringWithFormat:@"%@ %@", userDictionary[@"result"][@"user"][@"fname"], userDictionary[@"result"][@"user"][@"lname"]];
    
    [self.addressTable triggerPullToRefresh];
}

- (void)loadDeliveryAdresses {
    [[APIManager sharedManager] getUserAddress:^(NSArray *addressArray) {
        isTableEmpty = NO;
        
        NSLog(@"addressesArray: %@", addressArray);
        if ([addressArray count] == 0) {
            isAddressEmpty = YES;
        } else {
            isAddressEmpty = NO;
            
            self.addressesArray = addressArray;
        }
        
        [self.addressTable reloadData];
        [self.addressTable.pullToRefreshView stopAnimating];
    } failure:^(NSError *error) {
        [self.addressTable.pullToRefreshView stopAnimating];
        
        isTableEmpty = YES;
        
        NSLog(@"Error getting user delivery addresses with error: %@", error);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
        });
    }];
}

#pragma mark - Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isTableEmpty || isAddressEmpty) {
        return 1;
    } else {
        return [self.addressesArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isAddressEmpty) {
        NoAddressTableViewCell *cell = (NoAddressTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"NoAddressCell" forIndexPath:indexPath];
        [self configureNoAddressCell:cell atIndexPath:indexPath];
        
        return cell;
    } else if (isTableEmpty) {
        EmptyTableViewCell *cell = (EmptyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"EmptyCell" forIndexPath:indexPath];
        [self configureEmptyCell:cell atIndexPath:indexPath];
        
        return cell;
    } else {
        DeliveryTableViewCell *cell = (DeliveryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"DeliveryCell" forIndexPath:indexPath];
        [self configureDeliveryCell:cell atIndexPath:indexPath];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!isTableEmpty || !isAddressEmpty) {
        DeliveryTableViewCell *cell = (DeliveryTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        if (cell.contentView.backgroundColor == [UIColor whiteColor]) {
            cell.contentView.backgroundColor = kGlobeLightBlueColor;
        }
        
        NSArray *tableCells = [tableView visibleCells];
        
        for (DeliveryTableViewCell *deliveryCell in tableCells) {
            if (deliveryCell.tag != indexPath.row) {
                deliveryCell.contentView.backgroundColor = [UIColor whiteColor];
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
        return UITableViewAutomaticDimension;
    }
    
    if (isTableEmpty || isAddressEmpty) {
        return 500;
    } else {
        return 44;
    }
}

#pragma mark - Cell methods

- (void)configureNoAddressCell:(NoAddressTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.titleLabel.text = kErrorNoAddressesFound;
}

- (void)configureEmptyCell:(EmptyTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.titleLabel.text = kErrorNoInternet;
}

- (void)configureDeliveryCell:(DeliveryTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *addressDictionary = self.addressesArray[indexPath.row];
    
    cell.tag = indexPath.row;
    cell.nameLabel.text = self.fullnameString;
    cell.addressLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@", addressDictionary[@"building_name"], addressDictionary[@"street"], addressDictionary[@"village"], addressDictionary[@"postal"], addressDictionary[@"city"], addressDictionary[@"state"], addressDictionary [@"country"]];
    
    if ([addressDictionary[@"is_default"] isEqualToString:@"1"]) {
        [self.addressTable selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    NSIndexPath *indexPath = [self.addressTable indexPathForSelectedRow];
    
    if ([segue.identifier isEqualToString:@"ShowNewAddress"]) {
        // Added flurry here since bar button is wired in interface builder without code except this
        NSDictionary *detailParams = @{kFlurryTapped: kFlurryAddNew};
        [Flurry logEvent:kFlurryDeliveryAddress withParameters:detailParams];
    } else if ([segue.identifier isEqualToString:@"ShowBrowser"]) {
        // Added flurry here since bar button is wired in interface builder without code except this
        NSDictionary *detailParams = @{kFlurryTapped: kFlurryCheckout};
        [Flurry logEvent:kFlurryDeliveryAddress withParameters:detailParams];
        
        NSLog(@"Index selected: %ld", (long)indexPath.row);
        self.paymentModel.deliveryAddressIdentifier = self.addressesArray[indexPath.row][@"id"];
        
//        NSLog(@"Payment: %@", self.paymentModel);
//        NSLog(@"Price: %@", self.paymentModel.price);
//        NSLog(@"Quantity: %@", self.paymentModel.quantity);
//        NSLog(@"Delivery Address ID: %@", self.paymentModel.deliveryAddressIdentifier);
        [segue.destinationViewController setDealModel:self.dealModel];
        [segue.destinationViewController setPaymentModel:self.paymentModel];
    } else {
        // Pass nothing. Saved for future scenarios aside from going to new address page or browser page.
    }
}

@end
