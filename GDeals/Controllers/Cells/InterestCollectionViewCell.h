//
//  InterestCollectionViewCell.h
//  GDeals
//
//  Created by Jay Santos on 11/12/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InterestCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *interestButton;

@end
