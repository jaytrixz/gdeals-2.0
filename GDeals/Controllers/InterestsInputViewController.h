//
//  InterestsInputViewController.h
//  GDeals
//
//  Created by Jay Santos on 10/15/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface InterestsInputViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@end
