//
//  QRCodeScannerViewController.m
//  GDeals
//
//  Created by Jay Santos on 2/6/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "QRCodeScannerViewController.h"

#import "Constants.h"
#import "APIManager.h"
#import "SVProgressHUD.h"
#import "PointsInputViewController.h"

@interface QRCodeScannerViewController () {
    BOOL hasAlreadyScanned;
}

@property (weak, nonatomic) IBOutlet UIView *scannerView;

@property (strong, nonatomic) NSString *qrCodeString;

@property (strong, nonatomic) AVCaptureSession *captureSession;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer *videoPreviewLayer;

@end

@implementation QRCodeScannerViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self customSetup];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    // Need to implement in order for AVCapture to be within bounds of scanner view
    self.videoPreviewLayer.frame = _scannerView.layer.bounds;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)customSetup {
    self.captureSession = nil;
    
    [self scanQRCode];
}

- (void)scanQRCode {
    NSError *error;
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    if (!input) {
        NSLog(@"Error scanning QR code with error: %@", error);
    } else {
        self.captureSession = [[AVCaptureSession alloc] init];
        [self.captureSession addInput:input];
        
        AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
        [self.captureSession addOutput:captureMetadataOutput];
        
        dispatch_queue_t dispatchQueue;
        dispatchQueue = dispatch_queue_create("QRScanQueue", nil);
        [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
        [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
        
        self.videoPreviewLayer = [AVCaptureVideoPreviewLayer layerWithSession:_captureSession];
        self.videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        self.videoPreviewLayer.frame = _scannerView.layer.bounds;
        [self.scannerView.layer addSublayer:_videoPreviewLayer];
        
        [self.captureSession startRunning];
    }
}

- (void)stopScanning {
    [self.captureSession stopRunning];
    _captureSession = nil;
    
    [_videoPreviewLayer removeFromSuperlayer];
}

- (void)useGlobeRewardsWithCode:(NSString *)code {
    NSDictionary *parameters = [NSDictionary dictionary];
    parameters = @{@"points": self.dealModel.minimumRewardsPoints, @"qr_code": code, @"deal_id": self.dealModel.identifier, @"latitude": self.dealModel.latitude, @"longitude": self.dealModel.longitude};
    
    [[APIManager sharedManager] useGlobeRewardsPointsWithParameters:parameters success:^(NSString *result) {
        NSLog(@"Result: %@", result);
        
        if ([result isEqualToString:@"Success"]) {
            [self performSegueWithIdentifier:@"ShowGlobeRewardsThankYou" sender:nil];
        }
    } failure:^(NSError *error) {
        NSLog(@"Error using Globe rewards points with error: %@", error);
        
        [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
    }];
}

#pragma mark - Button methods

- (IBAction)cancelButtonTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - AVCapture methods

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection {
    if (metadataObjects != nil && [metadataObjects count] != 0) {
        AVMetadataMachineReadableCodeObject *metadataObject = [metadataObjects objectAtIndex:0];
        if ([[metadataObject type] isEqualToString:AVMetadataObjectTypeQRCode]) {
//            [_lblStatus performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
            
            self.qrCodeString = [metadataObject stringValue];
            NSLog(@"QR Code Result: %@", self.qrCodeString);
            
            [self performSelectorOnMainThread:@selector(stopScanning) withObject:nil waitUntilDone:NO];
            
            if (!hasAlreadyScanned) {
                hasAlreadyScanned = YES;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self performSegueWithIdentifier:@"ShowPointsInput" sender:nil];
                });
            }
            
        }
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"ShowPointsInput"]) {
        [segue.destinationViewController setDealModel:self.dealModel];
        [segue.destinationViewController setQrCodeString:self.qrCodeString];
        [segue.destinationViewController setRewardsPointsString:self.rewardsPointsString];
        [segue.destinationViewController setLatitudeString:self.latitudeString];
        [segue.destinationViewController setLongitudeString:self.longitudeString];
    }
}

@end
