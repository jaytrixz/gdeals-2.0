//
//  Deal.h
//  GDeals
//
//  Created by Jay Santos on 11/12/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Deal : NSObject

@property (strong, nonatomic) id cachedImage;

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *terms;
@property (strong, nonatomic) NSString *source;
@property (strong, nonatomic) NSString *endDate;
@property (strong, nonatomic) NSString *imageURL;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *discount;
@property (strong, nonatomic) NSString *quantity;
@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *startDate;
@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) NSString *merchantId;
@property (strong, nonatomic) NSString *perkPoints;
@property (strong, nonatomic) NSString *sourceText;
@property (strong, nonatomic) NSString *branchName;
@property (strong, nonatomic) NSString *companyName;
@property (strong, nonatomic) NSString *pointsExpiry;
@property (strong, nonatomic) NSString *originalPrice;
@property (strong, nonatomic) NSString *purchaseCount;
@property (strong, nonatomic) NSString *endRedemption;
@property (strong, nonatomic) NSString *bannerImageURL;
@property (strong, nonatomic) NSString *discountedPrice;
@property (strong, nonatomic) NSString *descriptionText;
@property (strong, nonatomic) NSString *startRedemption;
@property (strong, nonatomic) NSString *minimumRewardsPoints;

@property (strong, nonatomic) NSString *categoryId;
@property (strong, nonatomic) NSString *categoryIdNew;

@property (strong, nonatomic) NSDictionary *variantsDictionary;
@property (strong, nonatomic) NSDictionary *subVariantsDictionary;

@end
