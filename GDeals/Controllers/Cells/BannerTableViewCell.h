//
//  BannerTableViewCell.h
//  GDeals
//
//  Created by Jay Santos on 1/13/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BannerTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *bannerImage;

@end
