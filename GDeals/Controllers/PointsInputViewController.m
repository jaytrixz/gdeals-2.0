//
//  PointsInputViewController.m
//  GDeals
//
//  Created by Jay Santos on 2/26/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "PointsInputViewController.h"

#import "Constants.h"
#import "APIManager.h"
#import "SVProgressHUD.h"
#import "GlobeRewardsThankYouViewController.h"

@interface PointsInputViewController ()

@property (weak, nonatomic) IBOutlet UILabel *overviewLabel;
@property (weak, nonatomic) IBOutlet UILabel *branchNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *branchNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *branchAddressLabel;

@property (weak, nonatomic) IBOutlet UITextField *pointsTextField;

@end

@implementation PointsInputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button methods

- (IBAction)cancelButtonTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)confirmButtonTapped:(UIButton *)sender {
    if ([self.dealModel.minimumRewardsPoints integerValue] > [self.pointsTextField.text integerValue]) {
        [SVProgressHUD showErrorWithStatus:kErrorGlobeRewards maskType:SVProgressHUDMaskTypeGradient];
    } else if ([self.rewardsPointsString integerValue] < [self.pointsTextField.text integerValue]) {
        [SVProgressHUD showErrorWithStatus:kErrorInsufficientGlobeRewards maskType:SVProgressHUDMaskTypeGradient];
    } else {
        [SVProgressHUD showWithStatus:kHUDRedeemingPoints maskType:SVProgressHUDMaskTypeBlack];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self useGlobeRewardsWithCode:self.qrCodeString];
        });
    }
}

#pragma mark - Custom methods

- (void)customSetup {
    [SVProgressHUD showWithStatus:kHUDVerifyingData maskType:SVProgressHUDMaskTypeGradient];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self getQRCodeDetails];
    });
}

- (void)getQRCodeDetails {
    [[APIManager sharedManager] getQRCodeDetailsWithQRCode:self.qrCodeString success:^(NSDictionary *infoDictionary) {
        NSLog(@"infoDictionary: %@", infoDictionary);
        
        self.branchNameLabel.text = infoDictionary[@"name"];
        NSDictionary *addressDictionary = infoDictionary[@"address"];
        self.branchAddressLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@", addressDictionary[@"street_address"], addressDictionary[@"city"], addressDictionary[@"state"], addressDictionary[@"postcode"], addressDictionary[@"country"]];
        self.branchNumberLabel.text = infoDictionary[@"data"][@"delivery_no"][0];
        self.overviewLabel.text = [NSString stringWithFormat:@"You currently have: %@ Globe Rewards points.\n\n*Successful transactions may not be cancelled or reversed.", self.rewardsPointsString];
        NSLog(@"self.rewardsPointsString: %@", self.rewardsPointsString);
        self.pointsTextField.text = [NSString stringWithFormat:@"%@", self.dealModel.minimumRewardsPoints];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
    } failure:^(NSError *error) {
        NSLog(@"Failed to get QR code details with error: %@", error);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
        });
    }];
}

- (void)useGlobeRewardsWithCode:(NSString *)code {
    NSDictionary *parameters = [NSDictionary dictionary];
    parameters = @{@"points": self.pointsTextField.text, @"qr_code": code, @"deal_id": self.dealModel.identifier, @"latitude": self.latitudeString, @"longitude": self.longitudeString};
    
    [[APIManager sharedManager] useGlobeRewardsPointsWithParameters:parameters success:^(NSString *result) {
        NSLog(@"Result: %@", result);
        
        if ([result isEqualToString:@"Success"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                [self performSegueWithIdentifier:@"ShowGlobeRewardsThankYou" sender:nil];
            });
        }
    } failure:^(NSError *error) {
        NSLog(@"Error using Globe rewards points with error: %@", error);
        
        [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
    }];
}

#pragma mark - Text field methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger oldLength = 0;
    oldLength = [textField.text length];
    
    NSUInteger replacementLength = 0;
    replacementLength = [string length];
    
    NSUInteger rangeLength = 0;
    rangeLength = range.length;
    
    NSUInteger newLength = 0;
    newLength = oldLength - rangeLength + replacementLength;
    
    if (newLength < kMaximumNumberForGlobeRewardsPoints) {
        
    } else if (newLength >= kMaximumNumberForGlobeRewardsPoints) {
        
    }
    
    BOOL returnKey = NO;
    returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= kMaximumNumberForGlobeRewardsPoints || returnKey;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    
    [self confirmButtonTapped:nil];
    
    return YES;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ShowGlobeRewardsThankYou"]) {
        [segue.destinationViewController setDealModel:self.dealModel];
    }
}

@end
