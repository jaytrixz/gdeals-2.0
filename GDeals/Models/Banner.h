//
//  Banner.h
//  GDeals
//
//  Created by Jay Santos on 2/10/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Banner : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *imageURL;
@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) NSString *bannerIndex;

@end
