//
//  GlobeRewardsPaymentViewController.m
//  GDeals
//
//  Created by Jay Santos on 2/5/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "GlobeRewardsPaymentViewController.h"

#import "Constants.h"
#import "APIManager.h"
#import "InfoManager.h"
#import "BrowserViewController.h"
#import "DeliveryViewController.h"
#import "QRCodeScannerViewController.h"

@interface GlobeRewardsPaymentViewController ()

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;

@property (weak, nonatomic) IBOutlet UITextView *mechanicsText;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;

@property (strong, nonatomic) NSArray *addressesArray;

@property (strong, nonatomic) NSString *rewardsPointsString;

@end

@implementation GlobeRewardsPaymentViewController

- (void)viewWillAppear:(BOOL)animated {
    [self configureView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    [self formatTextView];
}

- (void)customSetup {
    [self.loadingIndicator startAnimating];
    
    self.addressesArray = [NSMutableArray array];
    
    [self fetchRewardsPoints];
}

- (void)formatTextView {
    NSString *mechanicsTextString = self.mechanicsText.text;
    NSString *allTextString = @"- Check the item that you want from their offers\n\n- Press Confirm to begin the transaction\n\n- Upon payment, you can Scan the store's assigned QR code to pay with your Globe Rewards points\n\n- Input the number of Globe Rewards points that you want to transfer";
    
    NSRange allTextRange = [mechanicsTextString rangeOfString:allTextString];
    NSRange checkRangeBold = [mechanicsTextString rangeOfString:@"Check"];
    NSRange confirmRangeBold = [mechanicsTextString rangeOfString:@"Confirm"];
    NSRange scanRangeBold = [mechanicsTextString rangeOfString:@"Scan"];
    NSRange inputRangeBold = [mechanicsTextString rangeOfString:@"Input"];
    
    UIFont *regularFontText = kGlobeFontRegular15;
    UIFont *boldFontText = kGlobeFontBold15;
    NSDictionary *dictionaryFontText = [NSDictionary dictionaryWithObjectsAndKeys:regularFontText, NSFontAttributeName, nil];
    NSDictionary *dictionaryBoldText = [NSDictionary dictionaryWithObjectsAndKeys:boldFontText, NSFontAttributeName, nil];
    
    NSMutableAttributedString *modifiedAttributedString = [[NSMutableAttributedString alloc] initWithString:mechanicsTextString];
    [modifiedAttributedString setAttributes:dictionaryFontText range:allTextRange];
    [modifiedAttributedString setAttributes:dictionaryBoldText range:checkRangeBold];
    [modifiedAttributedString setAttributes:dictionaryBoldText range:confirmRangeBold];
    [modifiedAttributedString setAttributes:dictionaryBoldText range:scanRangeBold];
    [modifiedAttributedString setAttributes:dictionaryBoldText range:inputRangeBold];
    
    [self.mechanicsText setAttributedText:modifiedAttributedString];
//    self.mechanicsText.editable = YES;
//    self.mechanicsText.font = kGlobeFontRegular15;
//    self.mechanicsText.editable = NO;
}

- (void)fetchRewardsPoints {
    self.dateLabel.text = [NSString stringWithFormat:@"as of %@", [[InfoManager sharedManager] getCurrentDate]];
    
    [[APIManager sharedManager] getGlobeRewardsPoints:^(NSString *points) {
        NSLog(@"Result: %@", points);
        self.rewardsPointsString = points;
        self.pointsLabel.text = [NSString stringWithFormat:@"%@ Points", self.rewardsPointsString];
        
        [self.loadingIndicator stopAnimating];
    } failure:^(NSError *error) {
        NSLog(@"Error getting rewards points with error: %@", error);
        
        [self.loadingIndicator stopAnimating];
    }];
}

#pragma mark - Button methods

- (IBAction)cancelButtonTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)confirmButtonTapped:(UIButton *)sender {
    if (![self.pointsLabel.text isEqualToString:kEmptyString]) {
        [self performSegueWithIdentifier:@"ShowQRCodeScanner" sender:nil];
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"ShowQRCodeScanner"]) {
        [segue.destinationViewController setDealModel:self.dealModel];
        [segue.destinationViewController setRewardsPointsString:self.rewardsPointsString];
        [segue.destinationViewController setLatitudeString:self.latitudeString];
        [segue.destinationViewController setLongitudeString:self.longitudeString];
    }
}

@end
