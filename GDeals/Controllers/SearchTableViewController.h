//
//  SearchTableViewController.h
//  GDeals
//
//  Created by Jay Santos on 2/13/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewController : UITableViewController

@property (strong, nonatomic) NSString *searchString;

@end
