//
//  NewAddressViewController.m
//  GDeals
//
//  Created by Jay Santos on 2/5/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "NewAddressViewController.h"

#import "Flurry.h"
#import "Constants.h"
#import "APIManager.h"
#import "SVProgressHUD.h"
#import "CityProvinceTableViewController.h"

@interface NewAddressViewController () <CityProvinceDelegate> {
    BOOL isCityProvinceSelected;
    UITextField *activeTextField;
}

@property (weak, nonatomic) IBOutlet UISwitch *defaultSwitch;

@property (weak, nonatomic) IBOutlet UITextField *zipTextField;
@property (weak, nonatomic) IBOutlet UITextField *stateTextField;
@property (weak, nonatomic) IBOutlet UITextField *streetTextField;
@property (weak, nonatomic) IBOutlet UITextField *villageTextField;
@property (weak, nonatomic) IBOutlet UITextField *buildingTextField;

@property (weak, nonatomic) IBOutlet UIScrollView *addressScrollview;

@end

@implementation NewAddressViewController

- (void)viewWillAppear:(BOOL)animated {
    [self registerForKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self deregisterFromKeyboardNotifications];
    
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configureView];
    
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    
}

- (void)customSetup {
    UITapGestureRecognizer *tapToHideKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard:)];
    [self.view addGestureRecognizer:tapToHideKeyboardGesture];
}

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)hideKeyboard:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
}


#pragma mark - City province methods

- (void)setCityProvinceWithCityProvince:(NSString *)cityProvince {
    self.stateTextField.text = cityProvince;
}

#pragma mark - Button methods

- (IBAction)saveButtonTapped:(UIBarButtonItem *)sender {
    NSDictionary *detailParams = @{kFlurryTapped: kFlurrySave};
    [Flurry logEvent:kFlurryAddNewDeliveryAddress withParameters:detailParams];
    
    [SVProgressHUD showWithStatus:kHUDAddingAddress maskType:SVProgressHUDMaskTypeGradient];
    
    [self.view endEditing:YES];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *switchValueString = nil;
        if ([self.defaultSwitch isOn]) {
            switchValueString = @"1";
        } else {
            switchValueString = @"0";
        }
        
//        NSDictionary *parameters = @{@"delivery_address_id": @"", @"building_name": self.buildingTextField.text, @"street": self.streetTextField.text, @"village": self.villageTextField.text, @"postal": self.zipTextField.text, @"city": self.stateTextField.text, @"is_default": switchValueString};
        NSDictionary *parameters = @{@"building_name": self.buildingTextField.text, @"street": self.streetTextField.text, @"village": self.villageTextField.text, @"postal": self.zipTextField.text, @"city": self.stateTextField.text, @"is_default": switchValueString};
        [[APIManager sharedManager] addNewUserAddressWithParameters:parameters success:^(NSString *result) {
            [SVProgressHUD showSuccessWithStatus:result maskType:SVProgressHUDMaskTypeGradient];
            
            [self.navigationController popViewControllerAnimated:YES];
        } failure:^(NSError *error) {
            NSLog(@"Error saving user address with error: %@", error);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
            });
        }];
    });
}

#pragma mark - Text field methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField.tag == 2) {
        if ([self.stateTextField.text isEqualToString:@""]) {
            isCityProvinceSelected = NO;
        } else {
            isCityProvinceSelected = YES;
        }
        
        if (!isCityProvinceSelected) {
            isCityProvinceSelected = YES;
            [self performSegueWithIdentifier:@"ShowCityProvince" sender:nil];
        }
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.tag == 1) {
        // this check prevents crashing when doing an undo
        if(range.length + range.location > textField.text.length) {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength > 4) {
            return NO;
        } else {
            return YES;
        }
    } else {
        return YES;
    }
}

- (void)keyboardWasShown:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect bkgndRect = activeTextField.superview.frame;
    bkgndRect.size.height += kbSize.height;
    [activeTextField.superview setFrame:bkgndRect];
    
//    CGFloat adjustedHeight = self.navigationController.navigationBar.frame.size.height + self.numberLabel.frame.size.height;
    
    [self.addressScrollview setContentOffset:CGPointMake(0.0, activeTextField.frame.origin.y - kbSize.height) animated:YES];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification *)aNotification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.addressScrollview.contentInset = contentInsets;
    self.addressScrollview.scrollIndicatorInsets = contentInsets;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    activeTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    activeTextField = nil;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"ShowCityProvince"]) {
        [segue.destinationViewController setCityProvinceDelegate:self];
    }
}

@end
