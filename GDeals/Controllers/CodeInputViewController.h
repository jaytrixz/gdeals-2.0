//
//  CodeInputViewController.h
//  GDeals
//
//  Created by Jay Santos on 10/15/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonHMAC.h>
#import <CommonCrypto/CommonDigest.h>

@interface CodeInputViewController : UIViewController

@property (strong, nonatomic) NSString *signatureString;
@property (strong, nonatomic) NSString *mobileNumberString;
@property (strong, nonatomic) NSString *verificationCodeString;

@end
