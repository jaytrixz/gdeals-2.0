//
//  Payment.h
//  GDeals
//
//  Created by Jay Santos on 2/20/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Payment : NSObject

@property (strong, nonatomic) NSString *price;
@property (strong, nonatomic) NSString *quantity;
@property (strong, nonatomic) NSString *thirdPartyDealIdentifier;
@property (strong, nonatomic) NSString *deliveryAddressIdentifier;

@end
