//
//  RecoTableViewCell.h
//  GDeals
//
//  Created by Jay Santos on 2/3/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UICollectionView *recoCollection;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;

@end
