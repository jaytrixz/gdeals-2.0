//
//  SelectionTableViewController.m
//  GDeals
//
//  Created by Jay Santos on 2/16/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "SelectionTableViewController.h"

#import "Constants.h"
#import "SelectTableViewCell.h"

@interface SelectionTableViewController () <SelectionDelegate>

@end

@implementation SelectionTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self configureView];
    
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    
}

- (void)customSetup {
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (self.participatingBranchesArray) {
        return [self.participatingBranchesArray count];
    } else {
        return [self.selectionsArray count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.participatingBranchesArray) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SelectionCell" forIndexPath:indexPath];
        [self configureTableCell:cell atIndexPath:indexPath];
        
        return cell;
    } else {
        SelectTableViewCell *cell = (SelectTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"SelectCell" forIndexPath:indexPath];
        [self configureSelectCell:cell atIndexPath:indexPath];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (!self.participatingBranchesArray) {
        if (self.isVariantSelected) {
            [self.selectionDelegate updateVariantWithNewVariant:self.selectionsArray[indexPath.row] andIndex:indexPath.row];
        } else {
            [self.selectionDelegate updateQuantityWithNewQuantity:[NSString stringWithFormat:@"%@", self.selectionsArray[indexPath.row]] andIndex:indexPath.row];
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - Cell methods

- (void)configureTableCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.textLabel.font = kGlobeFontRegular17;
    cell.detailTextLabel.font = kGlobeFontRegular14;
    cell.textLabel.text = self.participatingBranchesArray[indexPath.row][@"branch_name"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ km", self.participatingBranchesArray[indexPath.row][@"distance"]];
}

- (void)configureSelectCell:(SelectTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.textLabel.font = kGlobeFontRegular17;
    
    if (self.isVariantSelected) {
        cell.textLabel.text = [self.selectionsArray[indexPath.row] capitalizedString];
    } else {
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [self.selectionsArray[indexPath.row] capitalizedString]];
    }
    
    cell.detailTextLabel.text = kEmptyString;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
