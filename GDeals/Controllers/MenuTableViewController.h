//
//  MenuTableViewController.h
//  GDeals
//
//  Created by Jay Santos on 10/14/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface MenuTableViewController : UITableViewController <MFMailComposeViewControllerDelegate, UIAlertViewDelegate>

@end
