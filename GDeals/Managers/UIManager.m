//
//  UIManager.m
//  GDeals
//
//  Created by Jay Santos on 11/6/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import "UIManager.h"

#import "Constants.h"
#import "InfoManager.h"

@implementation UIManager

+ (instancetype)sharedManager {
    __strong static UIManager *sharedManager = nil;
    static dispatch_once_t token = 0;
    dispatch_once(&token, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

- (UITextView *)formatTermsAndConditionsWithTextView:(UITextView *)textView {
    NSMutableAttributedString *formattedTermsAndConditions = nil;
    formattedTermsAndConditions = [[NSMutableAttributedString alloc] initWithString:kTermsAndConditionsString];
    NSRange selectedRange = [[formattedTermsAndConditions string] rangeOfString:kTermsAndConditionsHyperLinkString];
    NSURL *termsURL = [NSURL URLWithString:kTermsAndConditionsLinkString];
    [formattedTermsAndConditions addAttribute:NSLinkAttributeName value:termsURL range:selectedRange];
    
    NSDictionary *linkAttributes = nil;
    linkAttributes = @{NSForegroundColorAttributeName:[UIColor colorWithRed:15/255.0 green:68/255.0 blue:160/255.0 alpha:1.0], NSUnderlineStyleAttributeName:@(NSUnderlinePatternSolid)};
    
    textView.linkTextAttributes = linkAttributes;
    textView.attributedText = formattedTermsAndConditions;
    textView.font = kGlobeFontRegular14;
    textView.textAlignment = NSTextAlignmentCenter;
    
    return textView;
}

- (UIView *)formatNavigationBarWithLogoAndNavigationBar:(UINavigationBar *)navigationBar
                                           andTitleText:(NSString *)titleText {
    navigationBar.translucent = NO;
    navigationBar.barTintColor = [UIColor colorWithRed:15/255.0 green:68/255.0 blue:160/255.0 alpha:1.0];
    
    UIView *headerView = [[UIView alloc] init];
    headerView.frame = CGRectMake(0, 0, 320, 44);
    
    UIImageView *logoImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_image"]];
    logoImage.frame = CGRectMake(10, 7, 30, 30);
    logoImage.layer.cornerRadius = 8.0;
    logoImage.clipsToBounds = YES;
    [headerView addSubview:logoImage];
    
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.frame = CGRectMake(45, 0, 100, 44);
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.text = titleText;
    nameLabel.textColor = [UIColor whiteColor];
    nameLabel.font = kGlobeFontRegular18;
    
    [headerView addSubview:nameLabel];
    
    return headerView;
}

- (UIView *)formatNavigationBarWithNavigationBar:(UINavigationBar *)navigationBar
                                    andTitleText:(NSString *)titleText {
    navigationBar.translucent = NO;
    navigationBar.barTintColor = [UIColor colorWithRed:15/255.0 green:68/255.0 blue:160/255.0 alpha:1.0];
    
    UIView *headerView = [[UIView alloc] init];
    headerView.frame = CGRectMake(0, 0, 320, 44);
    
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.frame = CGRectMake(0, 0, 150, 44);
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.text = titleText;
    nameLabel.textColor = [UIColor whiteColor];
    nameLabel.font = kGlobeFontRegular18;
    
    [headerView addSubview:nameLabel];
    
    return headerView;
}

- (NSArray *)loadInterestsNormalButtonImages {
    return @[@"fashion_interests_normal.png", @"home_interests_normal.png", @"electronics_interests_normal.png", @"automobile_interests_normal.png", @"lifestyle_interests_normal.png", @"beauty_interests_normal.png", @"dining_interests_normal.png", @"travel_interests_normal.png", @"kids_interests_normal.png"];
}

- (NSArray *)loadInterestsSelectedButtonImages {
    return @[@"fashion_interests_selected.png", @"home_interests_selected.png", @"electronics_interests_selected.png", @"automobile_interests_selected.png", @"lifestyle_interests_selected.png", @"beauty_interests_selected.png", @"dining_interests_selected.png", @"travel_interests_selected.png", @"kids_interests_selected.png"];
}

- (NSArray *)loadDefaultInterests {
    return @[@"Fashion", @"Home", @"Electronics", @"Automobile", @"Lifestyle", @"Beauty", @"Dining", @"Travel", @"Kids"];
}

- (UIImageView *)addGradientToImage:(UIImageView *)image {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = image.frame;
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColor clearColor].CGColor, (id)[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.35].CGColor, nil];
    [image.layer insertSublayer:gradient atIndex:0];
    
    return image;
}

- (UIImage *)createWhitePlaceholderImageWithImage:(UIImageView *)imageView {
    UIGraphicsBeginImageContext(imageView.image.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextFillRect(context, imageView.frame);
    UIImage *whiteImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return whiteImage;
}

- (UIImage *)applyIconImageWithVoucherName:(NSString *)name {
    UIImage *voucherIconImage = nil;
    if ([name isEqualToString:@"for_redemption"]) {
        voucherIconImage = [UIImage imageNamed:@"for_redemption_icon"];
    } else if ([name isEqualToString:@"for_delivery"]) {
        voucherIconImage = [UIImage imageNamed:@"for_delivery_icon"];
    } else if ([name isEqualToString:@"delivered"]) {
        voucherIconImage = [UIImage imageNamed:@"delivered_icon"];
    } else if ([name isEqualToString:@"redeemed"]) {
        voucherIconImage = [UIImage imageNamed:@"redeemed_icon"];
    } else if ([name isEqualToString:@"expired"]) {
        voucherIconImage = [UIImage imageNamed:@"expired_icon"];
    } else {
        voucherIconImage = [UIImage imageNamed:@"disapproved_icon"];
    }
    
    return voucherIconImage;
}

- (UIImage *)getCategoryIconWithCategoryIdentifier:(NSString *)identifier {
    NSString *categoryName = [[InfoManager sharedManager] getCategoryNameWithCategoryIdentifier:identifier];
    NSLog(@"categoryName: %@", categoryName);
    UIImage *categoryIconImage = nil;
    
    if ([categoryName isEqualToString:@"Fashion"]) {
        categoryIconImage = [UIImage imageNamed:@"fashion_category_icon"];
    } else if ([categoryName isEqualToString:@"Home"]) {
        categoryIconImage = [UIImage imageNamed:@"home_category_icon"];
    } else if ([categoryName isEqualToString:@"Electronic"]) {
        categoryIconImage = [UIImage imageNamed:@"electronics_category_icon"];
    } else if ([categoryName isEqualToString:@"Automobile"]) {
        categoryIconImage = [UIImage imageNamed:@"automobile_category_icon"];
    } else if ([categoryName isEqualToString:@"Lifestyle"]) {
        categoryIconImage = [UIImage imageNamed:@"lifestyle_category_icon"];
    } else if ([categoryName isEqualToString:@"Beauty"]) {
        categoryIconImage = [UIImage imageNamed:@"beauty_category_icon"];
    } else if ([categoryName isEqualToString:@"Dining"]) {
        categoryIconImage = [UIImage imageNamed:@"dining_category_icon"];
    } else if ([categoryName isEqualToString:@"Travel"]) {
        categoryIconImage = [UIImage imageNamed:@"travel_category_icon"];
    } else if ([categoryName isEqualToString:@"Kids"]) {
        categoryIconImage = [UIImage imageNamed:@"kids_category_icon"];
    } else {
        categoryIconImage = [UIImage imageNamed:@""];
    }
    
    return categoryIconImage;
}

@end


