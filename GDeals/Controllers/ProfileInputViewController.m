//
//  ProfileInputViewController.m
//  GDeals
//
//  Created by Jay Santos on 10/15/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import "ProfileInputViewController.h"

#import "Flurry.h"
#import "Constants.h"
#import "UIManager.h"
#import "APIManager.h"
#import "InfoManager.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"
#import "InterestsInputViewController.h"

@interface ProfileInputViewController () {
    UITextField *activeTextField;
}

@property (weak, nonatomic) IBOutlet FBLoginView *loginView;

@property (weak, nonatomic) IBOutlet UITextField *birthdayTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *nickNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;

@property (weak, nonatomic) IBOutlet UIScrollView *profileScrollView;

@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSegment;

@property (strong, nonatomic) UIDatePicker *birthdayDatePicker;

@end

@implementation ProfileInputViewController

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self deregisterFromKeyboardNotifications];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configureView];
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    [self registerForKeyboardNotifications];
    
    self.navigationItem.titleView = [[UIManager sharedManager] formatNavigationBarWithLogoAndNavigationBar:self.navigationController.navigationBar andTitleText:@"Your Profile"];
    self.navigationItem.hidesBackButton = YES;
}

- (void)customSetup {
    self.loginView.readPermissions = @[@"public_profile", @"email", @"user_birthday"];
    
    UITapGestureRecognizer *tapToHideKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard:)];
    [self.profileScrollView addGestureRecognizer:tapToHideKeyboardGesture];
    
    _birthdayDatePicker = [[UIDatePicker alloc] init];
    _birthdayDatePicker.datePickerMode = UIDatePickerModeDate;
    [_birthdayDatePicker addTarget:self action:@selector(birthdaySelectionFinished:) forControlEvents:UIControlEventValueChanged];
    self.birthdayTextField.inputView = _birthdayDatePicker;
}

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)hideKeyboard:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    
    return context;
}

- (void)birthdaySelectionFinished:(UIDatePicker *)picker {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    if ([picker.date compare:[NSDate date]] == NSOrderedDescending || [picker.date compare:[NSDate date]] == NSOrderedSame) {
        [[[UIAlertView alloc] initWithTitle:kErrorDateTitle message:kErrorMessage delegate:self cancelButtonTitle:nil otherButtonTitles:kOK, nil] show];
    } else {
        _birthdayTextField.text = [dateFormatter stringFromDate:picker.date];
    }
}

- (void)processProfileDetails {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Create a new managed object
    NSManagedObject *newUserObject = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
    [newUserObject setValue:_firstNameTextField.text forKey:@"firstname"];
    [newUserObject setValue:_lastNameTextField.text forKey:@"lastname"];
    [newUserObject setValue:_nickNameTextField.text forKey:@"nickname"];
    [newUserObject setValue:_birthdayTextField.text forKey:@"birthday"];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Error saving user profile details with error %@", error);
    }
    
    // Fetch the devices from persistent data store
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"User"];
    NSArray *userDetailsArray = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSLog(@"User Details: %@", userDetailsArray);
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:self.firstNameTextField.text forKey:@"fname"];
    [parameters setObject:self.lastNameTextField.text forKey:@"lname"];
    [parameters setObject:[[InfoManager sharedManager]removeZeroFromMobileNumber:self.mobileNumberString] forKey:@"mobile_number"];
    [parameters setObject:self.birthdayTextField.text forKey:@"birth_date"];
    if (self.genderSegment.selectedSegmentIndex == 0) {
        [parameters setObject:@"male" forKey:@"gender"];
    } else {
        [parameters setObject:@"female" forKey:@"gender"];
    }
    [parameters setObject:self.nickNameTextField.text forKey:@"nickname"];
    
    [[APIManager sharedManager] registerMobileNumberWithParameters:[parameters copy] success:^{
        [[APIManager sharedManager] loginWithMobileNumber:self.mobileNumberString success:^{
            
            [self performSegueWithIdentifier:@"ShowInterestsInputView" sender:nil];
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
            });
            
            NSLog(@"Error logging mobile number with error: %@", error);
        }];
    } failure:^(NSError *error) {
        NSLog(@"Error registering mobile number with error: %@", error);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
        });
    }];
}

#pragma mark - Button methods

- (IBAction)doneButtonTapped:(UIBarButtonItem *)sender {
    NSDictionary *detailParams = @{kFlurryTapped: kFlurryDone};
    [Flurry logEvent:kFlurryUserRegistrationManualFB withParameters:detailParams];
    
    [SVProgressHUD setFont:kGlobeFontRegular14];
    [SVProgressHUD setForegroundColor:kGlobeBlueColor];
    [SVProgressHUD showWithStatus:kHUDRegisteringUser maskType:SVProgressHUDMaskTypeGradient];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSLog(@"First name: %@", _firstNameTextField.text);
        NSLog(@"Last name: %@", _lastNameTextField.text);
        NSLog(@"Nickname: %@", _nickNameTextField.text);
        NSLog(@"Birthday: %@", _birthdayTextField.text);
        
        if ([_firstNameTextField.text isEqualToString:kEmptyString]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus:kErrorFirstName maskType:SVProgressHUDMaskTypeGradient];
            });
            
            return;
        }
        
        if ([_lastNameTextField.text isEqualToString:kEmptyString]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus:kErrorLastName maskType:SVProgressHUDMaskTypeGradient];
            });
            
            return;
        }
        
        if ([_nickNameTextField.text isEqualToString:kEmptyString]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus:kErrorNickname maskType:SVProgressHUDMaskTypeGradient];
            });
            
            return;
        }
        
        if ([_birthdayTextField.text isEqualToString:kEmptyString]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus:kErrorBirthday maskType:SVProgressHUDMaskTypeGradient];
            });
            
            return;
        }
        
        if (![_firstNameTextField.text isEqualToString:kEmptyString] && ![_lastNameTextField.text isEqualToString:kEmptyString] && ![_nickNameTextField.text isEqualToString:kEmptyString] && ![_birthdayTextField.text isEqualToString:kEmptyString]) {
            
            [self processProfileDetails];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus:kErrorProfileMessage maskType:SVProgressHUDMaskTypeGradient];
            });
        }
        
//        [self performSegueWithIdentifier:@"ShowInterestsInputView" sender:nil];
    });
}
    
- (IBAction)genderSegmentTapped:(UISegmentedControl *)sender {
    switch (self.genderSegment.selectedSegmentIndex) {
        case 0:
            break;
        case 1:
            break;
        default: 
            break;
    }
}

#pragma mark - UITextField methods

- (void)keyboardWasShown:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect bkgndRect = activeTextField.superview.frame;
    bkgndRect.size.height += kbSize.height;
    [activeTextField.superview setFrame:bkgndRect];
    [self.profileScrollView setContentOffset:CGPointMake(0.0, activeTextField.frame.origin.y + 70-kbSize.height) animated:YES];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification *)aNotification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _profileScrollView.contentInset = contentInsets;
    _profileScrollView.scrollIndicatorInsets = contentInsets;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    activeTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    activeTextField = nil;
} 

#pragma mark - Facebook methods

// This method will be called when the user information has been fetched
- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user {
    NSDictionary *detailParams = @{kFlurryTapped: kFlurryFBLogin};
    [Flurry logEvent:kFlurryUserRegistrationManualFB withParameters:detailParams];
    
    NSLog(@"User: %@", user);
    
    if (user[@"first_name"]) {
        self.firstNameTextField.text = user[@"first_name"];
    }
    
    if (user[@"last_name"]) {
        self.lastNameTextField.text = user[@"last_name"];
    }
    
    if (user[@"birthday"]) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"MM/dd/yyyy";
        NSDate *date = [dateFormatter dateFromString:user[@"birthday"]];
        dateFormatter.dateFormat = @"yyyy-MM-dd";
        self.birthdayTextField.text = [dateFormatter stringFromDate:date];
    }
    
    if ([user[@"gender"] isKindOfClass:[NSString class]]) {
        if ([user[@"gender"] isEqualToString:@"female"]) {
            self.genderSegment.selectedSegmentIndex = 1;
        }
    }
    
    [self processProfileDetails];
}

// Logged-in user experience
- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
//    self.statusLabel.text = @"You're logged in as";
}

// Logged-out user experience
- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
//    self.profilePictureView.profileID = nil;
//    self.nameLabel.text = @"";
//    self.statusLabel.text= @"You're not logged in!";
}

// Handle possible errors that can occur during login
- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
    NSString *alertMessage, *alertTitle;
    
    // If the user should perform an action outside of you app to recover,
    // the SDK will provide a message for the user, you just need to surface it.
    // This conveniently handles cases like Facebook password change or unverified Facebook accounts.
    if ([FBErrorUtility shouldNotifyUserForError:error]) {
        alertTitle = @"Facebook error";
        alertMessage = [FBErrorUtility userMessageForError:error];
        
        // This code will handle session closures that happen outside of the app
        // You can take a look at our error handling guide to know more about it
        // https://developers.facebook.com/docs/ios/errors
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession) {
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
        
        // If the user has cancelled a login, we will do nothing.
        // You can also choose to show the user a message if cancelling login will result in
        // the user not being able to complete a task they had initiated in your app
        // (like accessing FB-stored information or posting to Facebook)
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
        NSLog(@"user cancelled login");
        
        // For simplicity, this sample handles other errors with a generic message
        // You can checkout our error handling guide for more detailed information
        // https://developers.facebook.com/docs/ios/errors
    } else {
        alertTitle  = @"Something went wrong";
        alertMessage = @"Please try again later.";
        NSLog(@"Unexpected error:%@", error);
    }
    
    if (alertMessage) {
        [[[UIAlertView alloc] initWithTitle:alertTitle message:alertMessage delegate:nil cancelButtonTitle:kOK otherButtonTitles:nil] show];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"ShowInterestsInputView"]) {
//        [[segue destinationViewController] setMobileNumberString:_numberTextField.text];
    }
}

@end
