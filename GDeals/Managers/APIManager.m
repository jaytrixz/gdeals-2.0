//
//  APIManager.m
//  GDeals
//
//  Created by Jay Santos on 10/15/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import "APIManager.h"
#import "Constants.h"
#import "InfoManager.h"
#import "CoreDataManager.h"

#import "UIManager.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"

@implementation APIManager

+ (instancetype)sharedManager {
    __strong static APIManager *sharedManager = nil;
    static dispatch_once_t token = 0;
    dispatch_once(&token, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

- (void)getMobileNumber:(void (^)(NSString *mobileNumber))success
                failure:(void (^)(NSError *))failure {
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:kMobileNumberLinkString]];
    
    AFHTTPRequestOperation *getMobileNumberOperation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
    getMobileNumberOperation.responseSerializer = [AFJSONResponseSerializer serializer];
    [getMobileNumberOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        if (responseObject[@"msisdn"]) {
            NSString *mobileNumberString = responseObject[@"msisdn"];
            mobileNumberString = [mobileNumberString substringFromIndex:2];
            mobileNumberString = [kZeroString stringByAppendingString:mobileNumberString];
            
            NSLog(@"Mobile number: %@", mobileNumberString);
            success(mobileNumberString);
        } else {
            NSLog(@"Error: %@", responseObject[@"message"]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        failure(error);
    }];
    
    [[NSOperationQueue mainQueue] addOperation:getMobileNumberOperation];
}

- (void)verifyNumberWithMobileNumber:(NSString *)mobileNumber
                           signature:(NSString *)signature
           andVerificationCodeString:(NSString *)verificationCode
                             success:(void (^)(id responseObject))success
                             failure:(void (^)(NSError *))failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSDictionary *parameters = @{@"mobile_number":mobileNumber, @"verification_code":verificationCode, @"signature":signature};
    
    NSLog(@"Link: %@", kSendVerificationCodeLinkString);
    NSLog(@"Parameters: %@", parameters);
    [manager POST:kSendVerificationCodeLinkString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)verifyCodeWithMobileNumber:(NSString *)mobileNumber
                           success:(void (^)(id responseObject))success
                           failure:(void (^)(NSError *))failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSDictionary *parameters = @{@"mobile_number":mobileNumber};
    
    NSLog(@"Link: %@", kVerifyMobileNumberLinkString);
    NSLog(@"Parameters: %@", parameters);
    [manager GET:kVerifyMobileNumberLinkString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)loginWithMobileNumber:(NSString *)mobileNumber
                      success:(void (^)(void))success
                      failure:(void (^)(NSError *error))failure {
    if ([mobileNumber hasPrefix:@"0"]) {
        mobileNumber = [mobileNumber substringFromIndex:1];
    }
    NSDictionary *parameters = @{@"mobile_number":mobileNumber, @"client_id":kGdealsClientID, @"client_secret":kGdealsClientSecret};
    
    // Add hash
    parameters = @{@"mobile_number":mobileNumber, @"client_id":kGdealsClientID, @"client_secret":kGdealsClientSecret, @"signature":[[InfoManager sharedManager] createSHA1UsingData:parameters]};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kOAuthMobileNumberPath);
    NSLog(@"Parameters: %@", parameters);
    [manager POST:kOAuthMobileNumberPath parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"responseObject: %@", responseObject);
        
        if ([responseObject[@"message"] isEqualToString:@"Success"]) {
            NSDictionary *currentUserDictionary = responseObject;
            [[CoreDataManager sharedManager] saveCurrentUserWithUserDictionary:currentUserDictionary];
            
            // Save categories array to core data - new way
            NSArray *categoriesArray = currentUserDictionary[@"result"][@"categories"];
            [[CoreDataManager sharedManager] saveCategoriesToSettingsWithCategoriesArray:categoriesArray];
            
            [self getRequestTokenWithUserID:currentUserDictionary[@"result"][@"user"][@"id"] success:^{
                success();
            } failure:^(NSError *error) {
                failure(error);
            }];
        } else {
            failure(nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)getRequestTokenWithUserID:(NSString *)userID
                          success:(void (^)(void))success
                          failure:(void (^)(NSError *error))failure {
    NSDictionary *parameters = @{@"client_id":kGdealsClientID, @"client_secret":kGdealsClientSecret, @"response_type":@"request_token", @"scope":@"app.mobile", @"user_id":userID};
    
    // Add hash
    parameters = @{@"client_id":kGdealsClientID, @"client_secret":kGdealsClientSecret, @"response_type":@"request_token", @"scope":@"app.mobile", @"user_id":userID, @"signature":[[InfoManager sharedManager] createSHA1UsingData:parameters]};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kRequestTokenPath);
    NSLog(@"Parameters: %@", parameters);
    [manager POST:kRequestTokenPath parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"responseObject: %@", responseObject);
        
        if ([responseObject[@"message"] isEqualToString:@"Success"]) {
            NSString *requestTokenString = responseObject[@"result"][@"request_token"];
            
            [self getAccessTokenWithRequestToken:requestTokenString success:^{
                success();
            } failure:^(NSError *error) {
                failure(error);
            }];
        } else {
            failure(nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)getAccessTokenWithRequestToken:(NSString *)requestToken success:(void (^)(void))success failure:(void (^)(NSError *error))failure {
    NSDictionary *parameters = @{@"client_id":kGdealsClientID, @"client_secret":kGdealsClientSecret, @"request_token":requestToken, @"grant_type":@"authorization_code"};
    
    // Add hash
    parameters = @{@"client_id":kGdealsClientID, @"client_secret":kGdealsClientSecret, @"request_token":requestToken, @"grant_type":@"authorization_code", @"signature":[[InfoManager sharedManager] createSHA1UsingData:parameters]};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kAccessTokenPath);
    NSLog(@"Parameters: %@", parameters);
    [manager POST:kAccessTokenPath parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"responseObject: %@", responseObject);
        
        if ([responseObject[@"message"] isEqualToString:@"Success"]) {
            // Save access token to Core data
            NSString *accessToken = responseObject[@"result"][@"access_token"];
            [[CoreDataManager sharedManager] saveAccessTokenWithAccessToken:accessToken];
            
            // Get user ID
            NSDictionary *currentUserDictionary = [[CoreDataManager sharedManager] fetchCurrentUserDictionary];
            NSString *userID = currentUserDictionary[@"result"][@"user"][@"id"];
            
            // Save client version to core data
            NSString *clientVersion = currentUserDictionary[@"result"][@"client_version"][@"ios"];
            [[CoreDataManager sharedManager] saveClientVersionToSettingsWithClientVersionString:clientVersion];
            
            NSLog(@"Current user ID: %@ access token: %@ client version: %@", userID, accessToken, clientVersion);
            
//            // Create settings dictionary
//            NSMutableDictionary *settings = [[NSMutableDictionary alloc] init];
//            [settings setObject:accessToken forKey:@"access_token"];
//            [settings setObject:currentUserDictionary[@"result"][@"user"] forKey:@"current_user"];
//            [settings setObject:currentUserDictionary[@"result"][@"client_version"] forKey:@"current_version"];
//            NSDictionary *categoriesDictionary = currentUserDictionary[@"result"][@"categories"];
//            [settings setObject:categoriesDictionary forKey:@"current_categories"];
            
            // Save categories array to core data - old way
//            NSLog(@"currentUserDictionary: %@", currentUserDictionary);
//            NSArray *categoriesArray = currentUserDictionary[@"result"][@"categories"];
//            [[CoreDataManager sharedManager] saveCategoriesToSettingsWithCategoriesArray:categoriesArray];
            
            success();
        } else {
            failure(nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)registerMobileNumberWithParameters:(NSDictionary *)parameters
                                   success:(void (^)(void))success
                                   failure:(void (^)(NSError *error))failure {
    NSMutableDictionary *modifiedParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
    modifiedParameters[@"signature"] = [[InfoManager sharedManager] createSHA1UsingData:parameters];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kSignupPath);
    NSLog(@"Parameters: %@", modifiedParameters);
    [manager POST:kSignupPath parameters:[modifiedParameters copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)getUserInterests:(void (^)(NSArray *userInterestsArray))success
                 failure:(void (^)(NSError *error))failure {
    NSString *accessToken = [[CoreDataManager sharedManager] fetchCurrentUserAccessToken];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kAllTagsPath);
    NSLog(@"Parameters: %@", @{@"access_token": accessToken});
    [manager GET:kAllTagsPath parameters:@{@"access_token": accessToken} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"responseObject: %@", responseObject);
        if ([responseObject[@"message"] isEqualToString:@"Success"]) {
            NSArray *result = responseObject[@"result"];
            if (result) {
                success(result);
            } else {
                NSLog(@"Error: %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
                failure(nil);
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)updateUserInterests:(NSString *)interests
                    success:(void (^)(void))success
                    failure:(void (^)(NSError *error))failure
{
    NSString *accessToken = [[CoreDataManager sharedManager] fetchCurrentUserAccessToken];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kAllTagsPath);
    NSLog(@"Parameters: %@", @{@"tags": interests, @"access_token": accessToken});
    [manager PUT:kAllTagsPath parameters:@{@"tags": interests, @"access_token": accessToken} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)getBanners:(void (^)(NSDictionary *bannersDictionary))success
           failure:(void (^)(NSError *error))failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kGetBanners);
    [manager GET:kGetBanners parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"responseObject: %@", responseObject);
        if ([responseObject[@"message"] isEqualToString:@"Success"]) {
            NSDictionary *result = responseObject[@"result"];
            if (result) {
                success(result);
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)getDealsWithParameters:(NSDictionary *)parameters
                       success:(void (^)(NSDictionary *dealsDictionary))success
                       failure:(void (^)(NSError *error))failure {
    NSMutableDictionary *modifiedParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
    modifiedParameters[@"access_token"] = [[CoreDataManager sharedManager] fetchCurrentUserAccessToken];
    modifiedParameters[@"client_version"] = [[CoreDataManager sharedManager] fetchClientVersion];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kGetDeals);
    NSLog(@"Parameters: %@", modifiedParameters);
    [manager GET:kGetDeals parameters:modifiedParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"responseObject: %@", responseObject);
        if ([responseObject[@"message"] isEqualToString:@"Success"]) {
            NSDictionary *result = responseObject[@"result"];
            if (result) {
                success(result);
            } else {
                NSLog(@"Error: %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
                failure(nil);
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)loadBannerImageWithImageURL:(NSString *)imageURLString
                            success:(void (^)(id bannerImage))success
                            failure:(void (^)(NSError *error))failure {
    imageURLString = [[InfoManager sharedManager] formatImageString:imageURLString];
    AFHTTPRequestOperation *bannerImageRequestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imageURLString] cachePolicy:2 timeoutInterval:1]];
    bannerImageRequestOperation.responseSerializer = [AFImageResponseSerializer serializer];
    [bannerImageRequestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Banner image: %@", responseObject);
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    
    [bannerImageRequestOperation start];
}

- (void)loadImageWithImage:(id)imageView
               andImageURL:(NSString *)imageURLString
                   success:(void (^)(id image))success
                   failure:(void (^)(NSError *error))failure {
    imageURLString = [[InfoManager sharedManager] formatImageString:imageURLString];
    [imageView setImageWithURLRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imageURLString]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        success(image);
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        failure(error);
    }];
}

- (void)fetchVoucherDetails:(void (^)(NSArray *voucherDetailsArray))success
                    failure:(void (^)(NSError *error))failure {
    
    NSDictionary *parameters = @{@"access_token": [[CoreDataManager sharedManager] fetchCurrentUserAccessToken]};
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kGetVouchers);
    NSLog(@"Parameters: %@", parameters);
    [manager GET:kGetVouchers parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"responseObject: %@", responseObject);
        if ([responseObject[@"message"] isEqualToString:@"Success"]) {
            NSArray *result = responseObject[@"result"];
            if (result) {
                success(result);
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)updateUserDetailsWithParameters:(NSMutableDictionary *)parameters
                                success:(void (^)(NSString *result))success
                                failure:(void (^)(NSError *error))failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kUpdateUserDetails);
    NSLog(@"Parameters: %@", parameters);
    [manager PUT:kUpdateUserDetails parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"responseObject: %@", responseObject);
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        failure(error);
    }];
}

- (void)getGlobeRewardsPoints:(void (^)(NSString *points))success
                      failure:(void (^)(NSError *error))failure {
    NSDictionary *parameters = @{@"access_token": [[CoreDataManager sharedManager] fetchCurrentUserAccessToken]};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kGlobeRewardsPoints);
    NSLog(@"Parameters: %@", parameters);
    [manager GET:kGlobeRewardsPoints parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"responseObject: %@", responseObject);
        if ([responseObject[@"message"] isEqualToString:@"Success"]) {
            NSString *result = responseObject[@"result"];
            if (result) {
                success(result);
            } else {
                NSLog(@"Error: %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
                failure(nil);
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)getQRCodeDetailsWithQRCode:(NSString *)qrCodeString
                           success:(void (^)(NSDictionary *infoDictionary))success
                           failure:(void (^)(NSError *error))failure {
    NSDictionary *parameters = @{@"access_token": [[CoreDataManager sharedManager] fetchCurrentUserAccessToken], @"qr_code": qrCodeString};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kCheckQRCodeInfo);
    NSLog(@"Parameters: %@", parameters);
    [manager GET:kCheckQRCodeInfo parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"responseObject: %@", responseObject);
        if ([responseObject[@"message"] isEqualToString:@"Success"]) {
            NSDictionary *result = responseObject[@"result"];
            if (result) {
                success(result);
            } else {
                NSLog(@"Error: %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
                failure(nil);
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)getUserAddress:(void (^)(NSArray *))success
               failure:(void (^)(NSError *))failure {
    NSDictionary *parameters = @{@"access_token": [[CoreDataManager sharedManager] fetchCurrentUserAccessToken]};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kUserAddress);
    NSLog(@"Parameters: %@", parameters);
    [manager GET:kUserAddress parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"responseObject: %@", responseObject);
        if ([responseObject[@"message"] isEqualToString:@"Success"]) {
            NSArray *result = responseObject[@"result"];
            if (result) {
                success(result);
            } else {
                NSLog(@"Error: %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
                failure(nil);
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)saveUserAddressWithParameters:(NSDictionary *)parameters
                              success:(void (^)(NSString *result))success
                              failure:(void (^)(NSError *error))failure {
    NSString *accessToken = [[CoreDataManager sharedManager] fetchCurrentUserAccessToken];
    NSMutableDictionary *updatedParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
    [updatedParameters setObject:accessToken forKey:@"access_token"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kUserAddress);
    NSLog(@"Parameters: %@", updatedParameters);
    [manager PUT:kUserAddress parameters:updatedParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"message"] isEqualToString:@"Success"]) {
            NSString *messageString = responseObject[@"message"];
            success(messageString);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)addNewUserAddressWithParameters:(NSDictionary *)parameters
                                success:(void (^)(NSString *result))success
                                failure:(void (^)(NSError *error))failure {
    NSMutableDictionary *updatedParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
    NSString *accessToken = [[CoreDataManager sharedManager] fetchCurrentUserAccessToken];
    [updatedParameters setObject:accessToken forKey:@"access_token"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kUserAddress);
    NSLog(@"Parameters: %@", updatedParameters);
    [manager POST:kUserAddress parameters:updatedParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"message"] isEqualToString:@"Success"]) {
            NSString *messageString = responseObject[@"message"];
            success(messageString);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)useGlobeRewardsPointsWithParameters:(NSDictionary *)parameters
                                    success:(void (^)(NSString *result))success
                                    failure:(void (^)(NSError *error))failure {
    NSString *accessToken = [[CoreDataManager sharedManager] fetchCurrentUserAccessToken];
    NSMutableDictionary *updatedParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
    [updatedParameters setObject:accessToken forKey:@"access_token"];
    [updatedParameters setObject:[[InfoManager sharedManager] createSHA1UsingData:updatedParameters] forKey:@"signature"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kGlobeRewardsPoints);
    NSLog(@"Parameters: %@", updatedParameters);
    [manager POST:kGlobeRewardsPoints parameters:updatedParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"responseObject: %@", responseObject);
        success(responseObject[@"message"]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)getSeasonalBannerDealsWithParameters:(NSDictionary *)parameters
                                     success:(void (^)(NSDictionary *dealsDictionary))success
                                     failure:(void (^)(NSError *error))failure {
    NSMutableDictionary *modifiedParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
    modifiedParameters[@"access_token"] = [[CoreDataManager sharedManager] fetchCurrentUserAccessToken];
    modifiedParameters[@"client_version"] = [[CoreDataManager sharedManager] fetchClientVersion];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kGetDeals);
    NSLog(@"Parameters: %@", modifiedParameters);
    [manager GET:kGetDeals parameters:modifiedParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        NSLog(@"responseObject: %@", responseObject);
        if ([responseObject[@"message"] isEqualToString:@"Success"]) {
            NSDictionary *result = responseObject[@"result"];
            if (result) {
                success(result);
            } else {
                NSLog(@"Error: %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
                failure(nil);
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)searchDealWithParameters:(NSDictionary *)parameters
                         success:(void (^)(NSDictionary *resultsDictionary))success
                         failure:(void (^)(NSError *error))failure {
    NSMutableDictionary *modifiedParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
    modifiedParameters[@"access_token"] = [[CoreDataManager sharedManager] fetchCurrentUserAccessToken];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kSearchDeal);
    NSLog(@"Parameters: %@", modifiedParameters);
    [manager GET:kSearchDeal parameters:modifiedParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"responseObject: %@", responseObject);
        if ([responseObject[@"message"] isEqualToString:@"Success"]) {
            NSDictionary *result = responseObject[@"result"];
            if (result) {
                success(result);
            } else {
                NSLog(@"Error: %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
                failure(nil);
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)getParticipatingBranchesWithParameters:(NSDictionary *)parameters
                                       success:(void (^)(NSDictionary *resultsDictionary))success
                                       failure:(void (^)(NSError *error))failure {
    NSMutableDictionary *modifiedParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
    modifiedParameters[@"access_token"] = [[CoreDataManager sharedManager] fetchCurrentUserAccessToken];
    NSDictionary *userDictionary = [[CoreDataManager sharedManager] fetchCurrentUserDictionary];
    modifiedParameters[@"user_id"] = userDictionary[@"result"][@"user"][@"id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kRewardsBranches);
    NSLog(@"Parameters: %@", modifiedParameters);
    [manager GET:kRewardsBranches parameters:modifiedParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"responseObject: %@", responseObject);
        if ([responseObject[@"message"] isEqualToString:@"Success"]) {
            NSDictionary *result = responseObject[@"result"];
            if (result) {
                success(result);
            } else {
                NSLog(@"Error: %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
                failure(nil);
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    
}

- (void)submitPaymentToOkayOkayWithParameters:(NSDictionary *)parameters
                                      success:(void (^)(NSString *HTMLString))success
                                      failure:(void (^)(NSError *error))failure {
    NSMutableDictionary *updatedParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
    NSString *accessToken = [[CoreDataManager sharedManager] fetchCurrentUserAccessToken];
    [updatedParameters setObject:accessToken forKey:@"access_token"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kSubmitToOkayOkay);
    NSLog(@"Parameters: %@", updatedParameters);
    [manager POST:kSubmitToOkayOkay parameters:updatedParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"message"] isEqualToString:@"Success"]) {
            NSLog(@"responseObject: %@", responseObject);
            success(responseObject[@"result"][@"checkout_html"]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)getDealInfoWithDealIdentifier:(NSString *)identifier
                              success:(void (^)(NSDictionary *resultsDictionary))success
                              failure:(void (^)(NSError *error))failure {
    NSDictionary *parameters = @{@"deal_id": identifier};
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kGetDealInfo);
    NSLog(@"Parameters: %@", parameters);
    [manager GET:kGetDealInfo parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"responseObject: %@", responseObject);
        if ([responseObject[@"message"] isEqualToString:@"Success"]) {
            NSDictionary *result = responseObject[@"result"];
            if (result) {
                success(result);
            } else {
                NSLog(@"Error: %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
                failure(nil);
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)updateAccessTokensWithParameters:(NSDictionary *)parameters
                                 success:(void (^)(void))success
                                 failure:(void (^)(NSError *error))failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    NSLog(@"Link: %@", kUpdatePushAccessTokens);
    NSLog(@"Parameters: %@", parameters);
    [manager PUT:kUpdatePushAccessTokens parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"message"] isEqualToString:@"Success"]) {
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

@end
