//
//  DealDetailsViewController.m
//  GDeals
//
//  Created by Jay Santos on 1/23/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "DealDetailsViewController.h"

#import "Deal.h"
#import "Flurry.h"
#import "Constants.h"
#import "UIManager.h"
#import "APIManager.h"
#import "InfoManager.h"
#import "SVProgressHUD.h"
#import "RecoTableViewCell.h"
#import "TermsTableViewCell.h"
#import "INTULocationManager.h"
#import "OverviewTableViewCell.h"
#import "RecoCollectionViewCell.h"
#import "DescriptionTableViewCell.h"
#import "TermsNoDateTableViewCell.h"
#import "OverviewRewardsTableViewCell.h"
#import "PaymentOptionsViewController.h"
#import "SelectionTableViewController.h"
#import "DescriptionRewardsTableViewCell.h"
#import "GlobeRewardsPaymentViewController.h"

@interface DealDetailsViewController () {
    BOOL isLoading;
}

@property (strong, nonatomic) Deal *recoDealModel;

@property (strong, nonatomic) NSString *latitudeString;
@property (strong, nonatomic) NSString *longitudeString;

@property (strong, nonatomic) NSMutableArray *recoDealsArray;
@property (strong, nonatomic) NSMutableArray *participatingBranchesArray;

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *dealEndView;

@property (weak, nonatomic) IBOutlet UILabel *endDateLabel;

@property (weak, nonatomic) IBOutlet UIButton *buyNowButton;
@property (weak, nonatomic) IBOutlet UIButton *biggerBuyNowButton;

@property (weak, nonatomic) IBOutlet UITableView *detailsTable;

@end

@implementation DealDetailsViewController {
    BOOL isLocationDetected;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configureView];
    
    // to resize table cells to its final dimensions especially when reloading this view
    [self.detailsTable reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self customSetup];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.detailsTable reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    self.navigationController.navigationBar.tintColor = kGlobeLightBlueColor;
    
    [self trackUserLocation];
}

- (void)customSetup {
    if (![self.dealModel.endDate isEqual:(id)[NSNull null]]) {
        self.biggerBuyNowButton = nil;
        self.endDateLabel.text = [[InfoManager sharedManager] convertUnixTimeToString:[self.dealModel.endDate floatValue]];
    } else {
        // Hide buy now button IB and deal end view and reveal wider buy button
        self.dealEndView.hidden = YES;
        self.buyNowButton.hidden = YES;
        self.biggerBuyNowButton.hidden = NO;
    }
    
    self.recoDealsArray = [NSMutableArray array];
    self.participatingBranchesArray = [NSMutableArray array];
    
    [self.detailsTable reloadData];
}

- (void)trackUserLocation {
    INTULocationManager *locationManager = [INTULocationManager sharedInstance];
    [locationManager requestLocationWithDesiredAccuracy:INTULocationAccuracyCity timeout:10.0 delayUntilAuthorized:YES block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
        if (status == INTULocationStatusSuccess) {
            NSLog(@"Location saved");
            
            self.latitudeString = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
            self.longitudeString = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
            
            isLocationDetected = YES;
        }
    }];
}

- (void)getAllParticipatingBranches {
    NSDictionary *detailParams = @{kFlurryTapped: kFlurryParticipatingBranches};
    [Flurry logEvent:kFlurryDealDetails withParameters:detailParams];
    
    if ([self.participatingBranchesArray count] == 0) {
        if (isLocationDetected) {
            NSDictionary *parameters = @{@"deal_id": self.dealModel.identifier, @"latitude": self.latitudeString, @"longitude": self.longitudeString};
            [[APIManager sharedManager] getParticipatingBranchesWithParameters:parameters success:^(NSDictionary *resultsDictionary) {
                NSLog(@"resultsDictionary: %@", resultsDictionary);
                self.participatingBranchesArray = resultsDictionary[@"merchants"];
                
                [self.detailsTable reloadData];
            } failure:^(NSError *error) {
                NSLog(@"Error getting participating branches with error: %@", error);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
                });
            }];
        } else {
            [self trackUserLocation];
        }
    }
}

#pragma mark - Button methods

- (IBAction)buyButtonTapped:(id)sender {
    NSDictionary *detailParams = @{kFlurryTapped: kFlurryBuyNow};
    [Flurry logEvent:kFlurryDealDetails withParameters:detailParams];
    
    if ([self.dealModel.source isEqualToString:@"globe_rewards"]) {
        [SVProgressHUD showWithStatus:kHUDValidatingPoints maskType:SVProgressHUDMaskTypeGradient];
        
        if (!isLocationDetected) {
            // manually add zero coordinates to long and lat since this is not used anymore
            self.longitudeString = @"0.00000000";
            self.latitudeString = @"0.00000000";
            
            isLocationDetected = YES;
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[APIManager sharedManager] getGlobeRewardsPoints:^(NSString *points) {
                NSLog(@"points: %@", points);
                
                if ([points integerValue] >= [self.dealModel.minimumRewardsPoints integerValue]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                    });
                    
                    [self performSegueWithIdentifier:@"ShowGlobeRewardsPaymentOptions" sender:nil];
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSString *errorString = [NSString stringWithFormat:@"Sorry, you must have at least %@ points to avail this deal.", self.dealModel.minimumRewardsPoints];
                        [SVProgressHUD showErrorWithStatus:errorString maskType:SVProgressHUDMaskTypeGradient];
                    });
                }
            } failure:^(NSError *error) {
                NSLog(@"Failed to get globe rewards points with error: %@", error);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
                });
            }];
        });
    } else {
        [self performSegueWithIdentifier:@"ShowPaymentOptions" sender:nil];
    }
}

#pragma mark - Cells methods

- (void)configureOverviewRewardsCell:(OverviewRewardsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.iconImage.image = [[UIManager sharedManager] getCategoryIconWithCategoryIdentifier:self.dealModel.categoryIdNew];
    
    // check if dealImage has an image loaded
    CGImageRef cgref = [cell.dealImage.image CGImage];
    CIImage *cim = [cell.dealImage.image CIImage];
    if (cim == nil && cgref == NULL) {
        NSLog(@"dealImage has no image yet");
        if (self.dealModel.cachedImage != (id)[NSNull null]) {
            cell.dealImage.image = self.dealModel.cachedImage;
        }
//        else {
//            cell.dealImage.image = [UIImage imageNamed:@""];
//        }
    }
    
//    if (!cell.dealImage.image) {
//        if (self.dealModel.cachedImage != (id)[NSNull null]) {
//            cell.dealImage.image = self.dealModel.cachedImage;
//        } else {
//            cell.dealImage.image = [UIImage imageNamed:@""];
//        }
//    }
    
    cell.titleLabel.text = self.dealModel.name;
    cell.subtitleLabel.text = self.dealModel.sourceText;
    cell.totalBoughtLabel.text = [NSString stringWithFormat:@"%@", self.dealModel.purchaseCount];
    cell.totalPointsLabel.text = [NSString stringWithFormat:@"%@ Points", self.dealModel.minimumRewardsPoints];
}

- (void)configureOverviewCell:(OverviewTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"self.dealModel.cachedImage: %@", self.dealModel.cachedImage);
    
    cell.iconImage.image = [[UIManager sharedManager] getCategoryIconWithCategoryIdentifier:self.dealModel.categoryIdNew];
    
    if (self.dealModel.cachedImage != (id)[NSNull null]) {
        cell.dealImage.image = self.dealModel.cachedImage;
    }
    
//    // loads deal image if cached image is nil
//    if (!self.dealModel.cachedImage) {
//        [[APIManager sharedManager] loadImageWithImage:cell.dealImage andImageURL:self.dealModel.bannerImageURL success:^(id image) {
//            cell.dealImage.image = image;
//        } failure:^(NSError *error) {
//            NSLog(@"Failed to load deal image in deal details with error: %@", error);
//        }];
//    }
    
    cell.titleLabel.text = self.dealModel.name;
    cell.subtitleLabel.text = self.dealModel.sourceText;
    cell.totalBoughtLabel.text = [NSString stringWithFormat:@"%@", self.dealModel.purchaseCount];
    cell.discountLabel.text = [NSString stringWithFormat:@"%@%@", self.dealModel.discount, @"%"];
    
    
    cell.originalPriceLabel.text = [[InfoManager sharedManager] formatStringToPrice:self.dealModel.originalPrice];
    cell.originalPriceLabel.attributedText = [[InfoManager sharedManager] formatOriginalPrice:cell.originalPriceLabel.text withLineColor:[UIColor blackColor]];
    cell.discountedPriceLabel.text = [[InfoManager sharedManager] formatStringToPrice:self.dealModel.discountedPrice];
    cell.discountedPriceLabel.text = [NSString stringWithFormat:@"₱ %@", cell.discountedPriceLabel.text];
}

- (void)configureDescriptionRewardsCell:(DescriptionRewardsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.descriptionText.attributedText = [[NSAttributedString alloc] initWithData:[self.dealModel.descriptionText dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    cell.descriptionText.font = kGlobeFontRegular14;
    
    cell.branchesButton.layer.cornerRadius = 10;
    
    if (isLocationDetected) {
        if ([self.participatingBranchesArray count] == 0) {
            [self getAllParticipatingBranches];
        } else {
            [cell.activityLoader stopAnimating];
            cell.activityLoader.hidden = YES;
            
            cell.branchesButton.hidden = NO;
        }
    }
    
//    if ([self.participatingBranchesArray count] == 0) {
//        INTULocationManager *locationManager = [INTULocationManager sharedInstance];
//        [locationManager requestLocationWithDesiredAccuracy:INTULocationAccuracyCity timeout:10.0 delayUntilAuthorized:YES block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
//            if (status == INTULocationStatusSuccess) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [cell.activityLoader stopAnimating];
//                    cell.branchesButton.hidden = NO;
//                });
//                
//                self.latitudeString = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
//                self.longitudeString = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
//                NSDictionary *parameters = @{@"deal_id": self.dealModel.identifier, @"latitude": self.latitudeString, @"longitude": self.longitudeString};
//                [[APIManager sharedManager] getParticipatingBranchesWithParameters:parameters success:^(NSDictionary *resultsDictionary) {
//                    NSLog(@"resultsDictionary: %@", resultsDictionary);
//                    self.participatingBranchesArray = resultsDictionary[@"merchants"];
//                } failure:^(NSError *error) {
//                    NSLog(@"Error getting participating branches with error: %@", error);
//                    
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
//                    });
//                    
//                }];
//            }
//        }];
//    }
}

- (void)configureDescriptionCell:(DescriptionTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.descriptionText.attributedText = [[NSAttributedString alloc] initWithData:[self.dealModel.descriptionText dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    cell.descriptionText.font = kGlobeFontRegular14;
}

- (void)configureTermsNoDateCell:(TermsNoDateTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.termsText.text = self.dealModel.terms;
    cell.termsText.attributedText = [[NSAttributedString alloc] initWithData:[self.dealModel.terms dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    cell.termsText.font = kGlobeFontRegular14;
}

- (void)configureTermsCell:(TermsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    CGFloat startRedemptionTime = [self.dealModel.startRedemption floatValue];
    CGFloat endRedemptionTime = [self.dealModel.endRedemption floatValue];
    cell.redeemedText.text = [NSString stringWithFormat:@"Can be redeemed from:\n%@ up to %@", [[InfoManager sharedManager] convertUnixTimeToString:startRedemptionTime], [[InfoManager sharedManager] convertUnixTimeToString:endRedemptionTime]];
    
//    NSLog(@"self.dealModel.terms: %@", self.dealModel.terms);
    cell.termsText.text = self.dealModel.terms;
    cell.termsText.attributedText = [[NSAttributedString alloc] initWithData:[self.dealModel.terms dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    cell.termsText.font = kGlobeFontRegular14;
}

- (void)configureRecoCell:(RecoTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    if (!isLoading) {
        
        // to prevent fetching reco deals numerous times
        isLoading = YES;
        
        if ([self.recoDealsArray count] == 0) {
            if (!cell.loader.isAnimating) {
                [cell.loader startAnimating];
            }
            
            NSDictionary *parameters = [NSDictionary dictionary];
            if (self.dealModel.categoryId) {
                if (self.dealModel.categoryIdNew) {
                    parameters = @{@"action": @"00", @"page_legend": @"DD", @"strip_html_tags": @"1", @"source_page": @"LP", @"deal_id": self.dealModel.identifier, @"category_id": self.dealModel.categoryId, @"category_id_new": self.dealModel.categoryIdNew};
                } else {
                    parameters = @{@"action": @"00", @"page_legend": @"DD", @"strip_html_tags": @"1", @"source_page": @"LP", @"deal_id": self.dealModel.identifier, @"category_id": self.dealModel.categoryId};
                }
            } else {
                parameters = @{@"action": @"00", @"page_legend": @"DD", @"strip_html_tags": @"1", @"source_page": @"LP", @"deal_id": self.dealModel.identifier, @"category_id_new": self.dealModel.categoryIdNew};
            }
            
            [[APIManager sharedManager] getDealsWithParameters:parameters success:^(NSDictionary *dealsDictionary) {
                self.recoDealsArray = [[InfoManager sharedManager] setupDealsArrayWithDealDictionary:dealsDictionary];
                [cell.recoCollection reloadData];
                
                if (cell.loader.isAnimating) {
                    [cell.loader stopAnimating];
                }
            } failure:^(NSError *error) {
                NSLog(@"Error fetching reco deals with error: %@", error);
                
                if (cell.loader.isAnimating) {
                    [cell.loader stopAnimating];
                }
                
                //            dispatch_async(dispatch_get_main_queue(), ^{
                //                [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
                //            });
            }];
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section == 0) {
        return 1;
    } else {
        return 3;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Configure the cell...
    if (indexPath.section == 0) {
        if ([self.dealModel.source isEqualToString:@"globe_rewards"]) {
            OverviewRewardsTableViewCell *cell = (OverviewRewardsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"OverviewRewardsCell" forIndexPath:indexPath];
            if (!cell) {
                cell = [[OverviewRewardsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OverviewRewardsCell"];
            }
            
            [self configureOverviewRewardsCell:cell atIndexPath:indexPath];
            
            return cell;
        } else {
            OverviewTableViewCell *cell = (OverviewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"OverviewCell" forIndexPath:indexPath];
            if (!cell) {
                cell = [[OverviewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OverviewCell"];
            }
            
            [self configureOverviewCell:cell atIndexPath:indexPath];
            
            return cell;
        }
    } else {
        if (indexPath.row == 0) {
            if ([self.dealModel.source isEqualToString:@"globe_rewards"]) {
                DescriptionRewardsTableViewCell *cell = (DescriptionRewardsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"DescriptionRewardsCell" forIndexPath:indexPath];
                if (!cell) {
                    cell = [[DescriptionRewardsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DescriptionRewardsCell"];
                }
                
                [self configureDescriptionRewardsCell:cell atIndexPath:indexPath];
                
                return cell;
            } else {
                DescriptionTableViewCell *cell = (DescriptionTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"DescriptionCell" forIndexPath:indexPath];
                if (!cell) {
                    cell = [[DescriptionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DescriptionCell"];
                }
                
                [self configureDescriptionCell:cell atIndexPath:indexPath];
                
                return cell;
            }
            
        } else if (indexPath.row == 1) {
            if (self.dealModel.startRedemption == (id)[NSNull null] || self.dealModel.endRedemption == (id)[NSNull null]) {
                TermsNoDateTableViewCell *cell = (TermsNoDateTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"TermsNoDateCell" forIndexPath:indexPath];
                if (!cell) {
                    cell = [[TermsNoDateTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TermsNoDateCell"];
                }
                
                [self configureTermsNoDateCell:cell atIndexPath:indexPath];
                
                return cell;
            } else {
                TermsTableViewCell *cell = (TermsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"TermsCell" forIndexPath:indexPath];
                if (!cell) {
                    cell = [[TermsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TermsCell"];
                }
                
                [self configureTermsCell:cell atIndexPath:indexPath];
                
                return cell;
            }
        } else {
            RecoTableViewCell *cell = (RecoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"RecoCell" forIndexPath:indexPath];
            if (!cell) {
                cell = [[RecoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RecoCell"];
            }
            
            [self configureRecoCell:cell atIndexPath:indexPath];
            
            return cell;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
        return UITableViewAutomaticDimension;
    }
    
    if (indexPath.section == 0) {
        return 290;
    } else {
        if (indexPath.row == 0) {
            return 95;
        } else if (indexPath.row == 1) {
            return 150;
        } else {
            return 241;
        }
    }
}

#pragma mark - Collection view methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.recoDealsArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
     RecoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RecoCollectionCell" forIndexPath:indexPath];
    
    self.recoDealModel = [[Deal alloc] init];
    self.recoDealModel = self.recoDealsArray[indexPath.row];
    
//    NSLog(@"Image URL: %@", self.recoDealModel.imageURL);
    
    // create white image as placeholder image
    cell.dealImage.image = [[UIManager sharedManager] createWhitePlaceholderImageWithImage:cell.dealImage];
    cell.dealImage.tag = indexPath.row;
    
    [[APIManager sharedManager] loadImageWithImage:cell.dealImage andImageURL:self.recoDealModel.imageURL success:^(id image) {
        if ([image isKindOfClass:[UIImage class]]) {
            self.recoDealModel.cachedImage = image;
            cell.dealImage.image = self.recoDealModel.cachedImage;
        }
    } failure:^(NSError *error) {
        NSLog(@"Error loading image with error: %@", error);
    }];
    
    cell.titleLabel.text = self.recoDealModel.name;
    if ([self.recoDealModel.source isEqualToString:@"globe_rewards"]) {
        cell.originalPriceLabel.text = @"Min. Globe Rewards";
        cell.discountedPriceLabel.text = [NSString stringWithFormat:@"%@ Points", self.recoDealModel.minimumRewardsPoints];
    } else {
        cell.originalPriceLabel.text = [[InfoManager sharedManager] formatStringToPrice:self.recoDealModel.originalPrice];
        cell.originalPriceLabel.attributedText = [[InfoManager sharedManager] formatOriginalPrice:cell.originalPriceLabel.text withLineColor:[UIColor blackColor]];
        cell.discountedPriceLabel.text = [[InfoManager sharedManager] formatStringToPrice:self.recoDealModel.discountedPrice];
        cell.discountedPriceLabel.text = [NSString stringWithFormat:@"₱ %@", cell.discountedPriceLabel.text];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    // this pushes another instance of deal details view with the updated deal model and adds it to the navigation controller stack so the user can still go back anytime
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DealDetailsViewController *dealDetailsVC = [storyboard instantiateViewControllerWithIdentifier:@"DealDetailsView"];
    dealDetailsVC.dealModel = self.recoDealsArray[indexPath.row];
    [self.navigationController pushViewController:dealDetailsVC animated:YES];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"ShowPaymentOptions"]) {
        [segue.destinationViewController setDealModel:self.dealModel];
    } else if ([segue.identifier isEqualToString:@"ShowGlobeRewardsPaymentOptions"]) {
        [segue.destinationViewController setDealModel:self.dealModel];
        [segue.destinationViewController setLatitudeString:self.latitudeString];
        [segue.destinationViewController setLongitudeString:self.longitudeString];
    } else {
        [segue.destinationViewController setParticipatingBranchesArray:self.participatingBranchesArray];
    }
}

@end
