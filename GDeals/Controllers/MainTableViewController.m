//
//  MainTableViewController.m
//  GDeals
//
//  Created by Jay Santos on 10/14/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import "MainTableViewController.h"
#import "PhoneInputViewController.h"

#import "Deal.h"
#import "Banner.h"
#import "Constants.h"
#import "UIManager.h"
#import "APIManager.h"
#import "InfoManager.h"
#import "SVProgressHUD.h"
#import "SVPullToRefresh.h"
#import "CoreDataManager.h"
#import "MainTableViewCell.h"
#import "EmptyTableViewCell.h"
#import "BannerTableViewCell.h"
#import "UnavailableTableViewCell.h"
#import "DealDetailsViewController.h"
#import "SearchTableViewController.h"
#import "BannerContentViewController.h"

@interface MainTableViewController () {
    BOOL isLogged;
    BOOL isTableEmpty;
    BOOL areDealsUnavailable;
    
    NSTimer *bannerTimer;
    NSUInteger nextBannerIndex;
}

@property (strong, nonatomic) Deal *dealModel;

@property (weak, nonatomic) IBOutlet UISearchBar *dealsSearchBar;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuBarButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *searchBarButtonItem;

@property (assign, nonatomic) NSUInteger landingPageIndex;

@property (strong, nonatomic) NSMutableArray *allDealsArray;
@property (strong, nonatomic) NSMutableArray *allBannersAndPromosArray;

@property (strong, nonatomic) UIPageViewController *pageViewController;

@end

@implementation MainTableViewController

- (void)viewWillAppear:(BOOL)animated {
    [self customSetup];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
//    [self customSetup];
}

- (void)viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if (bannerTimer) {
        [bannerTimer invalidate];
        bannerTimer = nil;
    }
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    self.navigationItem.titleView = [[UIManager sharedManager] formatNavigationBarWithNavigationBar:self.navigationController.navigationBar andTitleText:@"GDeals"];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.delegate = self;
    if (revealViewController) {
        [_menuBarButtonItem setTarget:self.revealViewController];
        [_menuBarButtonItem setAction:@selector(revealToggle:)];
        [self.navigationController.navigationBar addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    // removes the search bar in the table
    self.tableView.tableHeaderView = nil;
}

- (void)customSetup {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetBannersAndDeals:) name:kResetBannersAndDealsNotification object:nil];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([[defaults objectForKey:@"UserIsLogged"] isEqualToString:@"YES"]) {
        isLogged = YES;
    } else {
        isLogged = NO;
    }
    
    isTableEmpty = NO;
    
    if (!isLogged) {
        PhoneInputViewController *phoneInputVC = (PhoneInputViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"PhoneInput"];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:phoneInputVC];
        [self.parentViewController presentViewController:navController animated:YES completion:nil];
    } else {
        // reapply banner timer
        bannerTimer = [NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(switchToNextBanner) userInfo:nil repeats:YES];
        
        if (!self.allBannersAndPromosArray) {
            self.allBannersAndPromosArray = [NSMutableArray array];
            self.allDealsArray = [NSMutableArray array];
            
            // Needed to call private methods inside of blocks
            __weak MainTableViewController *weakSelf = self;
            
            [self.tableView addPullToRefreshWithActionHandler:^{
                [weakSelf fetchDeals];
            }];
            
            [self.tableView addInfiniteScrollingWithActionHandler:^{
                [weakSelf fetchMoreDeals];
            }];
            
            [self.tableView.pullToRefreshView setTitle:@"Loading Deals" forState:SVPullToRefreshStateLoading];
            [self.tableView.pullToRefreshView setTitle:@"Pull to Refresh Deals" forState:SVPullToRefreshStateStopped];
            [self.tableView.pullToRefreshView setTitle:@"Release to Refresh Deals" forState:SVPullToRefreshStateTriggered];
            [self.tableView.pullToRefreshView setTextColor:kGlobeBlueColor];
            [self.tableView.pullToRefreshView setActivityIndicatorViewColor:kGlobeBlueColor];
            [self.tableView.pullToRefreshView setArrowColor:kGlobeBlueColor];
            
            [SVProgressHUD setFont:kGlobeFontRegular14];
            [SVProgressHUD setForegroundColor:kGlobeBlueColor];
            [SVProgressHUD showWithStatus:kHUDLoadingBanners maskType:SVProgressHUDMaskTypeBlack];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self fetchBanners];
            });
        }
    }
}

- (BannerContentViewController *)loadBannerViewAtIndex:(NSUInteger)index {
    NSLog(@"Index: %lu", (unsigned long)index);
    BannerContentViewController *bannerContentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BannerContentViewController"];
    NSUInteger bannerCount = [_allBannersAndPromosArray count];
    NSDictionary *bannerDictionary = nil;
    
    if (bannerCount == 0) {
        bannerContentVC.bannerIndex = index;
        
        if (bannerDictionary[@"detail"]) {
            bannerContentVC.bannerModel = [[InfoManager sharedManager] setupBannerModelWithBannerDictionary:nil];
        } else {
            bannerContentVC.dealModel = [[InfoManager sharedManager] setupDealModelWithDealDictionary:nil];
        }
    } else if (index >= bannerCount) {
        bannerDictionary = self.allBannersAndPromosArray[0];
        bannerContentVC.bannerIndex = 0;
        
        if (bannerDictionary[@"detail"]) {
            bannerContentVC.bannerModel = [[InfoManager sharedManager] setupBannerModelWithBannerDictionary:bannerDictionary];
        } else {
            bannerContentVC.dealModel = [[InfoManager sharedManager] setupDealModelWithDealDictionary:bannerDictionary];
        }
        
        nextBannerIndex = 1;
    } else {
        bannerDictionary = self.allBannersAndPromosArray[index];
        bannerContentVC.bannerIndex = index;
        
        if (bannerDictionary[@"detail"]) {
            bannerContentVC.bannerModel = [[InfoManager sharedManager] setupBannerModelWithBannerDictionary:bannerDictionary];
        } else {
            bannerContentVC.dealModel = [[InfoManager sharedManager] setupDealModelWithDealDictionary:bannerDictionary];
        }
        
        nextBannerIndex = index + 1;
    }
    
    return bannerContentVC;
}

- (void)fetchBanners {
    isTableEmpty = NO;
    areDealsUnavailable = NO;
    
    if (bannerTimer) {
        [bannerTimer invalidate];
        bannerTimer = nil;
    }
    
    [[APIManager sharedManager] getBanners:^(NSDictionary *bannersDictionary) {
        NSLog(@"bannersDictionary: %@", bannersDictionary);
        
        isTableEmpty = NO;
        areDealsUnavailable = NO;
        
        NSArray *bannersArray = bannersDictionary[@"banner"];
        NSArray *heroDealsArray = bannersDictionary[@"hero_deal"];
        for (NSDictionary *bannerDictionary in bannersArray) {
            [self.allBannersAndPromosArray addObject:bannerDictionary];
        }
        
        for (NSDictionary *heroDealDictionary in heroDealsArray) {
            [self.allBannersAndPromosArray addObject:heroDealDictionary];
        }
        
        BannerContentViewController *startingVC = [self loadBannerViewAtIndex:0];
        NSArray *viewControllers = @[startingVC];
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        self.pageViewController.dataSource = nil; // to reload the page view controller with new data
        self.pageViewController.dataSource = self;
        
//        NSLog(@"allBannersAndDealsArray: %@", self.allBannersAndPromosArray);
        
        if ([self.allDealsArray count] == 0) {
            [self.tableView triggerPullToRefresh];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        
        // timer to automatically advance banner to next screen after 4 seconds
        if (bannerTimer) {
            [bannerTimer invalidate];
            bannerTimer = nil;
        }
        
        bannerTimer = [NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(switchToNextBanner) userInfo:nil repeats:YES];
        
    } failure:^(NSError *error) {
        NSLog(@"Error loading banners with error: %@", error);
        
        isTableEmpty = YES;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
        });
        
        [self.tableView reloadData];
    }];
}

- (void)fetchDeals {
    areDealsUnavailable = NO;
    
    if ([self.allBannersAndPromosArray count] == 0) {
        [self fetchBanners];
    }
    
    NSDictionary *parameters = @{@"action": @"00", @"page_legend": @"LP", @"strip_html_tags": @"1", @"page": @"0", @"source_page": @"LP"};
    [[APIManager sharedManager] getDealsWithParameters:parameters success:^(NSDictionary *dealsDictionary) {
        NSLog(@"Deal dictionary: %@", dealsDictionary);
        
        areDealsUnavailable = NO;
        
        if ([dealsDictionary count] != 0) {
            self.allDealsArray = [[[InfoManager sharedManager] setupDealsArrayWithDealDictionary:dealsDictionary] mutableCopy];
            
            self.landingPageIndex++;
        } else {
            areDealsUnavailable = YES;
        }
        
        [self.tableView reloadData];
        [self.tableView.pullToRefreshView stopAnimating];
    } failure:^(NSError *error) {
        [self.tableView.pullToRefreshView stopAnimating];
        
        NSLog(@"Error loading deals with error: %@", error);
        
        isTableEmpty = YES;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
        });
    }];
}

- (void)fetchMoreDeals {
    NSDictionary *parameters = @{@"action": @"00", @"page_legend": @"LP", @"strip_html_tags": @"1", @"page": [NSString stringWithFormat:@"%lu", (unsigned long)self.landingPageIndex], @"source_page": @"LP"};
    [[APIManager sharedManager] getDealsWithParameters:parameters success:^(NSDictionary *dealsDictionary) {
//        NSLog(@"Deal dictionary: %@", dealsDictionary);
        
        if ([dealsDictionary count] != 0) {
            NSArray *newDealsArray = [[InfoManager sharedManager] setupDealsArrayWithDealDictionary:dealsDictionary];
            NSMutableArray *indexPathsArray = [NSMutableArray array];
            
            NSUInteger previousArrayCount = [_allDealsArray count];
            for (int i = 0; i < [newDealsArray count]; i++) {
                [indexPathsArray addObject:[NSIndexPath indexPathForRow:previousArrayCount + i inSection:1]];
            }
            
            [self.allDealsArray addObjectsFromArray:newDealsArray];
            
            [self.tableView beginUpdates];
            [self.tableView insertRowsAtIndexPaths:indexPathsArray withRowAnimation:UITableViewRowAnimationTop];
            [self.tableView endUpdates];
            
            self.landingPageIndex++;
        } else {
            
        }
        
        [self.tableView.infiniteScrollingView stopAnimating];
    } failure:^(NSError *error) {
        [self.tableView.infiniteScrollingView stopAnimating];
        
        NSLog(@"Error loading deals with error: %@", error);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
        });
    }];
}

- (void)resetBannersAndDeals:(NSNotification *)notification {
    self.tableView = nil;
    self.allBannersAndPromosArray = nil;
}

- (void)switchToNextBanner {
    BannerContentViewController *bannerVC = [self loadBannerViewAtIndex:nextBannerIndex];
    NSArray *viewControllers = @[bannerVC];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

#pragma mark - Button methods

- (IBAction)searchButtonTapped:(UIBarButtonItem *)sender {
    // re-enables the search bar IBOutlet to the table header
    self.tableView.tableHeaderView = self.dealsSearchBar;
    
    [self.dealsSearchBar becomeFirstResponder];
}

#pragma mark - Search methods

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    self.tableView.tableHeaderView = nil;
    
    [self.dealsSearchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search bar text: %@", searchBar.text);
    
    [self performSegueWithIdentifier:@"ShowSearchView" sender:nil];
}

#pragma mark - Page control methods

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    NSUInteger index = ((BannerContentViewController *) viewController).bannerIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    if (index == 0) {
        index = [_allBannersAndPromosArray count] - 1;
    } else {
        index--;
    }
    
    return [self loadBannerViewAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    NSUInteger index = ((BannerContentViewController *) viewController).bannerIndex;
    NSLog(@"The index: %lu", (unsigned long)index);
    
    if (index == NSNotFound) {
        return nil;
    }
    
    if (index == [_allBannersAndPromosArray count]) {
        index = 0;
    } else {
        index++;
    }
    
    return [self loadBannerViewAtIndex:index];
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {
    
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return 0;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (isTableEmpty || areDealsUnavailable) {
        return 1;
    } else {
        return 2;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        // to prevent seeing No Results text in search table
        return 1;
    } else {
        if (isTableEmpty || areDealsUnavailable) {
            return 1;
        } else if (section == 0) {
            return 1;
        } else {
            return [_allDealsArray count];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Configure the cell...
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
            cell.userInteractionEnabled = NO;
        }
        
        return cell;
    } else {
        if (isTableEmpty) {
            EmptyTableViewCell *cell = (EmptyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"EmptyCell" forIndexPath:indexPath];
            [self configureEmptyCell:cell atIndexPath:indexPath];
            
            return cell;
        } else if (areDealsUnavailable) {
            UnavailableTableViewCell *cell = (UnavailableTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"UnavailCell" forIndexPath:indexPath];
            [self configureUnavailableCell:cell atIndexPath:indexPath];
            
            return cell;
        }
        
        if (indexPath.section == 0) {
            BannerTableViewCell *cell = (BannerTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"BannerCell" forIndexPath:indexPath];
            
            [self configureBannerCell:cell atIndexPath:indexPath];
            
            return cell;
        } else {
            MainTableViewCell *cell = (MainTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MainCell" forIndexPath:indexPath];
            
            [self configureMainCell:cell atIndexPath:indexPath];
            
            return cell;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return 44;
    } else {
        if (isTableEmpty || areDealsUnavailable) {
            return 500;
        }
        
        if (indexPath.section == 0) {
            return kMainBannerRowHeight;
        } else {
            return kMainTableRowHeight;
        }
    }
}

#pragma mark - Cell methods

- (void)configureBannerCell:(BannerTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BannerViewController"];
    self.pageViewController.dataSource = self;
    
    BannerContentViewController *startingVC = [self loadBannerViewAtIndex:0];
    NSArray *viewControllers = @[startingVC];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    self.pageViewController.view.frame = CGRectMake(0, 0, cell.contentView.frame.size.width, cell.contentView.frame.size.height);
    
    [self addChildViewController:_pageViewController];
    [cell.contentView addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    //    cell.tag = startingVC.bannerIndex;
    //    UITapGestureRecognizer *tapBannerImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bannerImageTapped:)];
    //    [cell addGestureRecognizer:tapBannerImage];
}

- (void)configureMainCell:(MainTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    self.dealModel = [[Deal alloc] init];
    self.dealModel = self.allDealsArray[indexPath.row];
    
    if ([self.dealModel.source isEqualToString:@"globe_rewards"]) {
        self.dealModel.originalPrice = @"Min. Globe Rewards";
        self.dealModel.discountedPrice = [NSString stringWithFormat:@"%@ Points", self.dealModel.minimumRewardsPoints];
        
        cell.originalPriceLabel.text = self.dealModel.originalPrice;
        cell.discountedPriceLabel.text = self.dealModel.discountedPrice;
    } else {
        cell.originalPriceLabel.text = [[InfoManager sharedManager] formatStringToPrice:self.dealModel.originalPrice];
        cell.originalPriceLabel.attributedText = [[InfoManager sharedManager] formatOriginalPrice:cell.originalPriceLabel.text withLineColor:[UIColor whiteColor]];
        cell.discountedPriceLabel.text = [[InfoManager sharedManager] formatStringToPrice:self.dealModel.discountedPrice];
        cell.discountedPriceLabel.text = [NSString stringWithFormat:@"₱ %@", cell.discountedPriceLabel.text];
    }
    
    
//    // removes gradient sublayer to prevent overlapping
//    cell.dealImage.layer.sublayers = nil;
    
    // create white image as placeholder image
    cell.dealImage.image = [[UIManager sharedManager] createWhitePlaceholderImageWithImage:cell.dealImage];
    cell.dealImage.tag = indexPath.row;
    
    [[APIManager sharedManager] loadImageWithImage:cell.dealImage andImageURL:self.dealModel.imageURL success:^(id image) {
        if ([image isKindOfClass:[UIImage class]]) {
            self.dealModel.cachedImage = image;
            cell.dealImage.image = self.dealModel.cachedImage;
        }
    } failure:^(NSError *error) {
        NSLog(@"Error loading image with error: %@", error);
    }];
    
    if ([self.dealModel.source isEqualToString:@"globe_rewards"]) {
        cell.discountLabel.hidden = YES;
        cell.ribbonImage.hidden = YES;
    } else {
        cell.discountLabel.hidden = NO;
        cell.ribbonImage.hidden = NO;
        cell.discountLabel.text = [NSString stringWithFormat:@"save %@%@", self.dealModel.discount, @"%"];
    }
    
    cell.titleLabel.text = self.dealModel.name;
    cell.subtitleLabel.text = self.dealModel.sourceText;
    
    // Animates the table cell
    dispatch_async(dispatch_get_main_queue(), ^{
        CALayer *animationLayer = cell.layer;
        animationLayer.transform = CATransform3DMakeTranslation(0, animationLayer.bounds.size.height/3, 0.0f);
        
        NSTimeInterval animationDuration = 0.5;
        [UIView beginAnimations:nil
                        context:nil];
        
        [UIView setAnimationDuration:animationDuration];
        cell.layer.transform = CATransform3DIdentity;
        cell.layer.opacity = 1.0f;
        
        [UIView commitAnimations];
    });
}

- (void)configureEmptyCell:(EmptyTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.titleLabel.text = kErrorNoInternet;
    
    // Animates the table cell
    dispatch_async(dispatch_get_main_queue(), ^{
        CALayer *animationLayer = cell.layer;
        animationLayer.transform = CATransform3DMakeTranslation(0, animationLayer.bounds.size.height/3, 0.0f);
        
        NSTimeInterval animationDuration = 0.5;
        [UIView beginAnimations:nil
                        context:nil];
        
        [UIView setAnimationDuration:animationDuration];
        cell.layer.transform = CATransform3DIdentity;
        cell.layer.opacity = 1.0f;
        
        [UIView commitAnimations];
    });
}

- (void)configureUnavailableCell:(UnavailableTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.titleLabel.text = kErrorNoDealsFound;
    
    // Animates the table cell
    dispatch_async(dispatch_get_main_queue(), ^{
        CALayer *animationLayer = cell.layer;
        animationLayer.transform = CATransform3DMakeTranslation(0, animationLayer.bounds.size.height/3, 0.0f);
        
        NSTimeInterval animationDuration = 0.5;
        [UIView beginAnimations:nil
                        context:nil];
        
        [UIView setAnimationDuration:animationDuration];
        cell.layer.transform = CATransform3DIdentity;
        cell.layer.opacity = 1.0f;
        
        [UIView commitAnimations];
    });
}

#pragma mark - SWRevealViewController methods

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:    (FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:    (FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ShowDealDetails"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        MainTableViewCell *cell = sender;
        
        self.dealModel = [[Deal alloc] init];
        self.dealModel = self.allDealsArray[indexPath.row];
        self.dealModel.cachedImage = cell.dealImage.image;
        
        [segue.destinationViewController setDealModel:self.dealModel];
    } else {
        [segue.destinationViewController setSearchString:self.dealsSearchBar.text];
    }
}

@end
