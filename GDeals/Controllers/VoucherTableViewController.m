//
//  VoucherTableViewController.m
//  GDeals
//
//  Created by Jay Santos on 1/28/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "VoucherTableViewController.h"

#import "Constants.h"
#import "UIManager.h"
#import "APIManager.h"
#import "SVProgressHUD.h"
#import "EmptyTableViewCell.h"
#import "VoucherTableViewCell.h"
#import "VoucherDetailsTableViewController.h"

@interface VoucherTableViewController () {
    BOOL isTableEmpty;
}

@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuBarButtonItem;

@property (strong, nonatomic) NSMutableArray *vouchersArray;

@end

@implementation VoucherTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)customSetup {
    self.navigationItem.titleView = [[UIManager sharedManager] formatNavigationBarWithNavigationBar:self.navigationController.navigationBar andTitleText:self.navigationController.title];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.delegate = self;
    if (revealViewController) {
        [_menuBarButtonItem setTarget:self.revealViewController];
        [_menuBarButtonItem setAction:@selector(revealToggle:)];
        [self.navigationController.navigationBar addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    isTableEmpty = NO;
    
    self.vouchersArray = [NSMutableArray array];
    
    [self fetchVoucherDetails];
}

- (void)fetchVoucherDetails {
    isTableEmpty = NO;
    
    [SVProgressHUD setFont:kGlobeFontRegular14];
    [SVProgressHUD setForegroundColor:kGlobeBlueColor];
    [SVProgressHUD showWithStatus:kHUDLoadingVouchers maskType:SVProgressHUDMaskTypeGradient];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[APIManager sharedManager] fetchVoucherDetails:^(NSArray *voucherDetailsArray) {
            isTableEmpty = NO;
            
            [self.vouchersArray addObjectsFromArray:voucherDetailsArray];
            
            [self.tableView reloadData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        } failure:^(NSError *error) {
            NSLog(@"Error fetching voucher details with error: %@", error);
            
            isTableEmpty = YES;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        }];
    });
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (isTableEmpty) {
        return 1;
    } else {
        return [self.vouchersArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isTableEmpty) {
        EmptyTableViewCell *cell = (EmptyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"EmptyCell" forIndexPath:indexPath];
        [self configureEmptyCell:cell atIndexPath:indexPath];
        
        return cell;
    } else {
        VoucherTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VoucherCell" forIndexPath:indexPath];
        [self configureVoucherCell:cell atIndexPath:indexPath];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isTableEmpty) {
        return 100;
    } else {
        return 60;
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Cell methods

- (void)configureEmptyCell:(EmptyTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.titleLabel.text = kErrorNoInternet;
    
    // makes the cell and table view background color look uniform
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    // Animates the table cell
    dispatch_async(dispatch_get_main_queue(), ^{
        CALayer *animationLayer = cell.layer;
        animationLayer.transform = CATransform3DMakeTranslation(0, animationLayer.bounds.size.height/3, 0.0f);
        
        NSTimeInterval animationDuration = 0.5;
        [UIView beginAnimations:nil
                        context:nil];
        
        [UIView setAnimationDuration:animationDuration];
        cell.layer.transform = CATransform3DIdentity;
        cell.layer.opacity = 1.0f;
        
        [UIView commitAnimations];
    });
}

- (void)configureVoucherCell:(VoucherTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.iconImage.image = [[UIManager sharedManager] applyIconImageWithVoucherName:self.vouchersArray[indexPath.row][@"type"]];
    cell.nameLabel.text = self.vouchersArray[indexPath.row][@"name"];
    cell.countLabel.text = [NSString stringWithFormat:@"%@", self.vouchersArray[indexPath.row][@"count"]];
    cell.countView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.countView.layer.borderWidth = 1.0;
    cell.countView.layer.cornerRadius = 10.0;
    cell.countView.layer.masksToBounds = YES;
}

#pragma mark - SWRevealViewController methods

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:    (FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:    (FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    
    NSDictionary *voucherDictionary = self.vouchersArray[indexPath.row];
    [[segue destinationViewController] setTitle:voucherDictionary[@"name"]];
    [[segue destinationViewController] setNodesArray:voucherDictionary[@"nodes"]];
}

@end
