//
//  MenuTableViewController.m
//  GDeals
//
//  Created by Jay Santos on 10/14/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import "MenuTableViewController.h"

#import "Constants.h"
#import "CoreDataManager.h"
#import "MenuTableViewCell.h"
#import "CategoryTableViewCell.h"
#import "SWRevealViewController.h"
#import "MainTableViewController.h"
#import "PhoneInputViewController.h"
#import "CategoryDealsTableViewController.h"

@interface MenuTableViewController () {
    PhoneInputViewController *phoneInputVC;
    BOOL isCategoriesShown;
}

@property (strong, nonatomic) NSArray *menuArray;

@property (strong, nonatomic) NSArray *categoriesArray;

@property (assign, nonatomic) NSIndexPath *tableIndexPath;

@end

@implementation MenuTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.menuArray = @[@"main", @"categories", @"vouchers", @"preferences", @"profile", @"faq", @"contact", @"logout"];
    self.categoriesArray = @[@"fashion", @"home", @"electronics", @"lifestyle", @"beauty", @"dining", @"travel", @"kids", @"automobile"];
    
    isCategoriesShown = NO;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)showLoginView {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"NO" forKey:@"UserIsLogged"];
    [defaults synchronize];
    
    [[CoreDataManager sharedManager] deleteAllObjectsInCoreData];
    
    // Closes side menu
//    [self.revealViewController revealToggleAnimated:YES];
    
    phoneInputVC = (PhoneInputViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"PhoneInput"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:phoneInputVC];
    [self.parentViewController presentViewController:navController animated:YES completion:nil];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        sleep(3.0);
        
        // Load main view at the back which will be reloaded upon login with new data
        [self performSegueWithIdentifier:@"ShowMainView" sender:nil];
    });
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (isCategoriesShown) {
        return [_menuArray count] + [_categoriesArray count];
    } else {
        return [_menuArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = nil;
    
    if (isCategoriesShown) {
        NSInteger adjustedIndex;
        if (indexPath.row < 2) {
            cellIdentifier = @"main"; //_menuArray[indexPath.row];
            
            MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            cell.iconImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@%@", _menuArray[indexPath.row], @"_icon"]];
            
            if ([_menuArray[indexPath.row] isEqualToString:@"main"]) {
                cell.nameLabel.text = @"Home";
            } else {
                cell.nameLabel.text = [_menuArray[indexPath.row] capitalizedString];
            }
            
            return cell;
        } else if (indexPath.row > 1 && indexPath.row < 11) {
            adjustedIndex = indexPath.row - 2;
            cellIdentifier = @"categories"; //_categoriesArray[adjustedIndex];
            
            CategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            cell.iconImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@%@", _categoriesArray[adjustedIndex], @"_category_icon"]];
            cell.nameLabel.text = [_categoriesArray[adjustedIndex] capitalizedString];
            
            return cell;
        } else {
            adjustedIndex = indexPath.row - 9;
            cellIdentifier = @"main"; //_menuArray[adjustedIndex];
            
            MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            cell.iconImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@%@", _menuArray[adjustedIndex], @"_icon"]];
            
            if ([_menuArray[adjustedIndex] isEqualToString:@"faq"]) {
                cell.nameLabel.text = @"FAQ";
            } else if ([_menuArray[adjustedIndex] isEqualToString:@"contact"]) {
                cell.nameLabel.text = @"Contact Us";
            } else if ([_menuArray[adjustedIndex] isEqualToString:@"logout"]) {
                cell.nameLabel.text = @"Log Out";
            } else {
                cell.nameLabel.text = [_menuArray[adjustedIndex] capitalizedString];
            }
            
            return cell;
        }
    } else {
        cellIdentifier = @"main"; //_menuArray[indexPath.row];
        
        MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.iconImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@%@", _menuArray[indexPath.row], @"_icon"]];
        
        if ([_menuArray[indexPath.row] isEqualToString:@"main"]) {
            cell.nameLabel.text = @"Home";
        } else if ([_menuArray[indexPath.row] isEqualToString:@"faq"]) {
            cell.nameLabel.text = @"FAQ";
        } else if ([_menuArray[indexPath.row] isEqualToString:@"contact"]) {
            cell.nameLabel.text = @"Contact Us";
        } else if ([_menuArray[indexPath.row] isEqualToString:@"logout"]) {
            cell.nameLabel.text = @"Log Out";
        } else {
            cell.nameLabel.text = [_menuArray[indexPath.row] capitalizedString];
        }
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.tableIndexPath = indexPath;
    
    switch (indexPath.row) {
        case 0: {
            NSLog(@"Main tapped");
            
            [self performSegueWithIdentifier:@"ShowMainView" sender:nil];
        }
            
        break;
        
        case 1: {
            NSMutableArray *indexPathsArray = [[NSMutableArray alloc] init];
            for (int i = 0; i < [_categoriesArray count]; i++) {
                NSIndexPath *categoryIndex = [NSIndexPath indexPathForRow:i + 2 inSection:0];
                [indexPathsArray addObject:categoryIndex];
            }
            
            [tableView beginUpdates];
            
            if (isCategoriesShown) {
                isCategoriesShown = NO;
                
                [tableView deleteRowsAtIndexPaths:indexPathsArray withRowAnimation:UITableViewRowAnimationFade];
            } else {
                isCategoriesShown = YES;
                
                [tableView insertRowsAtIndexPaths:indexPathsArray withRowAnimation:UITableViewRowAnimationFade];
            }
            
            [tableView endUpdates];
        }
            
        break;
            
        case 2: {
            if (isCategoriesShown) {
                NSLog(@"Fashion tapped");
                
                [self performSegueWithIdentifier:@"ShowCategoryView" sender:nil];
            } else {
                NSLog(@"Vouchers tapped");
                
                [self performSegueWithIdentifier:@"ShowVouchersView" sender:nil];
            }
        }
            
        break;
            
        case 3: {
            if (isCategoriesShown) {
                NSLog(@"Home tapped");
                
                [self performSegueWithIdentifier:@"ShowCategoryView" sender:nil];
            } else {
                NSLog(@"Preferences tapped");
                
                [self performSegueWithIdentifier:@"ShowPreferencesView" sender:nil];
            }
        }
            
        break;
            
        case 4: {
            if (isCategoriesShown) {
                NSLog(@"Electronics tapped");
                
                [self performSegueWithIdentifier:@"ShowCategoryView" sender:nil];
            } else {
                NSLog(@"Profile tapped");
                
                [self performSegueWithIdentifier:@"ShowProfileView" sender:nil];
            }
        }
            
        break;
            
        case 5: {
            if (isCategoriesShown) {
                NSLog(@"Lifestyle tapped");
                
                [self performSegueWithIdentifier:@"ShowCategoryView" sender:nil];
            } else {
                NSLog(@"FAQ tapped");
                
                [self performSegueWithIdentifier:@"ShowFAQView" sender:nil];
            }
        }
            
        break;
            
        case 6: {
            if (isCategoriesShown) {
                NSLog(@"Beauty tapped");
                
                [self performSegueWithIdentifier:@"ShowCategoryView" sender:nil];
            } else {
                NSLog(@"Contact Us tapped");
                
                if ([MFMailComposeViewController canSendMail]) {
                    MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] initWithNibName:nil bundle:nil];
                    composeViewController.mailComposeDelegate = self;
                    [composeViewController setToRecipients:@[kSupportEmail]];
                    [composeViewController setSubject:@"GDeals Support"];
                    
                    [self presentViewController:composeViewController animated:YES completion:nil];
                } else {
                    [[[UIAlertView alloc] initWithTitle:@"Sorry" message:[NSString stringWithFormat:@"Your email has not been set up. Please email: %@", kSupportEmail] delegate:self cancelButtonTitle:nil otherButtonTitles:kOK, nil] show];
                }
            }
        }
            
        break;
            
        case 7: {
            if (isCategoriesShown) {
                NSLog(@"Dining tapped");
                
                [self performSegueWithIdentifier:@"ShowCategoryView" sender:nil];
            } else {
                NSLog(@"Log Out tapped");
                
                UIAlertView *logOutAlert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to log out?" message:nil delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
                logOutAlert.tag = 1;
                [logOutAlert show];
            }
        }
            
        break;
            
        case 8: {
            NSLog(@"Travel tapped");
            
            [self performSegueWithIdentifier:@"ShowCategoryView" sender:nil];
        }
            
        break;
            
        case 9: {
            NSLog(@"Kids tapped");
            
            [self performSegueWithIdentifier:@"ShowCategoryView" sender:nil];
        }
            
        break;
            
        case 10: {
            NSLog(@"Automobile tapped");
            
            [self performSegueWithIdentifier:@"ShowCategoryView" sender:nil];
        }
            
        break;
            
        case 11: {
            NSLog(@"Vouchers tapped");
            
            [self performSegueWithIdentifier:@"ShowVouchersView" sender:nil];
        }
            
        break;
            
        case 12: {
            NSLog(@"Preferences tapped");
            
            [self performSegueWithIdentifier:@"ShowPreferencesView" sender:nil];
        }
            
        break;
            
        case 13: {
            NSLog(@"Profile tapped");
            
            [self performSegueWithIdentifier:@"ShowProfileView" sender:nil];
        }
            
        break;
            
        case 14: {
            NSLog(@"FAQ tapped");
            
            [self performSegueWithIdentifier:@"ShowFAQView" sender:nil];
        }
            
        break;
            
        case 15: {
            NSLog(@"Contact Us tapped");
            
            if ([MFMailComposeViewController canSendMail]) {
                MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] initWithNibName:nil bundle:nil];
                composeViewController.mailComposeDelegate = self;
                [composeViewController setToRecipients:@[kSupportEmail]];
                [composeViewController setSubject:@"GDeals Support"];
                
                [self presentViewController:composeViewController animated:YES completion:nil];
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Sorry" message:[NSString stringWithFormat:@"Your email has not been set up. Please email: %@", kSupportEmail] delegate:self cancelButtonTitle:nil otherButtonTitles:kOK, nil] show];
            }
        }
            
        break;
            
        default: {
            NSLog(@"Log Out tapped");
            
            UIAlertView *logOutAlert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to log out?" message:nil delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            logOutAlert.tag = 1;
            [logOutAlert show];
        }
        
        break;
    }
}

# pragma mark - Mail Composer Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    // Closes side menu
    [self.revealViewController revealToggleAnimated:YES];
}

#pragma mark - Alert view methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        if (buttonIndex == 0) {
            NSLog(@"User cancelled to log out");
        } else {
            NSLog(@"User is logged out");
            
            [self showLoginView];
        }
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    UINavigationController *destinationViewController = (UINavigationController *)segue.destinationViewController;
    
    if ([segue.identifier isEqualToString:@"ShowCategoryView"]) {
        destinationViewController.title = [_categoriesArray[self.tableIndexPath.row - 2] capitalizedString];
    } else if ([segue.identifier isEqualToString:@"ShowVouchersView"]) {
        if (isCategoriesShown) {
            destinationViewController.title = [_menuArray[self.tableIndexPath.row - 9] capitalizedString];
        } else {
            destinationViewController.title = [_menuArray[self.tableIndexPath.row] capitalizedString];
        }
    } else if ([segue.identifier isEqualToString:@"ShowPreferencesView"]) {
        if (isCategoriesShown) {
            destinationViewController.title = [_menuArray[self.tableIndexPath.row - 9] capitalizedString];
        } else {
            destinationViewController.title = [_menuArray[self.tableIndexPath.row] capitalizedString];
        }
    } else if ([segue.identifier isEqualToString:@"ShowProfileView"]) {
        if (isCategoriesShown) {
            destinationViewController.title = [_menuArray[self.tableIndexPath.row - 9] capitalizedString];
        } else {
            destinationViewController.title = [_menuArray[self.tableIndexPath.row] capitalizedString];
        }
    } else if ([segue.identifier isEqualToString:@"ShowFAQView"]) {
        if (isCategoriesShown) {
            destinationViewController.title = [_menuArray[self.tableIndexPath.row - 9] uppercaseString];
        } else {
            destinationViewController.title = [_menuArray[self.tableIndexPath.row] uppercaseString];
        }
    }
}

@end
