//
//  PickerTableViewCell.h
//  GDeals
//
//  Created by Jay Santos on 2/9/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickerTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@end
