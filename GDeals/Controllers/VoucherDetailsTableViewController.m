//
//  VoucherDetailsTableViewController.m
//  GDeals
//
//  Created by Jay Santos on 1/29/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "VoucherDetailsTableViewController.h"

#import "Constants.h"
#import "UIManager.h"
#import "APIManager.h"
#import "InfoManager.h"
#import "AFNetworking.h"
#import "EmptyTableViewCell.h"
#import "VoucherDetailsTableViewCell.h"

@interface VoucherDetailsTableViewController () {
    BOOL isTableEmpty;
}

@property (strong, nonatomic) NSMutableArray *cachedImagesArray;

@end

@implementation VoucherDetailsTableViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configureView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self customSetup];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    self.navigationController.navigationBar.tintColor = kGlobeLightBlueColor;
}

- (void)customSetup {
    self.navigationItem.titleView = [[UIManager sharedManager] formatNavigationBarWithNavigationBar:self.navigationController.navigationBar andTitleText:self.title];
    
    isTableEmpty = NO;
    
    if ([self.nodesArray count] == 0) {
        isTableEmpty = YES;
    } else {
        isTableEmpty = NO;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (isTableEmpty) {
        return 1;
    } else {
        return [self.nodesArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isTableEmpty) {
        EmptyTableViewCell *cell = (EmptyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"EmptyCell" forIndexPath:indexPath];
        [self configureEmptyCell:cell atIndexPath:indexPath];
        
        return cell;
    } else {
        VoucherDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VoucherDetailsCell" forIndexPath:indexPath];
        [self configureVoucherDetails:cell atIndexPath:indexPath];
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
        return UITableViewAutomaticDimension;
    }
    
    if (isTableEmpty) {
        return 500;
    } else {
        return 296;
    }
}

#pragma mark - Cell methods

- (void)configureEmptyCell:(EmptyTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.titleLabel.text = kErrorNoVouchersFound;
    
    // makes the cell and table view background color look uniform
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    // Animates the table cell
    dispatch_async(dispatch_get_main_queue(), ^{
        CALayer *animationLayer = cell.layer;
        animationLayer.transform = CATransform3DMakeTranslation(0, animationLayer.bounds.size.height/3, 0.0f);
        
        NSTimeInterval animationDuration = 0.5;
        [UIView beginAnimations:nil
                        context:nil];
        
        [UIView setAnimationDuration:animationDuration];
        cell.layer.transform = CATransform3DIdentity;
        cell.layer.opacity = 1.0f;
        
        [UIView commitAnimations];
    });
}

- (void)configureVoucherDetails:(VoucherDetailsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *nodeDictionary = self.nodesArray[indexPath.row];
    
    cell.dealImage.image = [[UIManager sharedManager] createWhitePlaceholderImageWithImage:cell.dealImage];
    
    // adds null value inside of array to be populated by the downloaded cached image
    [self.cachedImagesArray insertObject:[NSNull null] atIndex:indexPath.row];
    
    [[APIManager sharedManager] loadImageWithImage:cell.dealImage andImageURL:nodeDictionary[@"image"] success:^(id image) {
        if ([image isKindOfClass:[UIImage class]]) {
            cell.dealImage.image = image;
            [self.cachedImagesArray insertObject:cell.dealImage.image atIndex:cell.dealImage.tag];
        }
    } failure:^(NSError *error) {
        NSLog(@"Error loading image with error: %@", error);
    }];
    
    cell.validityView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.validityView.layer.borderWidth = 1.0;
    cell.validityView.layer.cornerRadius = 10.0;
    cell.validityView.layer.masksToBounds = YES;
    
    cell.redemptionView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.redemptionView.layer.borderWidth = 1.0;
    cell.redemptionView.layer.cornerRadius = 10.0;
    cell.redemptionView.layer.masksToBounds = YES;
    
    cell.purchasedView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.purchasedView.layer.borderWidth = 1.0;
    cell.purchasedView.layer.cornerRadius = 10.0;
    cell.purchasedView.layer.masksToBounds = YES;
    
    cell.titleLabel.text = nodeDictionary[@"name"];
    cell.subtitleLabel.text = [nodeDictionary[@"store_name"] uppercaseString];
    cell.codeLabel.text = nodeDictionary[@"redemption_code"];
    
    if (nodeDictionary[@"redemption_start"] != (id)[NSNull null] || nodeDictionary[@"redemption_end"] != (id)[NSNull null]) {
        CGFloat startRedemptionTime = [nodeDictionary[@"redemption_start"] floatValue];
        CGFloat endRedemptionTime = [nodeDictionary[@"redemption_end"] floatValue];
        cell.validityLabel.text = [NSString stringWithFormat:@"%@ up to %@", [[InfoManager sharedManager] convertUnixTimeToString:startRedemptionTime], [[InfoManager sharedManager] convertUnixTimeToString:endRedemptionTime]];
    } else {
        cell.validityLabel.text = @"N/A";
    }
    
    if (nodeDictionary[@"purchased_date"] != (id)[NSNull null]) {
        CGFloat datePurchased = [nodeDictionary[@"purchased_date"] floatValue];
        cell.dateLabel.text = [[InfoManager sharedManager] convertUnixTimeToString:datePurchased];
    } else {
        cell.dateLabel.text = @"N/A";
    }
    
    cell.termsText.text = nodeDictionary[@"merchant_hotline_info"];
    
    // makes the cell and table view background color look uniform
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    // Animates the table cell
    dispatch_async(dispatch_get_main_queue(), ^{
        CALayer *animationLayer = cell.layer;
        animationLayer.transform = CATransform3DMakeTranslation(0, animationLayer.bounds.size.height/3, 0.0f);
        
        NSTimeInterval animationDuration = 0.5;
        [UIView beginAnimations:nil
                        context:nil];
        
        [UIView setAnimationDuration:animationDuration];
        cell.layer.transform = CATransform3DIdentity;
        cell.layer.opacity = 1.0f;
        
        [UIView commitAnimations];
    });
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
