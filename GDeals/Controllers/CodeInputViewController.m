//
//  CodeInputViewController.m
//  GDeals
//
//  Created by Jay Santos on 10/15/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import "CodeInputViewController.h"

#import "AFNetworking.h"

#import "Flurry.h"
#import "Constants.h"
#import "APIManager.h"
#import "InfoManager.h"
#import "SVProgressHUD.h"
#import "ProfileInputViewController.h"

@interface CodeInputViewController ()

@property (weak, nonatomic) IBOutlet UILabel *numberLabel;

@property (weak, nonatomic) IBOutlet UIButton *resendButton;
@property (weak, nonatomic) IBOutlet UIButton *blueVerifyButton;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *verifyButton;

@property (weak, nonatomic) IBOutlet UITextField *verificationCodeTextField;

@end

@implementation CodeInputViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configureView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    [self.verificationCodeTextField becomeFirstResponder];
    self.verificationCodeTextField.font = kGlobeFontRegular18;
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    _resendButton.layer.cornerRadius = 10;
    _blueVerifyButton.layer.cornerRadius = 10;
    
    self.numberLabel.text = self.mobileNumberString;
}

- (void)resendVerficationCode {
    NSInteger mobileNumberLength = self.mobileNumberString.length;
    
    if (mobileNumberLength != 0) {
        NSString *mobileNumberString = [self.mobileNumberString substringFromIndex:1];
        
        [[APIManager sharedManager] verifyNumberWithMobileNumber:mobileNumberString signature:self.signatureString andVerificationCodeString:self.verificationCodeString success:^(id responseObject) {
            NSLog(@"responseObject: %@", responseObject);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        } failure:^(NSError *error) {
            NSLog(@"Error resending verification code with error: %@", error);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
            });
        }];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:kErrorNumberMessage maskType:SVProgressHUDMaskTypeGradient];
        });
    }
}

#pragma mark - Button methods

- (IBAction)verifyButtonTapped:(UIBarButtonItem *)sender {
    NSDictionary *detailParams = @{kFlurryTapped: kFlurryVerify};
    [Flurry logEvent:kFlurryVerficationCode withParameters:detailParams];
    
    [self.view endEditing:YES];
    
    // bypass verification for the demo accounts using the numbers 09152711295 or 09362575607
    if ([self.numberLabel.text isEqualToString:kDemoAccount1] || [self.numberLabel.text isEqualToString:kDemoAccount2]) {
        if ([self.verificationCodeTextField.text isEqualToString:kDemoAccountPin]) {
            [self performSegueWithIdentifier:@"ShowProfileInputView" sender:nil];
        }
    } else {
        if ([self.verificationCodeTextField.text isEqualToString:kEmptyString]) {
            [SVProgressHUD showErrorWithStatus:kErrorInvalidVerificationCodeMessage maskType:SVProgressHUDMaskTypeGradient];
        } else {
            [SVProgressHUD setFont:kGlobeFontRegular14];
            [SVProgressHUD setForegroundColor:kGlobeBlueColor];
            [SVProgressHUD showWithStatus:kHUDVerifyingCode maskType:SVProgressHUDMaskTypeGradient];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [[APIManager sharedManager] verifyCodeWithMobileNumber:self.numberLabel.text success:^(id responseObject) {
                    if ([self.verificationCodeString isEqualToString:[self.verificationCodeTextField.text uppercaseString]]) {
                        NSLog(@"responseObject: %@", responseObject);
                        
                        if ([responseObject[@"message"] isEqualToString:@"Mobile number does not exists"] && [responseObject[@"status"] isEqualToString:@"Error"]) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [SVProgressHUD dismiss];
                            });
                            
                            // Mobile number doesn't exist. Show profile view
                            [self performSegueWithIdentifier:@"ShowProfileInputView" sender:nil];
                        } else {
                            // Proceed with login
                            [[APIManager sharedManager] loginWithMobileNumber:self.numberLabel.text success:^{
                                
                                [self performSegueWithIdentifier:@"ShowInterestsInputView" sender:nil];
                            } failure:^(NSError *error) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
                                });
                            }];
                            
                            //                    [self performSegueWithIdentifier:@"ShowInterestsInputView" sender:nil];
                        }
                    } else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [SVProgressHUD showErrorWithStatus:kErrorVerificationCodeMessage maskType:SVProgressHUDMaskTypeGradient];
                        });
                    }
                } failure:^(NSError *error) {
                    NSLog(@"Error verifying code with error: %@", error);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
                    });
                }];
            });
        }
    }
}
- (IBAction)blueVerifyButtonTapped:(UIButton *)sender {
    [self.view endEditing:YES];
    
    [self verifyButtonTapped:nil];
}

- (IBAction)resendButtonTapped:(UIButton *)sender {
    [self.view endEditing:YES];
    
    [SVProgressHUD setFont:kGlobeFontRegular14];
    [SVProgressHUD setForegroundColor:kGlobeBlueColor];
    [SVProgressHUD showWithStatus:kHUDResendingCode maskType:SVProgressHUDMaskTypeGradient];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self resendVerficationCode];
    });
}

#pragma mark - Text field methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger oldLength = 0;
    oldLength = [textField.text length];
    
    NSUInteger replacementLength = 0;
    replacementLength = [string length];
    
    NSUInteger rangeLength = 0;
    rangeLength = range.length;
    
    NSUInteger newLength = 0;
    newLength = oldLength - rangeLength + replacementLength;
    
    if (newLength < kMaximumNumberForVerificationCode) {
        self.verifyButton.enabled = NO;
    } else if (newLength >= kMaximumNumberForVerificationCode) {
        self.verifyButton.enabled = YES;
    }
    
    BOOL returnKey = NO;
    returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= kMaximumNumberForVerificationCode || returnKey;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    
    [self verifyButtonTapped:nil];
    
    return YES;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"ShowProfileInputView"]) {
        [[segue destinationViewController] setMobileNumberString:self.numberLabel.text];
    }
}

@end
