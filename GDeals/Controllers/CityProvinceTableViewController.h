//
//  CityProvinceTableViewController.h
//  GDeals
//
//  Created by Jay Santos on 2/19/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CityProvinceTableViewController;

@protocol CityProvinceDelegate <NSObject>

@optional
- (void)setCityProvinceWithCityProvince:(NSString *)cityProvince;

@end

@interface CityProvinceTableViewController : UITableViewController

@property (weak, nonatomic) id <CityProvinceDelegate> cityProvinceDelegate;

@end
