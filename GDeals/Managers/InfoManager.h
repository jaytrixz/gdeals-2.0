//
//  InfoManager.h
//  GDeals
//
//  Created by Jay Santos on 11/6/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonHMAC.h>
#import <CommonCrypto/CommonDigest.h>

#import "Deal.h"
#import "Banner.h"

@interface InfoManager : NSObject

+ (instancetype)sharedManager;

- (NSString *)createSHA1UsingData:(id)data;

- (NSString *)createVerificationCodeWithNumberOfCharacters:(NSInteger)numberOfCharacters;

- (NSString *)encryptStringWithHMACSHA1:(NSString *)dataString
                                withKey:(NSString *)keyString;

- (NSMutableArray *)getDefaultInterests;

- (NSString *)removeZeroFromMobileNumber:(NSString *)numberString;

- (NSString *)formatImageString:(NSString *)imageString;

- (NSAttributedString *)formatOriginalPrice:(NSString *)priceString
                              withLineColor:(UIColor *)color;

- (NSMutableArray *)setupDealsArrayWithDealDictionary:(NSDictionary *)dealDictionary;

- (NSString *)convertUnixTimeToString:(CGFloat)unixTime;

- (NSString *)convertCategoryIDToString:(NSString *)categoryID;

- (NSString *)getCategoryIdentifierWithCategoryName:(NSString *)categoryName;

- (NSString *)getCategoryNameWithCategoryIdentifier:(NSString *)categoryIdentifier;

- (NSString *)createSignatureWithSHA1WithDictionary:(NSDictionary *)detailDictionary;

- (NSString *)getCurrentDate;

- (NSString *)computeTotalAmountWithPrice:(NSString *)price
                              andQuantity:(NSString *)qty;

- (NSString *)formatStringToPrice:(NSString *)price;

- (Deal *)setupDealModelWithDealDictionary:(NSDictionary *)dealDictionary;

- (Banner *)setupBannerModelWithBannerDictionary:(NSDictionary *)bannerDictionary;

@end
