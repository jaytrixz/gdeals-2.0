//
//  PaymentOptionsViewController.m
//  GDeals
//
//  Created by Jay Santos on 2/5/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "PaymentOptionsViewController.h"

#import "Flurry.h"
#import "Constants.h"
#import "APIManager.h"
#import "InfoManager.h"
#import "TotalTableViewCell.h"
#import "VariantTableViewCell.h"
#import "QuantityTableViewCell.h"
#import "BrowserViewController.h"
#import "PaymentOverviewTableViewCell.h"
#import "PaymentDeliveryTableViewCell.h"
#import "SelectionTableViewController.h"

@interface PaymentOptionsViewController () <SelectionDelegate> {
    BOOL isVariantSelected;
    BOOL isVariantAvailable;
}

@property (weak, nonatomic) IBOutlet UITableView *paymentOptionsTable;

@property (strong, nonatomic) NSString *variantString;
@property (strong, nonatomic) NSString *quantityString;
@property (strong, nonatomic) NSString *totalPriceString;
@property (strong, nonatomic) NSString *thirdPartyDealIDString;

@property (assign, nonatomic) NSUInteger selectedIndex;

@property (strong, nonatomic) NSMutableArray *variantsArray;
@property (strong, nonatomic) NSMutableArray *quantitiesArray;
@property (strong, nonatomic) NSMutableArray *thirdPartyDealIDsArray;

@end

@implementation PaymentOptionsViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configureView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self customSetup];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.paymentOptionsTable reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    [self.paymentOptionsTable reloadData];
}

- (void)customSetup {
    self.paymentModel = [[Payment alloc] init];
    
    self.quantitiesArray = [NSMutableArray array];
    self.variantsArray = [NSMutableArray array];
    self.thirdPartyDealIDsArray = [NSMutableArray array];
    
    [self fetchVariantsAndThirdPartyDealIDs];
    
    [self fetchQuantity];
    
    [self applyDefaultValues];
}

- (void)fetchVariantsAndThirdPartyDealIDs {
    NSDictionary *variantsDictionary = self.dealModel.variantsDictionary;
    NSLog(@"variantsDictionary: %@", variantsDictionary);
    NSString *variantKeyString = nil;
    NSArray *variantNamesArray = [NSArray array];
    
    if (variantsDictionary[@"color"]) {
        variantKeyString = @"color";
    } else if (variantsDictionary[@"design"]) {
        variantKeyString = @"design";
    } else if (variantsDictionary[@"pax"]) {
        variantKeyString = @"pax";
    } else if (variantsDictionary[@"size"]) {
        variantKeyString = @"size";
    } else if (variantsDictionary[@"hotel"]) {
        variantKeyString = @"hotel";
    } else if (variantsDictionary[@"length"]) {
        variantKeyString = @"length";
    } else {
        [self.variantsArray addObject:variantsDictionary[@"name"]];
        [self.thirdPartyDealIDsArray addObject:variantsDictionary[@"id"]];
    }
    
    if (variantKeyString) {
        variantNamesArray = variantsDictionary[variantKeyString];
        
        for (NSDictionary *variantDict in variantNamesArray) {
            [self.variantsArray addObject:variantDict[@"name"]];
            [self.thirdPartyDealIDsArray addObject:variantDict[@"id"]];
        }
    }
    
    NSLog(@"self.variantsArray: %@", self.variantsArray);
    NSLog(@"self.thirdPartyDealIDsArray: %@", self.thirdPartyDealIDsArray);
    
    if ([self.variantsArray[0] isEqualToString:kEmptyString]) {
        isVariantAvailable = NO;
    } else {
        isVariantAvailable = YES;
    }
}

- (void)fetchQuantity {
    NSDictionary *variantsDictionary = self.dealModel.variantsDictionary;
    NSString *totalQuantity = nil;
    NSArray *variantArray = [NSArray array];
    self.quantitiesArray = [NSMutableArray array];
    
    if (variantsDictionary[@"color"]) {
        variantArray = variantsDictionary[@"color"];
    } else if (variantsDictionary[@"design"]) {
        variantArray = variantsDictionary[@"design"];
    } else if (variantsDictionary[@"pax"]) {
        variantArray = variantsDictionary[@"pax"];
    } else if (variantsDictionary[@"size"]) {
        variantArray = variantsDictionary[@"size"];
    } else if (variantsDictionary[@"hotel"]) {
        variantArray = variantsDictionary[@"hotel"];
    } else if (variantsDictionary[@"length"]) {
        variantArray = variantsDictionary[@"length"];
    } else {
        totalQuantity = variantsDictionary[@"qty"];
    }
    
    if ([variantArray count] == 1) {
        totalQuantity = variantArray[0][@"qty"];
        [self.quantitiesArray addObject:totalQuantity];
    } else {
        for (NSString *quantity in variantArray) {
            [self.quantitiesArray addObject:quantity];
        }
    }

    NSLog(@"self.quantitiesArray: %@", self.quantitiesArray);
}

- (NSString *)getDefaultPrice {
    NSDictionary *variantsDictionary = self.dealModel.variantsDictionary;
    NSString *variantKey = nil;
    NSString *discountedPrice = nil;
    
    if (variantsDictionary[@"color"]) {
        variantKey = @"color";
    } else if (variantsDictionary[@"design"]) {
        variantKey = @"design";
    } else if (variantsDictionary[@"pax"]) {
        variantKey = @"pax";
    } else if (variantsDictionary[@"size"]) {
        variantKey = @"size";
    } else if (variantsDictionary[@"hotel"]) {
        variantKey = @"hotel";
    } else if (variantsDictionary[@"length"]) {
        variantKey = @"length";
    } else {
        discountedPrice = variantsDictionary[@"discount"];
    }
    
    if (variantKey) {
        discountedPrice = variantsDictionary[variantKey][0][@"discount"];
    }
    
    return discountedPrice;
}

- (void)applyDefaultValues {
    self.quantityString = @"1";
    
    self.totalPriceString = [self getDefaultPrice];
    self.paymentModel.price = self.totalPriceString;
    NSLog(@"Price: %@", self.paymentModel.price);
}

#pragma mark - Selection methods

- (void)updateQuantityWithNewQuantity:(NSString *)qty andIndex:(NSUInteger)index {
    self.quantityString = qty;
}

- (void)updateVariantWithNewVariant:(NSString *)var andIndex:(NSUInteger)index {
    self.selectedIndex = index;
    self.variantString = self.variantsArray[self.selectedIndex];
    self.thirdPartyDealIDString = self.thirdPartyDealIDsArray[self.selectedIndex];
    NSLog(@"self.thirdPartyDealIDString: %@", self.thirdPartyDealIDString);
    self.paymentModel.thirdPartyDealIdentifier = self.thirdPartyDealIDString;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section == 0) {
        return 1;
    } else {
        if (isVariantAvailable) {
            return 4;
        } else {
            return 3;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        PaymentOverviewTableViewCell *cell = (PaymentOverviewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"PaymentOverviewCell" forIndexPath:indexPath];
        [self configurePaymentOverviewCell:cell atIndexPath:indexPath];
        
        return cell;
    } else {
        if (isVariantAvailable) {
            if (indexPath.row == 0) {
                VariantTableViewCell *cell = (VariantTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"VariantCell" forIndexPath:indexPath];
                [self configureVariantCell:cell atIndexPath:indexPath];
                
                return cell;
            } else if (indexPath.row == 1) {
                QuantityTableViewCell *cell = (QuantityTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"QuantityCell" forIndexPath:indexPath];
                [self configureQuantityCell:cell atIndexPath:indexPath];
                
                return cell;
            } else if (indexPath.row == 2) {
                PaymentDeliveryTableViewCell *cell = (PaymentDeliveryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"PaymentDeliveryCell" forIndexPath:indexPath];
                
                return cell;
            } else {
                TotalTableViewCell *cell = (TotalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"TotalCell" forIndexPath:indexPath];
                [self configureTotalCell:cell atIndexPath:indexPath];
                
                return cell;
            }
        } else {
            if (indexPath.row == 0) {
                QuantityTableViewCell *cell = (QuantityTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"QuantityCell" forIndexPath:indexPath];
                [self configureQuantityCell:cell atIndexPath:indexPath];
                
                return cell;
            } else if (indexPath.row == 1) {
                PaymentDeliveryTableViewCell *cell = (PaymentDeliveryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"PaymentDeliveryCell" forIndexPath:indexPath];
                
                return cell;
            } else {
                TotalTableViewCell *cell = (TotalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"TotalCell" forIndexPath:indexPath];
                [self configureTotalCell:cell atIndexPath:indexPath];
                
                return cell;
            }
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.paymentOptionsTable deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            if (isVariantAvailable) {
                isVariantSelected = YES;
            } else {
                isVariantSelected = NO;
            }
            
            [self performSegueWithIdentifier:@"ShowSelections" sender:nil];
        } else if (indexPath.row == 1) {
            if (isVariantAvailable) {
                isVariantSelected = NO;
                
                [self performSegueWithIdentifier:@"ShowSelections" sender:nil];
            }
        }
        
//        if (isVariantAvailable) {
//            if (indexPath.row == 0) {
//                isVariantSelected = YES;
//                
//                [self performSegueWithIdentifier:@"ShowSelections" sender:nil];
//            } else {
//                isVariantSelected = NO;
//            }
//        } else {
//            isVariantSelected = NO;
//        }
//        
//        [self performSegueWithIdentifier:@"ShowSelections" sender:nil];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
        return UITableViewAutomaticDimension;
    }
    
    if (indexPath.section == 0) {
        return 300;
    } else {
        if (indexPath.row == 4) {
            return 150;
        } else {
            return 44;
        }
    }
}

#pragma mark - Cells methods

- (void)configurePaymentOverviewCell:(PaymentOverviewTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.dealImage.image = self.dealModel.cachedImage;
    cell.nameLabel.text = self.dealModel.name;
}

- (void)configureVariantCell:(VariantTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
//    if (self.variantString) {
//        cell.variantLabel.text = self.variantString;
//    } else if ([self.variantsArray count] != 0) {
//        cell.variantLabel.text = self.variantsArray[0];
//    } else {
//        cell.variantLabel.text = kEmptyString;
//    }
    if (self.variantString) {
        self.variantString = self.variantString;
        self.thirdPartyDealIDString = self.thirdPartyDealIDString;
    } else if ([self.variantsArray count] != 0) {
        if (self.selectedIndex) {
            self.variantString = self.variantsArray[self.selectedIndex];
            self.thirdPartyDealIDString = self.thirdPartyDealIDsArray[self.selectedIndex];
        } else {
            self.variantString = self.variantsArray[0];
            self.thirdPartyDealIDString = self.thirdPartyDealIDsArray[0];
        }
    } else {
        self.variantString = kEmptyString;
        self.thirdPartyDealIDString = self.thirdPartyDealIDsArray[0];
    }
    
    cell.variantLabel.text = [self.variantString capitalizedString];
    self.paymentModel.thirdPartyDealIdentifier = self.thirdPartyDealIDString;
    NSLog(@"self.paymentModel.thirdPartyDealIdentifier: %@", self.paymentModel.thirdPartyDealIdentifier);
}

- (void)configureQuantityCell:(QuantityTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {    
    if (self.quantityString) {
        self.quantityString = self.quantityString;
    } else if ([self.quantitiesArray count] != 0) {
        self.quantityString = [NSString stringWithFormat:@"%@", self.quantitiesArray[0]];
    } else {
        self.quantityString = kEmptyString;
    }
    
    cell.quantityLabel.text = self.quantityString;
    self.paymentModel.quantity = self.quantityString;
    
    NSLog(@"self.variantsArray[0]: %@", self.variantsArray[0]);
    if ([self.variantsArray[0] isEqualToString:@""]) {
        self.thirdPartyDealIDString = self.thirdPartyDealIDsArray[0];
        self.paymentModel.thirdPartyDealIdentifier = self.thirdPartyDealIDString;
    }
}

- (void)configureTotalCell:(TotalTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    if (self.dealModel.variantsDictionary) {
        NSDictionary *variantsDictionary = self.dealModel.variantsDictionary;
        
        NSString *amountString = nil;
        [self getDefaultPrice];
        
        NSString *variantKey = nil;
        NSString *discountedPrice = nil;
        if (variantsDictionary[@"color"]) {
            variantKey = @"color";
        } else if (variantsDictionary[@"design"]) {
            variantKey = @"design";
        } else if (variantsDictionary[@"pax"]) {
            variantKey = @"pax";
        } else if (variantsDictionary[@"size"]) {
            variantKey = @"size";
        } else if (variantsDictionary[@"hotel"]) {
            variantKey = @"hotel";
        } else if (variantsDictionary[@"length"]) {
            variantKey = @"length";
        } else {
            discountedPrice = variantsDictionary[@"discount"];
        }
        
        if (variantKey) {
            discountedPrice = variantsDictionary[variantKey][0][@"discount"];
        }
        
        NSString *quantity = self.quantityString;
        
        self.paymentModel.price = discountedPrice;
        amountString = [[InfoManager sharedManager] computeTotalAmountWithPrice:discountedPrice andQuantity:quantity];
        
        // set total price here without commas format
        self.totalPriceString = amountString;
        amountString = [[InfoManager sharedManager] formatStringToPrice:amountString];
        cell.amountLabel.text = [NSString stringWithFormat:@"₱ %@", amountString];
    } else {
        self.totalPriceString = kEmptyString;
        cell.amountLabel.text = self.totalPriceString;
        self.paymentModel.price = self.totalPriceString;
    }
    
    NSLog(@"Total price: %@", self.totalPriceString);
//    self.paymentModel.price = self.totalPriceString;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"ShowSelections"]) {
        [segue.destinationViewController setSelectionDelegate:self];
        
        if (isVariantSelected) {
            [segue.destinationViewController setSelectionsArray:self.variantsArray];
        } else {
            NSMutableArray *iterationArray = [NSMutableArray array];
            if ([self.quantitiesArray count] != 0) {
                for (int i = 0; i < [self.quantitiesArray[self.selectedIndex] integerValue]; i++) {
                    [iterationArray addObject:[NSString stringWithFormat:@"%d", i + 1]];
                }
            } else {
                for (int i = 0; i < 10; i++) {
                    [iterationArray addObject:[NSString stringWithFormat:@"%d", i + 1]];
                }
            }
            
            [segue.destinationViewController setSelectionsArray:iterationArray];
        }
        
        [segue.destinationViewController setIsVariantSelected:isVariantSelected];
    } else if ([segue.identifier isEqualToString:@"ShowDelivery"]) {
        // Added flurry event here since coontinue button is wired in interface builder without code but this
        NSDictionary *detailParams = @{kFlurryTapped: kFlurryContinue};
        [Flurry logEvent:kFlurryCartOkayOkay withParameters:detailParams];
        
        [segue.destinationViewController setDealModel:self.dealModel];
        [segue.destinationViewController setPaymentModel:self.paymentModel];
    }
}

@end
