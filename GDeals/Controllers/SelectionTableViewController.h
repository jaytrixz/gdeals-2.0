//
//  SelectionTableViewController.h
//  GDeals
//
//  Created by Jay Santos on 2/16/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SelectionTableViewController;

@protocol SelectionDelegate <NSObject>

@optional
- (void)updateQuantityWithNewQuantity:(NSString *)qty andIndex:(NSUInteger)index;
- (void)updateVariantWithNewVariant:(NSString *)var andIndex:(NSUInteger)index;

@end

@interface SelectionTableViewController : UITableViewController

@property (weak, nonatomic) id <SelectionDelegate> selectionDelegate;

@property (assign, nonatomic) BOOL isVariantSelected;

@property (strong, nonatomic) NSArray *selectionsArray;
@property (strong, nonatomic) NSArray *participatingBranchesArray;

@property (assign, nonatomic) NSUInteger selectedIndex;

@end
