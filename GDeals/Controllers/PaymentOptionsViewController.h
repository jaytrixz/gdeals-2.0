//
//  PaymentOptionsViewController.h
//  GDeals
//
//  Created by Jay Santos on 2/5/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Deal.h"

#import "Payment.h"

@interface PaymentOptionsViewController : UIViewController

@property (strong, nonatomic) Deal *dealModel;

@property (strong, nonatomic) Payment *paymentModel;

@end
