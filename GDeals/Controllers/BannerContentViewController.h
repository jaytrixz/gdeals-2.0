//
//  BannerContentViewController.h
//  GDeals
//
//  Created by Jay Santos on 1/8/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Deal.h"
#import "Banner.h"

@interface BannerContentViewController : UIViewController

@property (assign, nonatomic) NSUInteger bannerIndex;

@property (strong, nonatomic) Deal *dealModel;

@property (strong, nonatomic) Banner *bannerModel;

@end
