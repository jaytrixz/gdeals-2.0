//
//  UnavailableTableViewCell.m
//  GDeals
//
//  Created by Jay Santos on 2/23/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "UnavailableTableViewCell.h"

@implementation UnavailableTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
