//
//  OverviewRewardsTableViewCell.h
//  GDeals
//
//  Created by Jay Santos on 2/3/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OverviewRewardsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *dealImage;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UIImageView *gradientImage;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalBoughtLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPointsLabel;

@end
