//
//  User.h
//  GDeals
//
//  Created by Jay Santos on 11/12/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *birthday;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *defaultDeliveryAddress;

@property (strong, nonatomic) NSMutableArray *interests;

@end
