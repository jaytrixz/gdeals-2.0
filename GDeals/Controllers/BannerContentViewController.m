//
//  BannerContentViewController.m
//  GDeals
//
//  Created by Jay Santos on 1/8/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "BannerContentViewController.h"

#import "Constants.h"
#import "APIManager.h"
#import "BannerDealsTableViewController.h"

@interface BannerContentViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *bannerImage;

@property (strong, nonatomic) UIImage *otherImage;

@end

@implementation BannerContentViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configureView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    
}

- (void)customSetup {
    if ([self.bannerModel.imageURL isEqualToString:@"none"]) {
        UIImageView *whiteImage = [[UIImageView alloc] init];
        whiteImage.backgroundColor = [UIColor whiteColor];
        self.bannerImage.image = whiteImage.image;
    } else {
        self.otherImage = nil;
        
        NSLog(@"self.dealModel.bannerImageURL: %@", self.dealModel.bannerImageURL);
        
        if (self.bannerModel.imageURL) {
            [[APIManager sharedManager] loadImageWithImage:self.bannerImage andImageURL:self.bannerModel.imageURL success:^(id image) {
                if ([image isKindOfClass:[UIImage class]]) {
                    self.bannerImage.image = image;
                    self.bannerImage.tag = self.bannerIndex;
                    
                    UITapGestureRecognizer *tapBannerImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bannerImageTapped:)];
                    [self.bannerImage addGestureRecognizer:tapBannerImage];
                }
            } failure:^(NSError *error) {
                NSLog(@"Error loading image with error: %@", error);
            }];
        } else {
            [[APIManager sharedManager] loadImageWithImage:self.bannerImage andImageURL:self.dealModel.imageURL success:^(id image) {
                if ([image isKindOfClass:[UIImage class]]) {
                    NSLog(@"Loaded other image");
                    self.otherImage = image;
                    
                    [[APIManager sharedManager] loadImageWithImage:self.bannerImage andImageURL:self.dealModel.bannerImageURL success:^(id image) {
                        if ([image isKindOfClass:[UIImage class]]) {
                            self.bannerImage.image = image;
                            self.bannerImage.tag = self.bannerIndex;
                            
                            UITapGestureRecognizer *tapBannerImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bannerImageTapped:)];
                            [self.bannerImage addGestureRecognizer:tapBannerImage];
                        }
                    } failure:^(NSError *error) {
                        NSLog(@"Error loading image with error: %@", error);
                    }];
                }
            } failure:^(NSError *error) {
                NSLog(@"Error loading other image with error: %@", error);
            }];
        }
    }
}

- (void)bannerImageTapped:(UIGestureRecognizer *)gestureRecognizer {
    NSLog(@"Banner tapped at index: %ld", (long)gestureRecognizer.view.tag);
    
    if (self.bannerModel) {
        [self performSegueWithIdentifier:@"ShowBannerDeals" sender:nil];
    } else {
        [self performSegueWithIdentifier:@"ShowBannerDetails" sender:nil];
    }

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"ShowBannerDeals"]) {
        [[segue destinationViewController] setBannerModel:self.bannerModel];
    } else {
        if (self.otherImage) {
            self.dealModel.cachedImage = self.otherImage;
        } else {
            self.dealModel.cachedImage = nil;
        }
        
        [[segue destinationViewController] setDealModel:self.dealModel];
    }
}


@end
