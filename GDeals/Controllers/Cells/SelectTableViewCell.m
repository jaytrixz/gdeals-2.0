//
//  SelectTableViewCell.m
//  GDeals
//
//  Created by Jay Santos on 2/25/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "SelectTableViewCell.h"

@implementation SelectTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
