//
//  CoreDataManager.h
//  GDeals
//
//  Created by Jay Santos on 11/25/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>

@interface CoreDataManager : NSObject {
    NSManagedObjectContext *context;
    NSManagedObjectModel *model;
}

+ (instancetype)sharedManager;

- (void)saveCurrentUserWithUserDictionary:(NSDictionary *)currentUser;

- (void)saveAccessTokenWithAccessToken:(NSString *)accessToken;

- (NSDictionary *)fetchCurrentUserDictionary;

- (NSString *)fetchCurrentUserAccessToken;

- (NSString *)fetchClientVersion;

- (void)saveClientVersionToSettingsWithClientVersionString:(NSString *)clientVersionString;

- (void)saveCategoriesToSettingsWithCategoriesArray:(NSArray *)categoriesArray;

- (NSArray *)fetchCategories;

- (NSDictionary *)fetchUserDetails;

- (void)saveUserDetailsWithFirstName:(NSString *)firstName
                         andLastName:(NSString *)lastName
                         andNickname:(NSString *)nickName
                         andBirthday:(NSString *)birthday
                           andGender:(NSString *)gender;

- (void)deleteAllObjectsInCoreData;

@end
