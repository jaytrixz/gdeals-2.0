//
//  QRCodeScannerViewController.h
//  GDeals
//
//  Created by Jay Santos on 2/6/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#import "Deal.h"

@interface QRCodeScannerViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate>

@property (strong, nonatomic) Deal *dealModel;

@property (strong, nonatomic) NSString *latitudeString;
@property (strong, nonatomic) NSString *longitudeString;
@property (strong, nonatomic) NSString *rewardsPointsString;


@end
