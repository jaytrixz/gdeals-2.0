//
//  CategoryDealsTableViewController.m
//  GDeals
//
//  Created by Jay Santos on 1/28/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "CategoryDealsTableViewController.h"

#import "Deal.h"
#import "Constants.h"
#import "UIManager.h"
#import "APIManager.h"
#import "InfoManager.h"
#import "SVProgressHUD.h"
#import "SVPullToRefresh.h"
#import "EmptyTableViewCell.h"
#import "BannerTableViewCell.h"
#import "UnavailableTableViewCell.h"
#import "CategoryDealTableViewCell.h"
#import "DealDetailsViewController.h"
#import "SearchTableViewController.h"
#import "BannerContentViewController.h"

@interface CategoryDealsTableViewController () {
    BOOL isTableEmpty;
    BOOL areDealsUnavailable;
    
    NSTimer *bannerTimer;
    NSUInteger currentBannerIndex;
}

@property (strong, nonatomic) Deal *dealModel;

@property (weak, nonatomic) IBOutlet UISearchBar *dealsSearchBar;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuBarButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *searchBarButtonItem;

@property (strong, nonatomic) NSString *categoryIDString;

@property (assign, nonatomic) NSUInteger landingPageIndex;

@property (strong, nonatomic) NSMutableArray *allCategoryDealsArray;
@property (strong, nonatomic) NSMutableArray *cachedDealImagesArray;
@property (strong, nonatomic) NSMutableArray *allBannersAndPromosArray;

@property (strong, nonatomic) UIPageViewController *pageViewController;

@end

@implementation CategoryDealsTableViewController

- (void)viewWillAppear:(BOOL)animated {
    if (bannerTimer) {
        [bannerTimer invalidate];
        bannerTimer = nil;
    }
    
    [self customSetup];
    
    // reapply banner timer
    bannerTimer = [NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(switchToNextBanner) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    if (bannerTimer) {
        [bannerTimer invalidate];
        bannerTimer = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    self.navigationItem.titleView = [[UIManager sharedManager] formatNavigationBarWithNavigationBar:self.navigationController.navigationBar andTitleText:self.navigationController.title];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.delegate = self;
    if (revealViewController) {
        [_menuBarButtonItem setTarget:self.revealViewController];
        [_menuBarButtonItem setAction:@selector(revealToggle:)];
        [self.navigationController.navigationBar addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    // removes the search bar in the table
    self.tableView.tableHeaderView = nil;
}

- (void)customSetup {
    self.categoryIDString = [[InfoManager sharedManager] getCategoryIdentifierWithCategoryName:self.navigationController.title];
    NSLog(@"Category ID: %@", self.categoryIDString);
    
    if (!self.allBannersAndPromosArray) {
        self.allBannersAndPromosArray = [NSMutableArray array];
        self.allCategoryDealsArray = [NSMutableArray array];
        self.cachedDealImagesArray = [NSMutableArray array];
        
        // Needed to call private methods inside of blocks
        __weak CategoryDealsTableViewController *weakSelf = self;
        
        [self.tableView addPullToRefreshWithActionHandler:^{
            [weakSelf fetchCategoryDeals];
        }];
        
        [self.tableView addInfiniteScrollingWithActionHandler:^{
            [weakSelf fetchMoreCategoryDeals];
        }];
        
        [self.tableView.pullToRefreshView setTitle:@"Loading Deals" forState:SVPullToRefreshStateLoading];
        [self.tableView.pullToRefreshView setTitle:@"Pull to Refresh Deals" forState:SVPullToRefreshStateStopped];
        [self.tableView.pullToRefreshView setTitle:@"Release to Refresh Deals" forState:SVPullToRefreshStateTriggered];
        [self.tableView.pullToRefreshView setTextColor:kGlobeBlueColor];
        [self.tableView.pullToRefreshView setActivityIndicatorViewColor:kGlobeBlueColor];
        [self.tableView.pullToRefreshView setArrowColor:kGlobeBlueColor];
        
        [SVProgressHUD setFont:kGlobeFontRegular14];
        [SVProgressHUD setForegroundColor:kGlobeBlueColor];
        [SVProgressHUD showWithStatus:kHUDLoadingBanners maskType:SVProgressHUDMaskTypeGradient];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self fetchBanners];
        });
    }
}

- (BannerContentViewController *)loadBannerViewAtIndex:(NSUInteger)index {
//    BannerContentViewController *bannerContentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BannerContentViewController"];
//    NSUInteger bannerCount = [_allBannersAndPromosArray count];
//    if (bannerCount == 0) {
//        bannerContentVC.bannerIndex = index;
//        bannerContentVC.bannerModel = [[InfoManager sharedManager] setupBannerModelWithBannerDictionary:nil];
//    } else if (index >= bannerCount) {
//        bannerContentVC.bannerIndex = 0;
//        bannerContentVC.bannerModel = [[InfoManager sharedManager] setupBannerModelWithBannerDictionary:self.allBannersAndPromosArray[0]];
//    } else {
//        bannerContentVC.bannerIndex = index;
//        bannerContentVC.bannerModel = [[InfoManager sharedManager] setupBannerModelWithBannerDictionary:self.allBannersAndPromosArray[index]];
//    }
//    
//    return bannerContentVC;
    
    
    NSLog(@"Index: %lu", (unsigned long)index);
    BannerContentViewController *bannerContentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BannerContentViewController"];
    NSUInteger bannerCount = [_allBannersAndPromosArray count];
    NSDictionary *bannerDictionary = nil;
    if (bannerCount == 0) {
        bannerContentVC.bannerIndex = index;
        bannerContentVC.bannerModel = [[InfoManager sharedManager] setupBannerModelWithBannerDictionary:nil];
        
        currentBannerIndex = 0;
    } else if (index >= bannerCount) {
        bannerDictionary = self.allBannersAndPromosArray[0];
        
        if (bannerDictionary[@"detail"]) {
            bannerContentVC.bannerIndex = 0;
            bannerContentVC.bannerModel = [[InfoManager sharedManager] setupBannerModelWithBannerDictionary:bannerDictionary];
        } else {
            bannerContentVC.bannerIndex = 0;
            bannerContentVC.dealModel = [[InfoManager sharedManager] setupDealModelWithDealDictionary:bannerDictionary];
        }
        
        currentBannerIndex = 1;
    } else {
        bannerDictionary = self.allBannersAndPromosArray[index];
        
        if (bannerDictionary[@"detail"]) {
            bannerContentVC.bannerIndex = index;
            bannerContentVC.bannerModel = [[InfoManager sharedManager] setupBannerModelWithBannerDictionary:bannerDictionary];
        } else {
            bannerContentVC.bannerIndex = index;
            bannerContentVC.dealModel = [[InfoManager sharedManager] setupDealModelWithDealDictionary:bannerDictionary];
        }
        
        currentBannerIndex = index + 1;
    }
    
    NSLog(@"Current Banner Index: %lu", (unsigned long)currentBannerIndex);
    
    return bannerContentVC;
}

- (void)fetchBanners {
    isTableEmpty = NO;
    areDealsUnavailable = NO;
    
    if (bannerTimer) {
        [bannerTimer invalidate];
        bannerTimer = nil;
    }
    
    [[APIManager sharedManager] getBanners:^(NSDictionary *bannersDictionary) {
        isTableEmpty = NO;
        areDealsUnavailable = NO;
        
        NSArray *bannersArray = bannersDictionary[@"banner"];
        NSArray *heroDealsArray = bannersDictionary[@"hero_deal"];
        for (NSDictionary *bannerDictionary in bannersArray) {
            [self.allBannersAndPromosArray addObject:bannerDictionary];
        }
        
        for (NSDictionary *heroDealDictionary in heroDealsArray) {
            [self.allBannersAndPromosArray addObject:heroDealDictionary];
        }
        
        BannerContentViewController *startingVC = [self loadBannerViewAtIndex:0];
        NSArray *viewControllers = @[startingVC];
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        self.pageViewController.dataSource = nil;
        self.pageViewController.dataSource = self;
        
//        NSLog(@"allBannersAndDealsArray: %@", self.allBannersAndPromosArray);
        
        [self.tableView triggerPullToRefresh];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        
        if (bannerTimer) {
            [bannerTimer invalidate];
            bannerTimer = nil;
        }
        
        bannerTimer = [NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(switchToNextBanner) userInfo:nil repeats:YES];
    } failure:^(NSError *error) {
        NSLog(@"Error loading banners with error: %@", error);
        
        isTableEmpty = YES;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
        });
    }];
}

- (void)fetchCategoryDeals {
    isTableEmpty = NO;
    areDealsUnavailable = NO;
    
    if ([self.allBannersAndPromosArray count] == 0) {
        [self fetchBanners];
    }
    
    NSDictionary *parameters = @{@"action": @"00", @"page_legend": @"LP", @"strip_html_tags": @"1", @"page": @"0", @"category_id_new": self.categoryIDString};
    [[APIManager sharedManager] getDealsWithParameters:parameters success:^(NSDictionary *dealsDictionary) {
        NSLog(@"Deal dictionary: %@", dealsDictionary);
        
        if ([dealsDictionary count] != 0) {
            self.allCategoryDealsArray = [[[InfoManager sharedManager] setupDealsArrayWithDealDictionary:dealsDictionary] mutableCopy];
            
            self.landingPageIndex++;
        } else {
            areDealsUnavailable = YES;
        }
        
        [self.tableView reloadData];
        [self.tableView.pullToRefreshView stopAnimating];
    } failure:^(NSError *error) {
        [self.tableView.pullToRefreshView stopAnimating];
        
        NSLog(@"Error loading deals with error: %@", error);
        
        isTableEmpty = YES;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
        });
    }];
}

- (void)fetchMoreCategoryDeals {
    NSDictionary *parameters = @{@"action": @"00", @"page_legend": @"LP", @"strip_html_tags": @"1", @"page": [NSString stringWithFormat:@"%lu", (unsigned long)self.landingPageIndex], @"category_id_new": self.categoryIDString};
    [[APIManager sharedManager] getDealsWithParameters:parameters success:^(NSDictionary *dealsDictionary) {
        NSLog(@"Deal dictionary: %@", dealsDictionary);
        
        if ([dealsDictionary count] != 0) {
            NSArray *newDealsArray = [[InfoManager sharedManager] setupDealsArrayWithDealDictionary:dealsDictionary];
            NSMutableArray *indexPathsArray = [NSMutableArray array];
            
            NSUInteger previousArrayCount = [_allCategoryDealsArray count];
            for (int i = 0; i < [newDealsArray count]; i++) {
                [indexPathsArray addObject:[NSIndexPath indexPathForRow:previousArrayCount + i inSection:1]];
            }
            
            [self.allCategoryDealsArray addObjectsFromArray:newDealsArray];
            
            [self.tableView beginUpdates];
            [self.tableView insertRowsAtIndexPaths:indexPathsArray withRowAnimation:UITableViewRowAnimationTop];
            [self.tableView endUpdates];
            
            self.landingPageIndex++;
        } else {
            
        }
        
        [self.tableView.infiniteScrollingView stopAnimating];
    } failure:^(NSError *error) {
        NSLog(@"Error loading more deals with error: %@", error);
        
        [self.tableView.infiniteScrollingView stopAnimating];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
        });
    }];
}

- (void)switchToNextBanner {
    BannerContentViewController *bannerVC = [self loadBannerViewAtIndex:currentBannerIndex];
    NSArray *viewControllers = @[bannerVC];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

#pragma mark - Button methods

- (IBAction)searchButtonTapped:(UIBarButtonItem *)sender {
    // re-enables the search bar IBOutlet to the table header
    self.tableView.tableHeaderView = self.dealsSearchBar;
    
    [self.dealsSearchBar becomeFirstResponder];
}

#pragma mark - Search methods

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    self.tableView.tableHeaderView = nil;
    
    [self.dealsSearchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search bar text: %@", searchBar.text);
    
    [self performSegueWithIdentifier:@"ShowSearchView" sender:nil];
}

#pragma mark - Page control methods

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    NSUInteger index = ((BannerContentViewController *) viewController).bannerIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    if (index == 0) {
        index = [_allBannersAndPromosArray count] - 1;
    } else {
        index--;
    }
    
    return [self loadBannerViewAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    NSUInteger index = ((BannerContentViewController *) viewController).bannerIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    if (index == [_allBannersAndPromosArray count]) {
        index = 0;
    } else {
        index++;
    }
    
    return [self loadBannerViewAtIndex:index];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return 0;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    if (isTableEmpty || areDealsUnavailable) {
        return 1;
    } else {
        return 2;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        // to prevent seeing No Results text in search table
        return 1;
    } else {
        if (isTableEmpty || areDealsUnavailable) {
            return 1;
        } else {
            if (section == 0) {
                return 1;
            } else {
                return [_allCategoryDealsArray count];
            }
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
            cell.userInteractionEnabled = NO;
        }
        
        return cell;
    } else {
        if (isTableEmpty) {
            EmptyTableViewCell *cell = (EmptyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"EmptyCell" forIndexPath:indexPath];
            [self configureEmptyCell:cell atIndexPath:indexPath];
            
            return cell;
        } else if (areDealsUnavailable) {
            UnavailableTableViewCell *cell = (UnavailableTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"UnavailCell" forIndexPath:indexPath];
            [self configureUnavailableCell:cell atIndexPath:indexPath];
            
            return cell;
        } else {
            if (indexPath.section == 0) {
                BannerTableViewCell *cell = (BannerTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"BannerCell" forIndexPath:indexPath];
                [self configureBannerCell:cell atIndexPath:indexPath];
                
                return cell;
            } else {
                CategoryDealTableViewCell *cell = (CategoryDealTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CategoryDealCell" forIndexPath:indexPath];
                [self configureCategoryCell:cell atIndexPath:indexPath];
                
                return cell;
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return 44;
    } else {
        if (isTableEmpty || areDealsUnavailable) {
            return 500;
        } else {
            if (indexPath.section == 0) {
                return kMainBannerRowHeight;
            } else {
                return kCategoryTableRowHeight;
            }
        }
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Cell methods

- (void)configureBannerCell:(BannerTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BannerViewController"];
    self.pageViewController.dataSource = self;
    
    BannerContentViewController *startingVC = [self loadBannerViewAtIndex:0];
    NSArray *viewControllers = @[startingVC];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    self.pageViewController.view.frame = CGRectMake(0, 0, cell.contentView.frame.size.width, cell.contentView.frame.size.height);
    
    [self addChildViewController:_pageViewController];
    [cell.contentView addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
//    cell.tag = startingVC.bannerIndex;
//    UITapGestureRecognizer *tapBannerImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bannerImageTapped:)];
//    [cell addGestureRecognizer:tapBannerImage];
}

- (void)configureCategoryCell:(CategoryDealTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    self.dealModel = [[Deal alloc] init];
    self.dealModel = self.allCategoryDealsArray[indexPath.row];
    
    self.dealModel.originalPrice = [[InfoManager sharedManager] formatStringToPrice:self.dealModel.originalPrice];
    self.dealModel.discountedPrice = [[InfoManager sharedManager] formatStringToPrice:self.dealModel.discountedPrice];
    
//    // removes gradient sublayer to prevent overlapping
//    cell.dealImage.layer.sublayers = nil;
    
    // create white image as placeholder image
    cell.dealImage.image = [[UIManager sharedManager] createWhitePlaceholderImageWithImage:cell.dealImage];
    cell.dealImage.tag = indexPath.row;
    
    // adds null value inside of array to be populated by the downloaded cached image
    [self.cachedDealImagesArray insertObject:[NSNull null] atIndex:indexPath.row];
    
    [[APIManager sharedManager] loadImageWithImage:cell.dealImage andImageURL:self.dealModel.imageURL success:^(id image) {
        if ([image isKindOfClass:[UIImage class]]) {
            self.dealModel.cachedImage = image;
            cell.dealImage.image = self.dealModel.cachedImage;
            [self.cachedDealImagesArray insertObject:cell.dealImage.image atIndex:cell.dealImage.tag];
        }
    } failure:^(NSError *error) {
        NSLog(@"Error loading image with error: %@", error);
    }];
    
    cell.discountLabel.text = [NSString stringWithFormat:@"save %@%@", self.dealModel.discount, @"%"];
    cell.titleLabel.text = self.dealModel.name;
    cell.subtitleLabel.text = self.dealModel.sourceText;
    cell.originalPriceLabel.attributedText = [[InfoManager sharedManager] formatOriginalPrice:self.dealModel.originalPrice withLineColor:[UIColor whiteColor]];
    cell.discountedPriceLabel.text = [NSString stringWithFormat:@"₱ %@", self.dealModel.discountedPrice];
    
    // Animates the table cell
    dispatch_async(dispatch_get_main_queue(), ^{
        CALayer *animationLayer = cell.layer;
        animationLayer.transform = CATransform3DMakeTranslation(0, animationLayer.bounds.size.height/3, 0.0f);
        
        NSTimeInterval animationDuration = 0.5;
        [UIView beginAnimations:nil
                        context:nil];
        
        [UIView setAnimationDuration:animationDuration];
        cell.layer.transform = CATransform3DIdentity;
        cell.layer.opacity = 1.0f;
        
        [UIView commitAnimations];
    });
}

- (void)configureEmptyCell:(EmptyTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.titleLabel.text = kErrorNoInternet;
    
    // makes the cell and table view background color look uniform
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    // Animates the table cell
    dispatch_async(dispatch_get_main_queue(), ^{
        CALayer *animationLayer = cell.layer;
        animationLayer.transform = CATransform3DMakeTranslation(0, animationLayer.bounds.size.height/3, 0.0f);
        
        NSTimeInterval animationDuration = 0.5;
        [UIView beginAnimations:nil
                        context:nil];
        
        [UIView setAnimationDuration:animationDuration];
        cell.layer.transform = CATransform3DIdentity;
        cell.layer.opacity = 1.0f;
        
        [UIView commitAnimations];
    });
}

- (void)configureUnavailableCell:(UnavailableTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.titleLabel.text = kErrorNoDealsFound;
    
    // makes the cell and table view background color look uniform
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    // Animates the table cell
    dispatch_async(dispatch_get_main_queue(), ^{
        CALayer *animationLayer = cell.layer;
        animationLayer.transform = CATransform3DMakeTranslation(0, animationLayer.bounds.size.height/3, 0.0f);
        
        NSTimeInterval animationDuration = 0.5;
        [UIView beginAnimations:nil
                        context:nil];
        
        [UIView setAnimationDuration:animationDuration];
        cell.layer.transform = CATransform3DIdentity;
        cell.layer.opacity = 1.0f;
        
        [UIView commitAnimations];
    });
}

#pragma mark - SWRevealViewController methods

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:    (FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:    (FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ShowCategoryDealDetails"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        self.dealModel = [[Deal alloc] init];
        self.dealModel = self.allCategoryDealsArray[indexPath.row];
        self.dealModel.cachedImage = self.cachedDealImagesArray[indexPath.row];
        
        [[segue destinationViewController] setDealModel:self.dealModel];
    } else {
        [segue.destinationViewController setSearchString:self.dealsSearchBar.text];
    }
}

@end
