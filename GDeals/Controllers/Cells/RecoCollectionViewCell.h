//
//  RecoCollectionViewCell.h
//  GDeals
//
//  Created by Jay Santos on 2/3/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecoCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *originalPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountedPriceLabel;

@property (weak, nonatomic) IBOutlet UIImageView *dealImage;

@end
