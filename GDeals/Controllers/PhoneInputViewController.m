//
//  PhoneInputViewController.m
//  GDeals
//
//  Created by Jay Santos on 10/15/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import "PhoneInputViewController.h"

#import "AFNetworking.h"

#import "Flurry.h"
#import "Constants.h"
#import "UIManager.h"
#import "APIManager.h"
#import "InfoManager.h"
#import "SVProgressHUD.h"
#import "CodeInputViewController.h"

@interface PhoneInputViewController ()

@property (strong, nonatomic) NSString *signatureString;
@property (strong, nonatomic) NSString *verificationCodeString;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;

@property (weak, nonatomic) IBOutlet UITextField *numberTextField;

@property (weak, nonatomic) IBOutlet UITextView *termsAndConditionsTextView;


@end

@implementation PhoneInputViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configureView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self getMobileNumber];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    [self.numberTextField becomeFirstResponder];
    self.numberTextField.font = kGlobeFontRegular18;
    
    self.navigationItem.titleView = [[UIManager sharedManager] formatNavigationBarWithLogoAndNavigationBar:self.navigationController.navigationBar andTitleText:@"Your Phone"];
    
    self.termsAndConditionsTextView = [[UIManager sharedManager] formatTermsAndConditionsWithTextView:self.termsAndConditionsTextView];
}

- (void)getMobileNumber {
    [[APIManager sharedManager] getMobileNumber:^(NSString *mobileNumber) {
        self.numberTextField.text = mobileNumber;
    } failure:^(NSError *error) {
        NSLog(@"Error getting mobile number with error: %@", error);
    }];
}

- (void)verifyNumber {
    
    NSInteger mobileNumberLength = self.numberTextField.text.length;
    
    if (mobileNumberLength != 0) {
        NSString *mobileNumberString = [self.numberTextField.text substringFromIndex:1];
        
        self.verificationCodeString = [[InfoManager sharedManager] createVerificationCodeWithNumberOfCharacters:4];
        
        NSDictionary *parameters = @{@"mobile_number":mobileNumberString, @"verification_code": self.verificationCodeString};
        
        self.signatureString = [[InfoManager sharedManager] createSHA1UsingData:parameters];
        
        [[APIManager sharedManager] verifyNumberWithMobileNumber:mobileNumberString signature:self.signatureString andVerificationCodeString:self.verificationCodeString success:^(id responseObject) {
            NSLog(@"responseObject: %@", responseObject);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            
            [self performSegueWithIdentifier:@"ShowCodeInputView" sender:nil];
        } failure:^(NSError *error) {
            NSLog(@"Error verifying mobile number with error: %@", error);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
            });
        }];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:kErrorNumberMessage maskType:SVProgressHUDMaskTypeGradient];
        });
    }
}

#pragma mark - Button methods

- (IBAction)doneButtonTapped:(UIBarButtonItem *)sender {
    NSDictionary *detailParams = @{kFlurryTapped: kFlurryDone};
    [Flurry logEvent:kFlurryMobileRegistration withParameters:detailParams];
    
    [self.view endEditing:YES];
    
    [SVProgressHUD setFont:kGlobeFontRegular14];
    [SVProgressHUD setForegroundColor:kGlobeBlueColor];
    [SVProgressHUD showWithStatus:kHUDVerifyingNumber maskType:SVProgressHUDMaskTypeGradient];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self verifyNumber];
    });
}

#pragma mark - Text field methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger oldLength = [textField.text length];
    
    NSUInteger replacementLength = [string length];
    
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    NSLog(@"Text length: %lu", (unsigned long)newLength);
    if (newLength < kMaximumNumberForMobileNumber) {
        self.doneButton.enabled = NO;
    } else if (newLength >= kMaximumNumberForMobileNumber) {
        self.doneButton.enabled = YES;
    }
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= kMaximumNumberForMobileNumber || returnKey;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"ShowCodeInputView"]) {
        [segue.destinationViewController setMobileNumberString:self.numberTextField.text];
        [segue.destinationViewController setVerificationCodeString:self.verificationCodeString];
        [segue.destinationViewController setSignatureString:self.signatureString];
    }
}

@end
