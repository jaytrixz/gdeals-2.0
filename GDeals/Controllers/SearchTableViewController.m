//
//  SearchTableViewController.m
//  GDeals
//
//  Created by Jay Santos on 2/13/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "SearchTableViewController.h"

#import "Deal.h"
#import "Constants.h"
#import "UIManager.h"
#import "APIManager.h"
#import "InfoManager.h"
#import "SVProgressHUD.h"
#import "MainTableViewCell.h"
#import "EmptyTableViewCell.h"
#import "EmptyTableViewCell.h"
#import "UnavailableTableViewCell.h"
#import "DealDetailsViewController.h"

@interface SearchTableViewController () {
    BOOL isTableEmpty;
    BOOL areDealsUnavailable;
}

@property (strong, nonatomic) Deal *dealModel;

@property (strong, nonatomic) NSMutableArray *searchResultsArray;

@end

@implementation SearchTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self configureView];
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    self.navigationController.navigationBar.tintColor = kGlobeLightBlueColor;
}

- (void)customSetup {
    self.searchResultsArray = [NSMutableArray array];
    
    [self searchDeal];
}

- (void)searchDeal {
    isTableEmpty = NO;
    areDealsUnavailable = NO;
    
    [SVProgressHUD setFont:kGlobeFontRegular14];
    [SVProgressHUD setForegroundColor:kGlobeBlueColor];
    [SVProgressHUD showWithStatus:kHUDSearchingDeals maskType:SVProgressHUDMaskTypeGradient];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDictionary *parameters = @{@"search_text": self.searchString, @"strip_html_tags": @"1"};
        [[APIManager sharedManager] searchDealWithParameters:parameters success:^(NSDictionary *resultsDictionary) {
            NSLog(@"resultsDictionary: %@", resultsDictionary);
            
            if ([resultsDictionary[@"deals"] count] != 0) {
                isTableEmpty = NO;
                
                self.searchResultsArray = [[InfoManager sharedManager] setupDealsArrayWithDealDictionary:resultsDictionary];
            } else {
                areDealsUnavailable = YES;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            
            [self.tableView reloadData];
        } failure:^(NSError *error) {
            NSLog(@"Error searching deal with error: %@", error);
            
            isTableEmpty = YES;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
            });
        }];
    });
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (isTableEmpty || areDealsUnavailable) {
        return 1;
    } else {
        return [_searchResultsArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Configure the cell...
    if (isTableEmpty) {
        EmptyTableViewCell *cell = (EmptyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"EmptyCell" forIndexPath:indexPath];
        
        [self configureEmptyCell:cell atIndexPath:indexPath];
        
        return cell;
    } else if (areDealsUnavailable) {
        UnavailableTableViewCell *cell = (UnavailableTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"UnavailCell" forIndexPath:indexPath];
        
        [self configureUnavailableCell:cell atIndexPath:indexPath];
        
        return cell;
    } else {
        MainTableViewCell *cell = (MainTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MainCell" forIndexPath:indexPath];
        
        [self configureSearchMainCell:cell atIndexPath:indexPath];
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isTableEmpty || areDealsUnavailable) {
        return 500;
    } else {
        return kMainTableRowHeight;
    }
}

#pragma mark - Cell methods

- (void)configureSearchMainCell:(MainTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    self.dealModel = [[Deal alloc] init];
    self.dealModel = self.searchResultsArray[indexPath.row];
    
    if ([self.dealModel.source isEqualToString:@"globe_rewards"]) {
        self.dealModel.originalPrice = @"Min. Globe Rewards";
        self.dealModel.discountedPrice = [NSString stringWithFormat:@"%@ Points", self.dealModel.minimumRewardsPoints];
        
        cell.originalPriceLabel.text = self.dealModel.originalPrice;
        cell.discountedPriceLabel.text = self.dealModel.discountedPrice;
    } else {
        self.dealModel.originalPrice = [[InfoManager sharedManager] formatStringToPrice:self.dealModel.originalPrice];
        self.dealModel.discountedPrice = [[InfoManager sharedManager] formatStringToPrice:self.dealModel.discountedPrice];
        
        cell.originalPriceLabel.attributedText = [[InfoManager sharedManager] formatOriginalPrice:self.dealModel.originalPrice withLineColor:[UIColor whiteColor]];
        cell.discountedPriceLabel.text = [NSString stringWithFormat:@"₱ %@", self.dealModel.discountedPrice];
    }
    
    
//    // removes gradient sublayer to prevent overlapping
//    cell.dealImage.layer.sublayers = nil;
    
    // create white image as placeholder image
    cell.dealImage.image = [[UIManager sharedManager] createWhitePlaceholderImageWithImage:cell.dealImage];
    cell.dealImage.tag = indexPath.row;
    
    [[APIManager sharedManager] loadImageWithImage:cell.dealImage andImageURL:self.dealModel.imageURL success:^(id image) {
        if ([image isKindOfClass:[UIImage class]]) {
            self.dealModel.cachedImage = image;
            cell.dealImage.image = self.dealModel.cachedImage;
        }
    } failure:^(NSError *error) {
        NSLog(@"Error loading image with error: %@", error);
    }];
    
    if ([self.dealModel.source isEqualToString:@"globe_rewards"]) {
        cell.discountLabel.hidden = YES;
        cell.ribbonImage.hidden = YES;
    } else {
        cell.discountLabel.hidden = NO;
        cell.ribbonImage.hidden = NO;
        cell.discountLabel.text = [NSString stringWithFormat:@"save %@%@", self.dealModel.discount, @"%"];
    }
    
    cell.titleLabel.text = self.dealModel.name;
    cell.subtitleLabel.text = self.dealModel.sourceText;
    
    // Animates the table cell
    dispatch_async(dispatch_get_main_queue(), ^{
        CALayer *animationLayer = cell.layer;
        animationLayer.transform = CATransform3DMakeTranslation(0, animationLayer.bounds.size.height/3, 0.0f);
        
        NSTimeInterval animationDuration = 0.5;
        [UIView beginAnimations:nil
                        context:nil];
        
        [UIView setAnimationDuration:animationDuration];
        cell.layer.transform = CATransform3DIdentity;
        cell.layer.opacity = 1.0f;
        
        [UIView commitAnimations];
    });
}

- (void)configureEmptyCell:(EmptyTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.titleLabel.text = kErrorNoInternet;
    
    // Animates the table cell
    dispatch_async(dispatch_get_main_queue(), ^{
        CALayer *animationLayer = cell.layer;
        animationLayer.transform = CATransform3DMakeTranslation(0, animationLayer.bounds.size.height/3, 0.0f);
        
        NSTimeInterval animationDuration = 0.5;
        [UIView beginAnimations:nil
                        context:nil];
        
        [UIView setAnimationDuration:animationDuration];
        cell.layer.transform = CATransform3DIdentity;
        cell.layer.opacity = 1.0f;
        
        [UIView commitAnimations];
    });
}

- (void)configureUnavailableCell:(UnavailableTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.titleLabel.text = kErrorNoDealsFound;
    
    // Animates the table cell
    dispatch_async(dispatch_get_main_queue(), ^{
        CALayer *animationLayer = cell.layer;
        animationLayer.transform = CATransform3DMakeTranslation(0, animationLayer.bounds.size.height/3, 0.0f);
        
        NSTimeInterval animationDuration = 0.5;
        [UIView beginAnimations:nil
                        context:nil];
        
        [UIView setAnimationDuration:animationDuration];
        cell.layer.transform = CATransform3DIdentity;
        cell.layer.opacity = 1.0f;
        
        [UIView commitAnimations];
    });
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"ShowSearchDealDetails"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        MainTableViewCell *cell = sender;
        
        self.dealModel = [[Deal alloc] init];
        self.dealModel = self.searchResultsArray[indexPath.row];
        self.dealModel.cachedImage = cell.dealImage.image;
        
        [segue.destinationViewController setDealModel:self.dealModel];
    }
}

@end
