//
//  DescriptionRewardsTableViewCell.h
//  GDeals
//
//  Created by Jay Santos on 2/4/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescriptionRewardsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *branchesButton;

@property (weak, nonatomic) IBOutlet UITextView *descriptionText;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;

@end
