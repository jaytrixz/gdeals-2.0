//
//  CityProvinceTableViewController.m
//  GDeals
//
//  Created by Jay Santos on 2/19/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "CityProvinceTableViewController.h"

#import "Constants.h"

@interface CityProvinceTableViewController ()

@property (strong, nonatomic) NSArray *manilaAreasArray;
@property (strong, nonatomic) NSArray *outsideManilaAreasArray;

@end

@implementation CityProvinceTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self configureView];
    
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    
}

- (void)customSetup {
    self.manilaAreasArray = @[@"Caloocan", @"Las Piñas", @"Makati", @"Malabon", @"Mandaluyong", @"Manila", @"Marikina", @"Muntinlupa", @"Navotas", @"Parañaque", @"Pasay", @"Pasig", @"Pateros", @"Quezon City", @"San Juan", @"Taguig", @"Valenzuela"];
    self.manilaAreasArray = [self.manilaAreasArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    self.outsideManilaAreasArray = @[@"Abra", @"Agusan del Norte", @"Agusan del Sur", @"Aklan", @"Albay", @"Antique", @"Apayao", @"Aurora", @"Basilan", @"Bataan", @"Batanes", @"Batangas", @"Benguet", @"Biliran", @"Bohol", @"Bukidnon", @"Bulacan", @"Cagayan", @"Camarines Norte", @"Camarines Sur", @"Camiguin" ,@"Capiz", @"Catanduanes", @"Cavite", @"Cebu", @"Compostela Valley", @"Cotabato", @"Davao del Norte", @"Davao del Sur", @"Davao Oriental", @"Dinagat Islands", @"Eastern Samar", @"Guimaras", @"Ifugao", @"Ilocos Norte", @"Iloilo", @"Isabela", @"Kalinga", @"La Union", @"Laguna", @"Lanao del Norte", @"Lanao del Sur", @"Leyte", @"Maguindanao", @"Marinduque", @"Masbate", @"Mindoro Occidental", @"Mindoro Oriental", @"Misamis Occidental", @"Mountain Province", @"Negros Occidental", @"Negros Oriental", @"Northern Samar", @"Nueva Ecija", @"Nueva Viscaya", @"Palawan", @"Pampanga", @"Pangasinan", @"Quezon", @"Quirino", @"Rizal", @"Romblon", @"Samar", @"Sarangani", @"Siquijor", @"Sorsogon", @"South Cotabato", @"Southern Leyte", @"Sultan Kudarat", @"Sulu" ,@"Surigao del Norte" ,@"Surigao del Sur", @"Tarlac", @"Tawi-Tawi", @"Zambales", @"Zamboanga del Norte", @"Zamboanga del Sur", @"Zamboanga Sibugay"];
    self.outsideManilaAreasArray = [self.outsideManilaAreasArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section == 0) {
        return [self.manilaAreasArray count];
    } else {
        return [self.outsideManilaAreasArray count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AreaCell" forIndexPath:indexPath];
    
    // Configure the cell...
    if (indexPath.section == 0) {
        cell.textLabel.text = self.manilaAreasArray[indexPath.row];
    } else {
        cell.textLabel.text = self.outsideManilaAreasArray[indexPath.row];
    }
    cell.textLabel.font = kGlobeFontRegular17;
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"Metro Manila";
    } else {
        return @"Outside Metro Manila";
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        [self.cityProvinceDelegate setCityProvinceWithCityProvince:self.manilaAreasArray[indexPath.row]];
    } else {
        [self.cityProvinceDelegate setCityProvinceWithCityProvince:self.outsideManilaAreasArray[indexPath.row]];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
