//
//  ThankYouViewController.h
//  GDeals
//
//  Created by Jay Santos on 2/5/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Deal.h"

@interface ThankYouViewController : UIViewController

@property (strong, nonatomic) Deal *dealModel;

@end
