//
//  Constants.h
//  GDeals
//
//  Created by Jay Santos on 10/15/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#ifndef GDeals_Constants_h
#define GDeals_Constants_h

// Demo accounts
#define kDemoAccount1                               @"09152711295"
#define kDemoAccount2                               @"09362575607"
#define kDemoAccountPin                             @"1234"

// Flurry
#define kFlurryAPIKey                               @"PY7HS49YQD3T7Y6B3GFW"

#define kFlurryTapped                               @"TAPPED"
#define kFlurryVerify                               @"VERIFY"
#define kFlurryParticipatingBranches                @"SEE PARTICIPATING BRANCHES"
#define kFlurryBuyNow                               @"BUY NOW"
#define kFlurryAddNew                               @"ADD NEW"
#define kFlurryCheckout                             @"CHECKOUT"
#define kFlurryDone                                 @"DONE"
#define kFlurrySave                                 @"SAVE"
#define kFlurryContinue                             @"CONTINUE"
#define kFlurryFBLogin                              @"FB Login"
#define kFlurryEnjoyShopping                        @"ENJOY SHOPPING"

#define kFlurryVerficationCode                      @"Verification Code"
#define kFlurryDealDetails                          @"Deal Details"
#define kFlurryDeliveryAddress                      @"Delivery Address"
#define kFlurryPreferences                          @"Preferences"
#define kFlurryAddNewDeliveryAddress                @"Add New Delivery Address"
#define kFlurryCartOkayOkay                         @"Cart OkayOkay"
#define kFlurryMobileRegistration                   @"Mobile Registration"
#define kFlurryUserRegistrationManualFB             @"User Registration Manual/FB"
#define kFlurryProfile                              @"Profile"

// Common Strings
#define kZeroString                                 @"0"
#define kEmptyString                                @""

// Links
#define kTermsAndConditionsLinkString               @"http://gdeals.ph/termsandconditions"
#define kMobileNumberLinkString                     @"http://serviceproxy.globelabsbeta.com/icu/header.php?mode=json"
//#define kBaseURLString                              @"http://staging.api.gdeals.ph/" // Staging
#define kBaseURLString                              @"https://api.gdeals.ph/" // Production
#define kSendVerificationCodeLinkString             kBaseURLString @"sms/v3/send_verification_code/"
#define kVerifyMobileNumberLinkString               kBaseURLString @"user/v2/verify_mobile_number/"
#define kSecuredSuccessURLString                    @"https://theshop.ph/deals/success/"
#define kSecuredOtherSuccessURLString               @"https://deals.theshop.ph/deals/success/"
#define kSuccessURLString                           @"http://theshop.ph/deals/success/"
#define kOtherSuccessURLString                      @"http://deals.theshop.ph/deals/success/"
#define kFailedURLString                            @"theshop.ph/deals/checkout/message?type=customer+cancel+transaction"

// Paths
#define kOAuthMobileNumberPath                      kBaseURLString @"oauth/v2/mobile_number/"
#define kRequestTokenPath                           kBaseURLString @"oauth/v2/request_token/"
#define kAccessTokenPath                            kBaseURLString @"oauth/v2/access_token/"
#define kSignupPath                                 kBaseURLString @"signup/v3/mobile_number/"
#define kAllTagsPath                                kBaseURLString @"user/v2/tags/"
#define kGetDeals                                   kBaseURLString @"feed/v4/deals/featured"
#define kGetBanners                                 kBaseURLString @"carousel/v2/carousel_valid_list/active"
#define kGetVouchers                                kBaseURLString @"deal/v2/redemption_summary"
#define kUpdateUserDetails                          kBaseURLString @"user/v2/update"
#define kGlobeRewardsPoints                         kBaseURLString @"user/v2/globe_reward_points/"
#define kCheckQRCodeInfo                            kBaseURLString @"merchant/v2/rewards_qr_code"
#define kUserAddress                                kBaseURLString @"user/v1/delivery_address/"
#define kSearchDeal                                 kBaseURLString @"feed/v1/search/deal"
#define kRewardsBranches                            kBaseURLString @"/feed/v1/participating_merchants"
#define kSubmitToOkayOkay                           kBaseURLString @"deal/v2/reserve_okayokay_sku/"
#define kGetDealInfo                                kBaseURLString @"deal/v2/detail"
#define kUpdatePushAccessTokens                     kBaseURLString @"/user/v2/device_token/"

// External Links
#define kFaqLink                                    @"http://gdeals.ph/faq"
#define kSupportEmail                               @"gdeals.support@globelabsbeta.com"

// Terms and Conditions
#define kTermsAndConditionsString                   @"By providing your mobile number you agree to GDeals Terms & Conditions."
#define kTermsAndConditionsHyperLinkString          @"GDeals Terms & Conditions"

// Encryption & Signature
#define kSHA1SecretKeyString                        @"gdeal.SHA1!"
#define kLettersAndNumbersString                    @"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

// Text Fields
#define kMaximumNumberForMobileNumber               11
#define kMaximumNumberForVerificationCode           4
#define kMaximumNumberForGlobeRewardsPoints         14

// Notifications
#define kFacebookLoginNotification                  @"FacebookLoginNotification"
#define kResetBannersAndDealsNotification           @"ResetBannersAndDeals"

// Alerts
#define kErrorTitle                                 @"Sorry"
#define kErrorDateTitle                             @"Invalid Date"
#define kErrorVCodeTitle                            @"Invalid Verification Code"
#define kErrorProfileTitle                          @"Incomplete Profile"

#define kErrorMessage                               @"Please check and try again."
#define kErrorNumberMessage                         @"No mobile number detected. " kErrorMessage
#define kErrorVerificationCodeMessage               @"Incorrect verification code. " kErrorMessage
#define kErrorInvalidVerificationCodeMessage        @"Invalid verification code. " kErrorMessage
#define kErrorProfileMessage                        @"Please make sure all fields aren't left empty and try again."
#define kErrorNoInternet                            @"Could not connect to remote server."
#define kErrorNoDealsFound                          @"No deals found."
#define kErrorNoVouchersFound                       @"No vouchers found."
#define kErrorNoAddressesFound                      @"No delivery address yet. Tap + to add one now."

#define kErrorFirstName                             @"First name field is empty. Please check and try again."
#define kErrorLastName                              @"Last name field is empty. Please check and try again."
#define kErrorNickname                              @"Nickname field is empty. Please check and try again."
#define kErrorBirthday                              @"Birthday field is empty. Please check and try again."

#define kErrorGlobeRewards                          @"Sorry, the points you used for the transaction did not meet the minimum points requirement. Please transact using an amount equal to or higher than the minimum points requirement."
#define kErrorInsufficientGlobeRewards              @"Sorry, you have insufficient Globe Rewards points."

#define kOK                                         @"OK"

// HUD Messages
#define kHUDLoading                                 @"Loading"
#define kHUDLoadingBanners                          @"Loading Banners"
#define kHUDVerifyingCode                           @"Verifying Code"
#define kHUDResendingCode                           @"Resending Code"
#define kHUDValidatingPoints                        @"Validating Points"
#define kHUDLoadingFAQ                              @"Loading FAQ"
#define kHUDLoadingInterests                        @"Loading Interests"
#define kHUDSavingPreferences                       @"Saving Preferences"
#define kHUDAddingAddress                           @"Adding Address"
#define kHUDVerifyingNumber                         @"Verifying Number"
#define kHUDRedeemingPoints                         @"Redeeming Points"
#define kHUDLoadingPreferences                      @"Loading Preferences"
#define kHUDRegisteringUser                         @"Registering User"
#define kHUDUpdatingProfile                         @"Updating Profile"
#define kHUDSearchingDeals                          @"Searching Deals"
#define kHUDLoadingVouchers                         @"Loading Vouchers"
#define kHUDVerifyingData                           @"Verifying Data"
#define kHUDUpdatingPreferences                     @"Updating Preferences"

// GDeals ID and Secret Keys
#define kGdealsClientID                             @"1f518e9cf7b48b4471b135fda4ac8607"
#define kGdealsClientSecret                         @"108a5da3a8d42b04edd3343b2321d350"

// Table rows
#define kMainBannerRowHeight                        190
#define kMainTableRowHeight                         202
#define kCategoryTableRowHeight                     202

// Colors
#define kGlobeBlueColor                             [UIColor colorWithRed:15/255.0 green:68/255.0 blue:160/255.0 alpha:1.0]
#define kGlobeLightBlueColor                        [UIColor colorWithRed:169/255.0 green:220/255.0 blue:247/255.0 alpha:1.0]

// Fonts
#define kGlobeFontBold15                            [UIFont fontWithName:@"FSElliotPro-Bold" size:15.0]

#define kGlobeFontRegular14                         [UIFont fontWithName:@"FSElliotPro" size:14.0]
#define kGlobeFontRegular15                         [UIFont fontWithName:@"FSElliotPro" size:15.0]
#define kGlobeFontRegular17                         [UIFont fontWithName:@"FSElliotPro" size:17.0]
#define kGlobeFontRegular18                         [UIFont fontWithName:@"FSElliotPro" size:18.0]

#endif
