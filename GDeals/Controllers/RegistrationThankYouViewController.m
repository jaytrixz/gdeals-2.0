//
//  RegistrationThankYouViewController.m
//  GDeals
//
//  Created by Jay Santos on 11/12/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import "RegistrationThankYouViewController.h"

#import "Flurry.h"
#import "Constants.h"
#import "UIManager.h"
#import "SWRevealViewController.h"
#import "MainTableViewController.h"

@interface RegistrationThankYouViewController ()

@property (weak, nonatomic) IBOutlet UIButton *enjoyShoppingButton;

@end

@implementation RegistrationThankYouViewController

//- (void)viewWillAppear:(BOOL)animated {
//    [self viewWillAppear:animated];
//    
//    [self configureView];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    self.navigationItem.titleView = [[UIManager sharedManager] formatNavigationBarWithLogoAndNavigationBar:self.navigationController.navigationBar andTitleText:@"GDeals"];
    self.navigationItem.hidesBackButton = YES;
    
    _enjoyShoppingButton.layer.cornerRadius = 10;
}

#pragma mark - Button methods

- (IBAction)enjoyShoppingButtonTapped:(UIButton *)sender {
    NSDictionary *detailParams = @{kFlurryTapped: kFlurryEnjoyShopping};
    [Flurry logEvent:kFlurryPreferences withParameters:detailParams];
    
    NSLog(@"Enjoy shopping button tapped");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"YES" forKey:@"UserIsLogged"];
    [defaults synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kResetBannersAndDealsNotification object:self];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
//    // Present main table view controller
//    SWRevealViewController *revealViewController = self.revealViewController;
//    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    
//    MainTableViewController *mainTableVC = [storyBoard instantiateViewControllerWithIdentifier:@"MainTableViewController"];
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mainTableVC];
//    [revealViewController setFrontViewController:navigationController animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
