//
//  APIManager.h
//  GDeals
//
//  Created by Jay Santos on 10/15/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface APIManager : NSObject {
    NSDictionary *jsonDictionary;
}

+ (instancetype)sharedManager;

- (void)getMobileNumber:(void (^)(NSString *mobileNumber))success
                failure:(void (^)(NSError *))failure;

- (void)verifyNumberWithMobileNumber:(NSString *)mobileNumber
                           signature:(NSString *)signature
           andVerificationCodeString:(NSString *)verificationCode
                             success:(void (^)(id responseObject))success
                             failure:(void (^)(NSError *))failure;

- (void)verifyCodeWithMobileNumber:(NSString *)mobileNumber
                           success:(void (^)(id responseObject))success
                           failure:(void (^)(NSError *))failure;

- (void)loginWithMobileNumber:(NSString *)mobileNumber
                      success:(void (^)(void))success
                      failure:(void (^)(NSError *error))failure;

- (void)registerMobileNumberWithParameters:(NSDictionary *)parameters
                                   success:(void (^)(void))success
                                   failure:(void (^)(NSError *error))failure;

- (void)getUserInterests:(void (^)(NSArray *userInterestsArray))success
                 failure:(void (^)(NSError *error))failure;

- (void)updateUserInterests:(NSString *)interests
                    success:(void (^)(void))success
                    failure:(void (^)(NSError *error))failure;

- (void)getBanners:(void (^)(NSDictionary *bannersDictionary))success
           failure:(void (^)(NSError *error))failure;

- (void)getDealsWithParameters:(NSDictionary *)parameters
                       success:(void (^)(NSDictionary *dealsDictionary))success
                       failure:(void (^)(NSError *error))failure;

- (void)loadImageWithImage:(id)imageView
               andImageURL:(NSString *)imageURLString
                   success:(void (^)(id image))success
                   failure:(void (^)(NSError *error))failure;

- (void)fetchVoucherDetails:(void (^)(NSArray *voucherDetailsArray))success
                    failure:(void (^)(NSError *error))failure;

- (void)updateUserDetailsWithParameters:(NSMutableDictionary *)parameters
                                success:(void (^)(NSString *result))success
                                failure:(void (^)(NSError *error))failure;

- (void)getGlobeRewardsPoints:(void (^)(NSString *points))success
                      failure:(void (^)(NSError *error))failure;

- (void)getQRCodeDetailsWithQRCode:(NSString *)qrCodeString
                           success:(void (^)(NSDictionary *infoDictionary))success
                           failure:(void (^)(NSError *error))failure;

- (void)getUserAddress:(void (^)(NSArray *addressArray))success
               failure:(void (^)(NSError *error))failure;

- (void)addNewUserAddressWithParameters:(NSDictionary *)parameters
                                success:(void (^)(NSString *result))success
                                failure:(void (^)(NSError *error))failure;

- (void)saveUserAddressWithParameters:(NSDictionary *)parameters
                              success:(void (^)(NSString *result))success
                              failure:(void (^)(NSError *error))failure;

- (void)useGlobeRewardsPointsWithParameters:(NSDictionary *)parameters
                                    success:(void (^)(NSString *result))success
                                    failure:(void (^)(NSError *error))failure;

- (void)getSeasonalBannerDealsWithParameters:(NSDictionary *)parameters
                                     success:(void (^)(NSDictionary *dealsDictionary))success
                                     failure:(void (^)(NSError *error))failure;

- (void)searchDealWithParameters:(NSDictionary *)parameters
                         success:(void (^)(NSDictionary *resultsDictionary))success
                         failure:(void (^)(NSError *error))failure;

- (void)getParticipatingBranchesWithParameters:(NSDictionary *)parameters
                                       success:(void (^)(NSDictionary *resultsDictionary))success
                                       failure:(void (^)(NSError *error))failure;

- (void)submitPaymentToOkayOkayWithParameters:(NSDictionary *)parameters
                                      success:(void (^)(NSString *HTMLString))success
                                      failure:(void (^)(NSError *error))failure;

- (void)getDealInfoWithDealIdentifier:(NSString *)identifier
                              success:(void (^)(NSDictionary *resultsDictionary))success
                              failure:(void (^)(NSError *error))failure;

- (void)updateAccessTokensWithParameters:(NSDictionary *)parameters
                                 success:(void (^)(void))success
                                 failure:(void (^)(NSError *error))failure;

@end
