//
//  RecoDealsTableViewController.m
//  GDeals
//
//  Created by Jay Santos on 2/10/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "RecoDealsTableViewController.h"

#import "Deal.h"
#import "UIManager.h"
#import "Constants.h"
#import "APIManager.h"
#import "InfoManager.h"
#import "MainTableViewCell.h"
#import "DealDetailsViewController.h"

@interface RecoDealsTableViewController ()

@property (strong, nonatomic) Deal *dealModel;

@property (strong, nonatomic) NSMutableArray *recoDealsArray;

@end

@implementation RecoDealsTableViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configureView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    
}

- (void)customSetup {
    self.recoDealsArray = [NSMutableArray array];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.recoDealsArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MainTableViewCell *cell = (MainTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MainCell" forIndexPath:indexPath];
    
    self.dealModel = [[Deal alloc] init];
    self.dealModel = self.recoDealsArray[indexPath.row];
    
    if ([self.dealModel.source isEqualToString:@"globe_rewards"]) {
        self.dealModel.originalPrice = @"Min. Globe Rewards";
        self.dealModel.discountedPrice = [NSString stringWithFormat:@"%@ Points", self.dealModel.minimumRewardsPoints];
        
        cell.originalPriceLabel.text = self.dealModel.originalPrice;
        cell.discountedPriceLabel.text = self.dealModel.discountedPrice;
    } else {
        self.dealModel.originalPrice = [[InfoManager sharedManager] formatStringToPrice:self.dealModel.originalPrice];
        self.dealModel.discountedPrice = [[InfoManager sharedManager] formatStringToPrice:self.dealModel.discountedPrice];
        
        cell.originalPriceLabel.attributedText = [[InfoManager sharedManager] formatOriginalPrice:self.dealModel.originalPrice withLineColor:[UIColor whiteColor]];
        cell.discountedPriceLabel.text = [NSString stringWithFormat:@"₱ %@", self.dealModel.discountedPrice];
    }
    
    
//        // removes gradient sublayer to prevent overlapping
//        cell.dealImage.layer.sublayers = nil;
    
    // create white image as placeholder image
    cell.dealImage.image = [[UIManager sharedManager] createWhitePlaceholderImageWithImage:cell.dealImage];
    cell.dealImage.tag = indexPath.row;
    
    [[APIManager sharedManager] loadImageWithImage:cell.dealImage andImageURL:self.dealModel.imageURL success:^(id image) {
        if ([image isKindOfClass:[UIImage class]]) {
            self.dealModel.cachedImage = image;
            cell.dealImage.image = self.dealModel.cachedImage;
        }
    } failure:^(NSError *error) {
        NSLog(@"Error loading image with error: %@", error);
    }];
    
    if ([self.dealModel.source isEqualToString:@"globe_rewards"]) {
        cell.discountLabel.hidden = YES;
        cell.ribbonImage.hidden = YES;
    } else {
        cell.discountLabel.hidden = NO;
        cell.ribbonImage.hidden = NO;
        cell.discountLabel.text = [NSString stringWithFormat:@"save %@%@", self.dealModel.discount, @"%"];
    }
    
    cell.titleLabel.text = self.dealModel.name;
    cell.subtitleLabel.text = self.dealModel.sourceText;
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"ShowDealDetails"]) {
        
    }
}

@end
