//
//  InfoManager.m
//  GDeals
//
//  Created by Jay Santos on 11/6/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import "InfoManager.h"

#import "Constants.h"
#import "CoreDataManager.h"

@implementation InfoManager

+ (instancetype)sharedManager {
    __strong static InfoManager *sharedManager = nil;
    static dispatch_once_t token = 0;
    dispatch_once(&token, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

- (NSString *)createSHA1UsingData:(id)data {
    NSString *secretKey = nil;
    secretKey = kSHA1SecretKeyString;
    
    NSString *rawString = nil;
    rawString = kEmptyString;
    
    NSString *hash = nil;
    
    if ([data isKindOfClass:[NSMutableDictionary class]] ||
        [data isKindOfClass:[NSDictionary class]]) {
        NSArray *keysArray = nil;
        keysArray = [[data allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        NSLog(@"Keys array: %@", keysArray);
        
        for (NSString *key in keysArray) {
            rawString = [rawString stringByAppendingFormat:@"%@", [data objectForKey:key]];
        }
        
        NSLog(@"Raw string: %@", rawString);
        hash = [self encryptStringWithHMACSHA1:rawString withKey:secretKey];
    } else if ([data isKindOfClass:[NSString class]]) {
        const char *cStr = [data UTF8String];
        unsigned char result[CC_SHA1_DIGEST_LENGTH];
        CC_SHA1(cStr, strlen(cStr), result);
        NSString *s = [NSString stringWithFormat:
                       @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                       result[0], result[1], result[2], result[3], result[4],
                       result[5], result[6], result[7],
                       result[8], result[9], result[10], result[11], result[12],
                       result[13], result[14], result[15],
                       result[16], result[17], result[18], result[19]
                       ];
        hash = s;
    }
    
    NSLog(@"Hash: %@", hash);
    return hash;
}

- (NSString *)createVerificationCodeWithNumberOfCharacters:(NSInteger)numberOfCharacters {
    NSString *lettersAndNumbers = nil;
    lettersAndNumbers = kLettersAndNumbersString;
    
    NSMutableString *verificationCodeString = nil;
    verificationCodeString = [NSMutableString stringWithCapacity:numberOfCharacters];
    
    for (int i = 0; i < numberOfCharacters; i++) {
        [verificationCodeString appendFormat:@"%C", [lettersAndNumbers characterAtIndex:arc4random() % [lettersAndNumbers length]]];
    }
    
    NSLog(@"Verification code: %@", verificationCodeString);
    return verificationCodeString;
}

- (NSString *)encryptStringWithHMACSHA1:(NSString *)dataString
                                withKey:(NSString *)keyString {
    const char *cKey = nil;
    cKey  = [keyString cStringUsingEncoding:NSUTF8StringEncoding];
    
    const char *cData = nil;
    cData = [dataString cStringUsingEncoding:NSUTF8StringEncoding];
    
    unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    
    NSMutableString *output = nil;
    output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", cHMAC[i]];
    }
    
    NSLog(@"Encrypted string: %@", output);
    return [NSString stringWithFormat:@"%@", output];
}

- (NSMutableArray *)getDefaultInterests {
    NSMutableArray *defaultInterestsArray = [NSMutableArray arrayWithArray:@[@"fashion", @"home", @"electronics", @"automobile", @"lifestyle", @"beauty", @"dining", @"travel", @"kids"]];
    
    return defaultInterestsArray;
}

- (NSString *)removeZeroFromMobileNumber:(NSString *)numberString {
    if ([numberString hasPrefix:@"0"] && [numberString length] > 1) {
        numberString = [numberString substringFromIndex:1];
    }
    
    return numberString;
}

- (NSString *)formatImageString:(NSString *)imageString {
    if ([imageString rangeOfString:@"_thumb"].location != NSNotFound) {
        return [imageString stringByReplacingOccurrencesOfString:@"_thumb" withString:@"_large"];
    } else {
        return imageString;
    }
}

- (NSAttributedString *)formatOriginalPrice:(NSString *)priceString
                              withLineColor:(UIColor *)color {
    return [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"₱ %@", priceString] attributes:@{NSStrikethroughStyleAttributeName: @(NSUnderlineStyleSingle), NSStrikethroughColorAttributeName:color}];
}

- (NSMutableArray *)setupDealsArrayWithDealDictionary:(NSDictionary *)dealDictionary {
    NSMutableArray *dealsArray = [NSMutableArray array];
    
    NSArray *rawDealsArray = [NSArray array];
    rawDealsArray = dealDictionary[@"deals"];
    
    for (NSDictionary *rawDealDictionary in rawDealsArray) {
        Deal *dealModel = [[Deal alloc] init];
        dealModel.branchName = rawDealDictionary[@"branch_name"];
        dealModel.categoryId = rawDealDictionary[@"category_id"];
        dealModel.categoryIdNew = rawDealDictionary[@"category_id_new"];
        dealModel.companyName = rawDealDictionary[@"company_name"];
        dealModel.sourceText = rawDealDictionary[@"data"][@"deal_source_text"];
        dealModel.minimumRewardsPoints = rawDealDictionary[@"data"][@"deal_min_rewards_points"];
        
        NSString *rawDescriptionText = rawDealDictionary[@"data"][@"description"];
        if (rawDescriptionText != (id)[NSNull null]) {
            dealModel.descriptionText = [self removeDotsFromHTMLWithText:rawDescriptionText];
        } else {
            dealModel.descriptionText = @"";
        }
        
        dealModel.perkPoints = rawDealDictionary[@"data"][@"perk_points"];
        dealModel.pointsExpiry = rawDealDictionary[@"data"][@"points_expiry"];
        dealModel.purchaseCount = rawDealDictionary[@"data"][@"purchase_count"];
        
        NSString *rawTermsText = rawDealDictionary[@"data"][@"terms"];
        if (rawTermsText != (id)[NSNull null]) {
            dealModel.terms = [self removeDotsFromHTMLWithText:rawTermsText];
        } else {
            dealModel.terms = @"No terms and conditions. Please contact the deal owner.";
        }
        
        dealModel.endDate = rawDealDictionary[@"deal_end_date"];
        dealModel.startDate = rawDealDictionary[@"deal_start_date"];
        dealModel.discount = rawDealDictionary[@"discount_percentage"];
        dealModel.discountedPrice = rawDealDictionary[@"discounted_price"];
        dealModel.identifier = rawDealDictionary[@"id"];
        dealModel.latitude = rawDealDictionary[@"latitude"];
        dealModel.longitude = rawDealDictionary[@"longitude"];
        dealModel.merchantId = rawDealDictionary[@"merchant_id"];
        dealModel.imageURL = rawDealDictionary[@"image"];
        dealModel.name = rawDealDictionary[@"name"];
        
        if (dealModel.endRedemption == (id)[NSNull null]) {
            dealModel.endRedemption = @"";
        } else {
            dealModel.endRedemption = rawDealDictionary[@"redemption_end"];
        }
        
        if (dealModel.startRedemption == (id)[NSNull null]) {
            dealModel.startRedemption = @"";
        } else {
            dealModel.startRedemption = rawDealDictionary[@"redemption_start"];
        }
        
        dealModel.originalPrice = rawDealDictionary[@"regular_price"];
        dealModel.source = rawDealDictionary[@"source"];
        dealModel.subVariantsDictionary = rawDealDictionary[@"sub_variants"];
        dealModel.variantsDictionary = rawDealDictionary[@"variants"];
        
        [dealsArray addObject:dealModel];
    }
    
    return dealsArray;
}

- (NSString *)convertUnixTimeToString:(CGFloat)unixTime {
    NSString *timeString = nil;
    
    if (unixTime) {
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:unixTime];
        NSDateFormatter *timeFormatter= [[NSDateFormatter alloc] init];
        timeFormatter.locale = [NSLocale currentLocale];
        timeFormatter.dateFormat = @"MMM dd, yyyy";
        
        timeString = [timeFormatter stringFromDate:date];
    }
    
    return timeString;
}

- (NSString *)convertCategoryIDToString:(NSString *)categoryID {
    return @"";
}

// Private methods
- (NSString *)removeDotsFromHTMLWithText:(NSString *)rawHtmlText {
    rawHtmlText = [rawHtmlText stringByReplacingOccurrencesOfString:@"<li>" withString:@""];
    rawHtmlText = [rawHtmlText stringByReplacingOccurrencesOfString:@"</li>" withString:@""];
    
    return rawHtmlText;
}

- (NSString *)getCategoryIdentifierWithCategoryName:(NSString *)categoryName {
    NSArray *categoriesArray = [[CoreDataManager sharedManager] fetchCategories];
    NSString *categoryIdentifier = nil;
    
    for (NSDictionary *categoryDictionary in categoriesArray) {
        if ([categoryDictionary[@"name"] isEqualToString:categoryName]) {
            categoryIdentifier = categoryDictionary[@"id"];
            
            break;
        }
    }
    
    return categoryIdentifier;
}

- (NSString *)getCategoryNameWithCategoryIdentifier:(NSString *)categoryIdentifier {
    NSArray *categoriesArray = [[CoreDataManager sharedManager] fetchCategories];
    NSString *categoryName = nil;
    
//    NSLog(@"categoryIdentifier: %@", categoryIdentifier);
//    NSLog(@"categoriesArray: %@", categoriesArray);
    for (NSDictionary *categoryDictionary in categoriesArray) {
        if ([categoryDictionary[@"id"] isEqualToString:categoryIdentifier]) {
            categoryName = categoryDictionary[@"name"];
            
            break;
        }
    }
    
    return categoryName;
}

- (NSString *)createSignatureWithSHA1WithDictionary:(NSDictionary *)detailDictionary {
    
    NSString *signatureString = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", detailDictionary[@"access_token"], detailDictionary[@"birth_date"], detailDictionary[@"fname"], detailDictionary[@"gender"], detailDictionary[@"lname"], detailDictionary[@"nickname"], detailDictionary[@"user_id"]];
    signatureString = [[InfoManager sharedManager] createSHA1UsingData:signatureString];
    
    return signatureString;
}

- (NSString *)getCurrentDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale currentLocale];
    dateFormatter.dateFormat = @"LLL dd, yyyy hh:mm a";
    
    return [dateFormatter stringFromDate:[NSDate date]];
}

- (NSString *)computeTotalAmountWithPrice:(NSString *)price
                              andQuantity:(NSString *)qty {
    CGFloat totalAmount = [price floatValue] * [qty floatValue];
    
    return [NSString stringWithFormat:@"%.0f", totalAmount];
}

- (NSString *)formatStringToPrice:(NSString *)price {
    NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    return [priceFormatter stringFromNumber:[priceFormatter numberFromString:price]];
}

- (Deal *)setupDealModelWithDealDictionary:(NSDictionary *)dealDictionary {
    Deal *dealModel = [[Deal alloc] init];
    dealModel.branchName = dealDictionary[@"branch_name"];
    dealModel.categoryId = dealDictionary[@"category_id"];
    dealModel.categoryIdNew = dealDictionary[@"category_id_new"];
    dealModel.companyName = dealDictionary[@"company_name"];
    dealModel.sourceText = dealDictionary[@"data"][@"deal_source_text"];
    dealModel.minimumRewardsPoints = dealDictionary[@"data"][@"deal_min_rewards_points"];
    
    NSString *rawDescriptionText = dealDictionary[@"data"][@"description"];
    if (rawDescriptionText != (id)[NSNull null]) {
        dealModel.descriptionText = [self removeDotsFromHTMLWithText:rawDescriptionText];
    } else {
        dealModel.descriptionText = @"";
    }
    
    dealModel.perkPoints = dealDictionary[@"data"][@"perk_points"];
    dealModel.pointsExpiry = dealDictionary[@"data"][@"points_expiry"];
    dealModel.purchaseCount = dealDictionary[@"data"][@"purchase_count"];
    
    NSString *rawTermsText = dealDictionary[@"data"][@"terms"];
    if (rawTermsText != (id)[NSNull null]) {
        dealModel.terms = [self removeDotsFromHTMLWithText:rawTermsText];
    } else {
        dealModel.terms = @"No terms and conditions. Please contact the deal owner.";
    }
    
    dealModel.endDate = dealDictionary[@"deal_end_date"];
    dealModel.startDate = dealDictionary[@"deal_start_date"];
    dealModel.discount = dealDictionary[@"discount_percentage"];
    dealModel.discountedPrice = dealDictionary[@"discounted_price"];
    dealModel.identifier = dealDictionary[@"id"];
    dealModel.latitude = dealDictionary[@"latitude"];
    dealModel.longitude = dealDictionary[@"longitude"];
    dealModel.merchantId = dealDictionary[@"merchant_id"];
    
    if (dealDictionary[@"image_carousel"]) {
        dealModel.bannerImageURL = dealDictionary[@"image_carousel"];
    }
    dealModel.imageURL = dealDictionary[@"image"];
    dealModel.name = dealDictionary[@"name"];
    
    if (dealModel.endRedemption == (id)[NSNull null]) {
        dealModel.endRedemption = @"";
    } else {
        dealModel.endRedemption = dealDictionary[@"redemption_end"];
    }
    
    if (dealModel.startRedemption == (id)[NSNull null]) {
        dealModel.startRedemption = @"";
    } else {
        dealModel.startRedemption = dealDictionary[@"redemption_start"];
    }
    
    dealModel.originalPrice = dealDictionary[@"regular_price"];
    dealModel.source = dealDictionary[@"source"];
    dealModel.subVariantsDictionary = dealDictionary[@"sub_variants"];
    dealModel.variantsDictionary = dealDictionary[@"variants"];
    
    return dealModel;
}

- (Banner *)setupBannerModelWithBannerDictionary:(NSDictionary *)bannerDictionary {
    Banner *bannerModel = [[Banner alloc] init];
    
    if (bannerDictionary) {
        bannerModel.identifier = bannerDictionary[@"id"];
        
        bannerModel.imageURL = bannerDictionary[@"image"];
        bannerModel.name = bannerDictionary[@"name"];
    } else {
        bannerModel.identifier = @"";
        bannerModel.imageURL = @"none";
        bannerModel.name = @"";
    }
    
    return bannerModel;
}

@end
