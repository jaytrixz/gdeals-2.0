//
//  InterestsInputViewController.m
//  GDeals
//
//  Created by Jay Santos on 10/15/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import "InterestsInputViewController.h"

#import "Flurry.h"
#import "Constants.h"
#import "UIManager.h"
#import "APIManager.h"
#import "InfoManager.h"
#import "SVProgressHUD.h"
#import "InterestCollectionViewCell.h"

@interface InterestsInputViewController ()

@property (strong, nonatomic) NSArray *interestsArray;
@property (strong, nonatomic) NSArray *interestNormalImagesArray;
@property (strong, nonatomic) NSArray *interestSelectedImagesArray;

@property (strong, nonatomic) NSMutableArray *defaultInterestsArray;
@property (strong, nonatomic) NSMutableArray *updatedInterestsArray;

@property (weak, nonatomic) IBOutlet UICollectionView *interestsCollection;

@end

@implementation InterestsInputViewController

static NSString * const reuseIdentifier = @"InterestCell";

//- (void)viewWillAppear:(BOOL)animated {
//    [self viewWillAppear:animated];
//    
//    [self configureView];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configureView];
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    self.navigationItem.titleView = [[UIManager sharedManager] formatNavigationBarWithLogoAndNavigationBar:self.navigationController.navigationBar andTitleText:@"Preferences"];
    self.navigationItem.hidesBackButton = YES;
}

- (void)customSetup {
    self.interestsArray = [[UIManager sharedManager] loadDefaultInterests];
    self.interestNormalImagesArray = [[UIManager sharedManager] loadInterestsNormalButtonImages];
    self.interestSelectedImagesArray = [[UIManager sharedManager] loadInterestsSelectedButtonImages];
    
    self.updatedInterestsArray = [NSMutableArray array];
    
    [SVProgressHUD setFont:kGlobeFontRegular14];
    [SVProgressHUD setForegroundColor:kGlobeBlueColor];
    [SVProgressHUD showWithStatus:kHUDLoadingInterests maskType:SVProgressHUDMaskTypeGradient];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        self.defaultInterestsArray = [[InfoManager sharedManager] getDefaultInterests];
        [[APIManager sharedManager] getUserInterests:^(NSArray *userInterestsArray) {
            for (NSString *defaultInterestName in userInterestsArray) {
                if ([self.defaultInterestsArray containsObject:defaultInterestName]) {
                    [self.updatedInterestsArray addObject:defaultInterestName];
                }
            }
            
            [_interestsCollection reloadData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
            });
            
            NSLog(@"Error getting user interests with error: %@", error);
        }];
    });
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)updateSelectedInterests {
    NSString *joinedInterests = [_updatedInterestsArray componentsJoinedByString:@","];
    
    [[APIManager sharedManager] updateUserInterests:joinedInterests success:^{
        // Do nothing
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        
        [self performSegueWithIdentifier:@"ShowRegistrationThankYouView" sender:nil];
    } failure:^(NSError *error) {
        NSLog(@"Error updating user interests with error: %@", error);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
    }];
}

#pragma mark - Button methods

- (IBAction)doneButtonTapped:(UIBarButtonItem *)sender {
    NSDictionary *detailParams = @{kFlurryTapped: kFlurryDone};
    [Flurry logEvent:kFlurryPreferences withParameters:detailParams];
    
    [SVProgressHUD setFont:kGlobeFontRegular14];
    [SVProgressHUD setForegroundColor:kGlobeBlueColor];
    [SVProgressHUD showWithStatus:kHUDSavingPreferences maskType:SVProgressHUDMaskTypeGradient];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self updateSelectedInterests];
    });
}

- (void)interestButtonTapped:(UIButton *)button {
    NSIndexPath *indexPath = [_interestsCollection indexPathForCell:(UICollectionViewCell *)[button superview]];
   
    if (button.selected) {
        button.selected = NO;
        
        NSLog(@"%@ interest is not tapped", _interestsArray[indexPath.row]);
        [self.updatedInterestsArray removeObject:[_interestsArray[indexPath.row] lowercaseString]];
    } else {
        button.selected = YES;
        
        NSLog(@"%@ interest is tapped", _interestsArray[indexPath.row]);
        [self.updatedInterestsArray addObject:[_interestsArray[indexPath.row] lowercaseString]];
    }
}

#pragma mark - UICollectionView methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_interestsArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    InterestCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    cell.interestButton.layer.cornerRadius = 45;
    cell.interestButton.clipsToBounds = YES;
    
    NSLog(@"Loaded interest: %@", _interestNormalImagesArray[indexPath.row]);
    [cell.interestButton setBackgroundImage:[UIImage imageNamed:_interestNormalImagesArray[indexPath.row]] forState:UIControlStateNormal];
    [cell.interestButton setBackgroundImage:[UIImage imageNamed:_interestSelectedImagesArray[indexPath.row]] forState:UIControlStateHighlighted];
    [cell.interestButton setBackgroundImage:[UIImage imageNamed:_interestSelectedImagesArray[indexPath.row]] forState:UIControlStateSelected];
    
    [cell.interestButton addTarget:self action:@selector(interestButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if ([_updatedInterestsArray containsObject:_defaultInterestsArray[indexPath.row]]) {
        cell.interestButton.selected = YES;
    } else {
        cell.interestButton.selected = NO;
    }
    
    return cell;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
