//
//  ThankYouViewController.m
//  GDeals
//
//  Created by Jay Santos on 2/5/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "ThankYouViewController.h"

#import "Constants.h"
#import "UIManager.h"
#import "APIManager.h"
#import "InfoManager.h"
#import "RecoCollectionViewCell.h"
#import "DealDetailsViewController.h"

@interface ThankYouViewController ()

@property (strong, nonatomic) Deal *recoDealModel;

@property (strong, nonatomic) NSMutableArray *recoDealsArray;

@property (weak, nonatomic) IBOutlet UIButton *enjoyShoppingButton;

@property (weak, nonatomic) IBOutlet UITextView *creditCardText;
@property (weak, nonatomic) IBOutlet UITextView *onlineBankingText;

@property (weak, nonatomic) IBOutlet UICollectionView *recoCollection;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;

@end

@implementation ThankYouViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configureView];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    self.enjoyShoppingButton.layer.cornerRadius = 10;
}

- (void)customSetup {
    self.recoDealsArray = [NSMutableArray array];
    
    [self formatTextViews];
    
    [self loadRecoDeals];
}

- (void)formatTextViews {
    NSString *creditCardTextString = self.creditCardText.text;
    NSString *onlineTextString = self.onlineBankingText.text;
    NSString *otherCreditCardTextString = @" please go to Vouchers in the main menu to access details of your order.";
    NSString *otherOnlineTextString = @" please wait for your payment to reflect within 24 hours.";
    
    NSRange allCreditCardRange = [otherCreditCardTextString rangeOfString:otherCreditCardTextString];
    NSRange allOnlineRange = [otherOnlineTextString rangeOfString:otherOnlineTextString];
    NSRange cardRangeBold = [creditCardTextString rangeOfString:@"For credit card purchase,"];
    NSRange onlineRangeBold = [onlineTextString rangeOfString:@"For Online Banking and Bank Transfer,"];
    
    UIFont *regularFont = kGlobeFontRegular15;
    UIFont *fontText = kGlobeFontBold15;
    NSDictionary *dictionaryFontText = [NSDictionary dictionaryWithObjectsAndKeys:regularFont, NSFontAttributeName, nil];
    NSDictionary *dictionaryBoldText = [NSDictionary dictionaryWithObjectsAndKeys:fontText, NSFontAttributeName, nil];
    
    NSMutableAttributedString *modifiedCardAttributedString = [[NSMutableAttributedString alloc] initWithString:creditCardTextString];
    [modifiedCardAttributedString setAttributes:dictionaryFontText range:allCreditCardRange];
    [modifiedCardAttributedString setAttributes:dictionaryBoldText range:cardRangeBold];
    
    NSMutableAttributedString *modifiedOnlineAttributedString = [[NSMutableAttributedString alloc] initWithString:onlineTextString];
    [modifiedOnlineAttributedString setAttributes:dictionaryFontText range:allOnlineRange];
    [modifiedOnlineAttributedString setAttributes:dictionaryBoldText range:onlineRangeBold];
    
    [self.creditCardText setAttributedText:modifiedCardAttributedString];
    [self.onlineBankingText setAttributedText:modifiedOnlineAttributedString];
}

- (void)loadRecoDeals {
    if ([self.recoDealsArray count] == 0) {
        [self.activityLoader startAnimating];
        
        NSDictionary *parameters = [NSDictionary dictionary];
        if (self.dealModel.categoryId) {
            if (self.dealModel.categoryIdNew) {
                parameters = @{@"action": @"00", @"page_legend": @"DD", @"strip_html_tags": @"1", @"source_page": @"MV", @"deal_id": self.dealModel.identifier, @"category_id": self.dealModel.categoryId, @"category_id_new": self.dealModel.categoryIdNew};
            } else {
                parameters = @{@"action": @"00", @"page_legend": @"DD", @"strip_html_tags": @"1", @"source_page": @"MV", @"deal_id": self.dealModel.identifier, @"category_id": self.dealModel.categoryId};
            }
        } else {
            parameters = @{@"action": @"00", @"page_legend": @"DD", @"strip_html_tags": @"1", @"source_page": @"MV", @"deal_id": self.dealModel.identifier, @"category_id_new": self.dealModel.categoryIdNew};
        }
        
        [[APIManager sharedManager] getDealsWithParameters:parameters success:^(NSDictionary *dealsDictionary) {
            NSLog(@"dealsDictionary: %@", dealsDictionary);
            
            self.recoDealsArray = [[InfoManager sharedManager] setupDealsArrayWithDealDictionary:dealsDictionary];
            [self.recoCollection reloadData];
            
            [self.activityLoader stopAnimating];
        } failure:^(NSError *error) {
            NSLog(@"Error fetching reco deals with error: %@", error);
            
            [self.activityLoader stopAnimating];
            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
//            });
        }];
    }
}

#pragma mark - Button methods

- (IBAction)enjoyShoppingButtonTapped:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Collection view methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.recoDealsArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RecoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RecoCollectionCell" forIndexPath:indexPath];
    
    self.recoDealModel = [[Deal alloc] init];
    self.recoDealModel = self.recoDealsArray[indexPath.row];
    
//    NSLog(@"Image URL: %@", self.recoDealModel.imageURL);
    
    // create white image as placeholder image
    cell.dealImage.image = [[UIManager sharedManager] createWhitePlaceholderImageWithImage:cell.dealImage];
    cell.dealImage.tag = indexPath.row;
    
    [[APIManager sharedManager] loadImageWithImage:cell.dealImage andImageURL:self.recoDealModel.imageURL success:^(id image) {
        if ([image isKindOfClass:[UIImage class]]) {
            self.recoDealModel.cachedImage = image;
            cell.dealImage.image = self.recoDealModel.cachedImage;
        }
    } failure:^(NSError *error) {
        NSLog(@"Error loading image with error: %@", error);
    }];
    
    cell.titleLabel.text = self.recoDealModel.name;
    if ([self.recoDealModel.source isEqualToString:@"globe_rewards"]) {
        cell.originalPriceLabel.text = @"Min. Globe Rewards";
        cell.discountedPriceLabel.text = [NSString stringWithFormat:@"%@ Points", self.recoDealModel.minimumRewardsPoints];
    } else {
        cell.originalPriceLabel.text = [[InfoManager sharedManager] formatStringToPrice:self.recoDealModel.originalPrice];
        cell.originalPriceLabel.attributedText = [[InfoManager sharedManager] formatOriginalPrice:cell.originalPriceLabel.text withLineColor:[UIColor blackColor]];
        cell.discountedPriceLabel.text = [[InfoManager sharedManager] formatStringToPrice:self.recoDealModel.discountedPrice];
        cell.discountedPriceLabel.text = [NSString stringWithFormat:@"₱ %@", cell.discountedPriceLabel.text];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    // this pushes another instance of deal details view with the updated deal model and adds it to the navigation controller stack so the user can still go back anytime
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DealDetailsViewController *dealDetailsVC = [storyboard instantiateViewControllerWithIdentifier:@"DealDetailsView"];
    dealDetailsVC.dealModel = self.recoDealsArray[indexPath.row];
    [self.navigationController pushViewController:dealDetailsVC animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
