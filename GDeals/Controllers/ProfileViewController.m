//
//  ProfileViewController.m
//  GDeals
//
//  Created by Jay Santos on 1/29/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "ProfileViewController.h"

#import "Flurry.h"
#import "Constants.h"
#import "UIManager.h"
#import "APIManager.h"
#import "InfoManager.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"
#import "CoreDataManager.h"

@interface ProfileViewController () {
    UITextField *activeTextField;
}

@property (weak, nonatomic) IBOutlet UILabel *numberLabel;

@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *nickNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *birthdayTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;

@property (weak, nonatomic) IBOutlet UIScrollView *profileScrollView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuBarButtonItem;

@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSegment;

@property (strong, nonatomic) UIDatePicker *birthdayDatePicker;

@property (strong, nonatomic) NSDictionary *userDictionary;
@property (strong, nonatomic) NSMutableDictionary *userDetailsDictionary;

@end

@implementation ProfileViewController

- (void)viewWillAppear:(BOOL)animated {
    [self registerForKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self deregisterFromKeyboardNotifications];
    
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configureView];
    
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    self.navigationItem.titleView = [[UIManager sharedManager] formatNavigationBarWithNavigationBar:self.navigationController.navigationBar andTitleText:self.navigationController.title];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.delegate = self;
    if (revealViewController) {
        [_menuBarButtonItem setTarget:self.revealViewController];
        [_menuBarButtonItem setAction:@selector(revealToggle:)];
        [self.navigationController.navigationBar addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

- (void)customSetup {
    UITapGestureRecognizer *tapToHideKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard:)];
    [self.view addGestureRecognizer:tapToHideKeyboardGesture];
    
    _birthdayDatePicker = [[UIDatePicker alloc] init];
    _birthdayDatePicker.datePickerMode = UIDatePickerModeDate;
    [_birthdayDatePicker addTarget:self action:@selector(birthdaySelectionFinished:) forControlEvents:UIControlEventValueChanged];
    self.birthdayTextField.inputView = _birthdayDatePicker;
    
    [self getUserDetails];
}

#pragma mark - Custom methods

- (void)getUserDetails {
    // get essential user details from user in core data
    self.userDictionary = [[CoreDataManager sharedManager] fetchUserDetails];
    NSLog(@"self.userDictionary: %@", self.userDictionary);
    
    // get number from current user in core data
    self.userDetailsDictionary = [NSMutableDictionary dictionary];
    self.userDetailsDictionary = [[[CoreDataManager sharedManager] fetchCurrentUserDictionary] mutableCopy];
    self.userDetailsDictionary = self.userDetailsDictionary[@"result"][@"user"];
    NSLog(@"self.userDetailsDictionary: %@", self.userDetailsDictionary);
    
    self.numberLabel.text = [NSString stringWithFormat:@"%@%@", @"0", self.userDetailsDictionary[@"mobile_no"]];
    
    if ([self.userDictionary count] == 0) {
        // get all updated values from user details dictionary
        self.firstNameTextField.text = self.userDetailsDictionary[@"fname"];
        self.lastNameTextField.text = self.userDetailsDictionary[@"lname"];
        self.nickNameTextField.text = self.userDetailsDictionary[@"nickname"];
        self.birthdayTextField.text = self.userDetailsDictionary[@"birth_date"];
        
        if ([self.userDetailsDictionary[@"gender"] isEqualToString:@"male"]) {
            self.genderSegment.selectedSegmentIndex = 0;
        } else {
            self.genderSegment.selectedSegmentIndex = 1;
        }
    } else {
        // get all updated values from user dictionary, not from user details dictionary
        self.firstNameTextField.text = self.userDictionary[@"fname"];
        self.lastNameTextField.text = self.userDictionary[@"lname"];
        self.nickNameTextField.text = self.userDictionary[@"nickname"];
        self.birthdayTextField.text = self.userDictionary[@"birth_date"];
        
        if ([self.userDictionary[@"gender"] isEqualToString:@"male"]) {
            self.genderSegment.selectedSegmentIndex = 0;
        } else {
            self.genderSegment.selectedSegmentIndex = 1;
        }
    }
    
}

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)hideKeyboard:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    
    return context;
}

- (void)birthdaySelectionFinished:(UIDatePicker *)picker {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    if ([picker.date compare:[NSDate date]] == NSOrderedDescending || [picker.date compare:[NSDate date]] == NSOrderedSame) {
        [[[UIAlertView alloc] initWithTitle:kErrorDateTitle message:kErrorMessage delegate:self cancelButtonTitle:nil otherButtonTitles:kOK, nil] show];
    } else {
        _birthdayTextField.text = [dateFormatter stringFromDate:picker.date];
    }
}

#pragma mark - Button methods

- (IBAction)genderSegmentTapped:(UISegmentedControl *)sender {
    switch (self.genderSegment.selectedSegmentIndex) {
        case 0:
            break;
        case 1:
            break;
        default:
            break;
    }
}

- (IBAction)saveButtonTapped:(UIBarButtonItem *)sender {
    NSDictionary *detailParams = @{kFlurryTapped: kFlurrySave};
    [Flurry logEvent:kFlurryProfile withParameters:detailParams];
    
    [SVProgressHUD setFont:kGlobeFontRegular14];
    [SVProgressHUD setForegroundColor:kGlobeBlueColor];
    [SVProgressHUD showWithStatus:kHUDUpdatingProfile maskType:SVProgressHUDMaskTypeGradient];
    
    [self hideKeyboard:nil];
    
    NSLog(@"First name: %@", _firstNameTextField.text);
    NSLog(@"Last name: %@", _lastNameTextField.text);
    NSLog(@"Nickname: %@", _nickNameTextField.text);
    NSLog(@"Birthday: %@", _birthdayTextField.text);
    
    if ([_firstNameTextField.text isEqualToString:@""]) {
        [SVProgressHUD showErrorWithStatus:@"First name field is empty. Please check and try again." maskType:SVProgressHUDMaskTypeGradient];
        return;
    }
    
    if ([_lastNameTextField.text isEqualToString:@""]) {
        [SVProgressHUD showErrorWithStatus:@"Last name field is empty. Please check and try again." maskType:SVProgressHUDMaskTypeGradient];
        return;
    }
    
    if ([_nickNameTextField.text isEqualToString:@""]) {
        [SVProgressHUD showErrorWithStatus:@"Nickname field is empty. Please check and try again." maskType:SVProgressHUDMaskTypeGradient];
        return;
    }
    
    if ([_birthdayTextField.text isEqualToString:@""]) {
        [SVProgressHUD showErrorWithStatus:@"Birthday field is empty. Please check and try again." maskType:SVProgressHUDMaskTypeGradient];
        return;
    }
    
    if (![_firstNameTextField.text isEqualToString:@""] && ![_lastNameTextField.text isEqualToString:@""] && ![_nickNameTextField.text isEqualToString:@""] && ![_birthdayTextField.text isEqualToString:@""]) {
        
        NSString *genderString = nil;
        if (self.genderSegment.selectedSegmentIndex == 0) {
            genderString = @"male";
        } else {
            genderString = @"female";
        }
        [[CoreDataManager sharedManager] saveUserDetailsWithFirstName:_firstNameTextField.text andLastName:_lastNameTextField.text andNickname:_nickNameTextField.text andBirthday:_birthdayTextField.text andGender:genderString];
        
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        [parameters setObject:[[InfoManager sharedManager] removeZeroFromMobileNumber:self.numberLabel.text] forKey:@"mobile_number"];
        [parameters setObject:self.firstNameTextField.text forKey:@"fname"];
        [parameters setObject:self.lastNameTextField.text forKey:@"lname"];
        [parameters setObject:self.nickNameTextField.text forKey:@"nickname"];
        [parameters setObject:self.birthdayTextField.text forKey:@"birth_date"];
        [parameters setObject:genderString forKey:@"gender"];
        [parameters setObject:[[CoreDataManager sharedManager] fetchCurrentUserAccessToken] forKey:@"access_token"];
        [parameters setObject:self.userDetailsDictionary[@"id"] forKey:@"user_id"];
        
        NSString *signatureString = [[InfoManager sharedManager] createSignatureWithSHA1WithDictionary:parameters];
        [parameters setObject:signatureString forKey:@"signature"];
        
        [[APIManager sharedManager] updateUserDetailsWithParameters:parameters success:^(NSString *result) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showSuccessWithStatus:@"Profile Saved" maskType:SVProgressHUDMaskTypeGradient];
            });
        } failure:^(NSError *error) {
            NSLog(@"Error updating user details with error: %@", error);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
            });
        }];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:kErrorProfileMessage maskType:SVProgressHUDMaskTypeGradient];
        });
    }
}

#pragma mark - UITextField methods

- (void)keyboardWasShown:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect bkgndRect = activeTextField.superview.frame;
    bkgndRect.size.height += kbSize.height;
    [activeTextField.superview setFrame:bkgndRect];
    
    CGFloat adjustedHeight = self.navigationController.navigationBar.frame.size.height + self.numberLabel.frame.size.height;
    
    [self.profileScrollView setContentOffset:CGPointMake(0.0, activeTextField.frame.origin.y + adjustedHeight - kbSize.height) animated:YES];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification *)aNotification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.profileScrollView.contentInset = contentInsets;
    self.profileScrollView.scrollIndicatorInsets = contentInsets;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    activeTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    activeTextField = nil;
}

#pragma mark - SWRevealViewController methods

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:    (FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:    (FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
