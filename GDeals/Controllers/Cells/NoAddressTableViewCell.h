//
//  NoAddressTableViewCell.h
//  GDeals
//
//  Created by Jay Santos on 2/25/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoAddressTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
