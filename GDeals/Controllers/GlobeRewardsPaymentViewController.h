//
//  GlobeRewardsPaymentViewController.h
//  GDeals
//
//  Created by Jay Santos on 2/5/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Deal.h"

@interface GlobeRewardsPaymentViewController : UIViewController

@property (strong, nonatomic) Deal *dealModel;

@property (strong, nonatomic) NSString *latitudeString;
@property (strong, nonatomic) NSString *longitudeString;

@end
