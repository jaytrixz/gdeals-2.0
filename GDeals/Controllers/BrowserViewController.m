//
//  BrowserViewController.m
//  GDeals
//
//  Created by Jay Santos on 2/5/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import "BrowserViewController.h"

#import "Constants.h"
#import "UIManager.h"
#import "APIManager.h"
#import "SVProgressHUD.h"
#import "CoreDataManager.h"
#import "ThankYouViewController.h"
#import "GlobeRewardsThankYouViewController.h"

@interface BrowserViewController () {
    BOOL isSuccessURLLoaded;
}

@property (weak, nonatomic) IBOutlet UIWebView *browserWebView;

@end

@implementation BrowserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configureView];
    
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom methods

- (void)configureView {
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

- (void)customSetup {
    [SVProgressHUD showWithStatus:kHUDLoading maskType:SVProgressHUDMaskTypeGradient];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self getPaymentCheckoutHTML];
    });
}

- (void)getPaymentCheckoutHTML {
    if ([self.dealModel.source isEqualToString:@"okayokay"]) {
        NSLog(@"self.paymentModel.thirdPartyDealIdentifier: %@", self.paymentModel.thirdPartyDealIdentifier);
        
        NSDictionary *parameters = @{@"access_token": [[CoreDataManager sharedManager] fetchCurrentUserAccessToken], @"delivery_address_id": self.paymentModel.deliveryAddressIdentifier, @"payment_type": @"credit", @"price": self.paymentModel.price, @"quantity": self.paymentModel.quantity, @"third_party_deal_id": self.paymentModel.thirdPartyDealIdentifier, @"deal_id": self.dealModel.identifier};
        [[APIManager sharedManager] submitPaymentToOkayOkayWithParameters:parameters success:^(NSString *HTMLString) {
            NSLog(@"HTML: %@", HTMLString);
            
            [self.browserWebView loadHTMLString:HTMLString baseURL:nil];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        } failure:^(NSError *error) {
            NSLog(@"Error getting payment checkout HTML with error: %@", error);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus:[error localizedDescription] maskType:SVProgressHUDMaskTypeGradient];
            });
        }];
    } else {
        
    }
}

#pragma mark - Webview methods 

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [SVProgressHUD showWithStatus:kHUDLoading maskType:SVProgressHUDMaskTypeGradient];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [SVProgressHUD dismiss];
    
    NSString *currentURL = self.browserWebView.request.URL.absoluteString;
    
    NSLog(@"Current URL: %@", currentURL);
    
    if ([currentURL rangeOfString:kSuccessURLString].location != NSNotFound || [currentURL rangeOfString:kOtherSuccessURLString].location != NSNotFound || [currentURL rangeOfString:kSecuredSuccessURLString].location != NSNotFound || [currentURL rangeOfString:kSecuredOtherSuccessURLString].location != NSNotFound) {
        
        [webView stopLoading];
        
        isSuccessURLLoaded = YES;
        
        // this prevents the segue from advancing twice
        if (isSuccessURLLoaded) {
            [self performSegueWithIdentifier:@"ShowThankYouView" sender:nil];
            
            isSuccessURLLoaded = NO;
        }
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"Failed to load webpage with error: %@", error);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"ShowThankYouView"]) {
        [segue.destinationViewController setDealModel:self.dealModel];
    }
}

@end
