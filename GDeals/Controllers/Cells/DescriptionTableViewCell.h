//
//  DescriptionTableViewCell.h
//  GDeals
//
//  Created by Jay Santos on 1/22/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescriptionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextView *descriptionText;

@end
