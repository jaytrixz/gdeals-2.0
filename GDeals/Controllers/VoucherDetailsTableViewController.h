//
//  VoucherDetailsTableViewController.h
//  GDeals
//
//  Created by Jay Santos on 1/29/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoucherDetailsTableViewController : UITableViewController

@property (strong, nonatomic) NSArray *nodesArray;

@end
