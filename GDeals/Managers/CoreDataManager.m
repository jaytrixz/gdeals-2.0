//
//  CoreDataManager.m
//  GDeals
//
//  Created by Jay Santos on 11/25/14.
//  Copyright (c) 2014 Globe Telecom, Inc. All rights reserved.
//

#import "CoreDataManager.h"

@implementation CoreDataManager

+ (instancetype)sharedManager {
    __strong static CoreDataManager *sharedManager = nil;
    static dispatch_once_t token = 0;
    dispatch_once(&token, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

- (NSManagedObjectContext *)managedObjectContext {
    context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    
    return context;
}

- (NSManagedObjectModel *)managedObjectModel {
    model = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectModel)]) {
        model = [delegate managedObjectModel];
    }
    
    return model;
}

- (void)saveCurrentUserWithUserDictionary:(NSDictionary *)currentUser {
    context = [self managedObjectContext];
    
    NSManagedObject *currentUserObject = [NSEntityDescription insertNewObjectForEntityForName:@"CurrentUser" inManagedObjectContext:context];
    NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:currentUser];
    [currentUserObject setValue:userData forKey:@"user"];
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Error saving to core data stack with error: %@ %@", error, [error localizedDescription]);
    }
}

- (void)saveAccessTokenWithAccessToken:(NSString *)accessToken {
    context = [self managedObjectContext];
    
    NSManagedObject *currentUserObject = [NSEntityDescription insertNewObjectForEntityForName:@"CurrentUser" inManagedObjectContext:context];
    [currentUserObject setValue:accessToken forKey:@"access_token"];
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Error saving to core data stack with error: %@ %@", error, [error localizedDescription]);
    }
}

- (NSDictionary *)fetchCurrentUserDictionary {
    context = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"CurrentUser"];
    NSArray *resultsArray = [context executeFetchRequest:fetchRequest error:nil];
    
    NSDictionary *userDictionary = nil;
    for (NSManagedObject *object in resultsArray) {
        NSData *userData = [object valueForKey:@"user"];
        if (userData) {
            userDictionary = (NSDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:userData];
        }
        
        break;
    }
    
    return userDictionary;
}

- (NSString *)fetchCurrentUserAccessToken {
    context = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"CurrentUser"];
    NSArray *resultsArray = [context executeFetchRequest:fetchRequest error:nil];
    
    NSString *accessTokenString = nil;
    for (NSManagedObject *object in resultsArray) {
        accessTokenString = [object valueForKey:@"access_token"];
    }
    
    return accessTokenString;
}

- (NSString *)fetchClientVersion {
    context = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Settings"];
    NSArray *resultsArray = [context executeFetchRequest:fetchRequest error:nil];
    
    NSString *clientVersionString = nil;
    for (NSManagedObject *object in resultsArray) {
        clientVersionString = [object valueForKey:@"version"];
        
        if (clientVersionString) {
            break;
        }
    }
    
    return clientVersionString;
}

- (void)saveClientVersionToSettingsWithClientVersionString:(NSString *)clientVersionString {
    context = [self managedObjectContext];
    
    NSManagedObject *settingsObject = [NSEntityDescription insertNewObjectForEntityForName:@"Settings" inManagedObjectContext:context];
    [settingsObject setValue:clientVersionString forKey:@"version"];
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Error saving to core data stack with error: %@ %@", error, [error localizedDescription]);
    }
}

- (void)saveCategoriesToSettingsWithCategoriesArray:(NSArray *)categoriesArray {
    context = [self managedObjectContext];
    
    NSManagedObject *categoriesObject = [NSEntityDescription insertNewObjectForEntityForName:@"Settings" inManagedObjectContext:context];
    NSData *categoriesData = [NSKeyedArchiver archivedDataWithRootObject:categoriesArray];
    [categoriesObject setValue:categoriesData forKey:@"categories"];
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Error saving to core data stack with error: %@", error);
    }
}

- (NSArray *)fetchCategories {
    context = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Settings"];
    NSArray *resultsArray = [context executeFetchRequest:fetchRequest error:nil];
    
    NSArray *categoriesArray = nil;
    for (NSManagedObject *object in resultsArray) {
        NSData *categoriesData = [object valueForKey:@"categories"];
        if (categoriesData) {
            categoriesArray = (NSArray *)[NSKeyedUnarchiver unarchiveObjectWithData:categoriesData];
            
            break;
        }
    }
    
    NSLog(@"categoriesArray: %@", categoriesArray);
    return categoriesArray;
}

- (NSDictionary *)fetchUserDetails {
    NSDictionary *userDetailsDictionary = [NSDictionary dictionary];
    
    NSString *firstNameString = nil;
    NSString *lastNameString = nil;
    NSString *nickNameString = nil;
    NSString *birthdayString = nil;
    NSString *genderString = nil;
    
    context = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"User"];
    NSArray *resultsArray = [context executeFetchRequest:fetchRequest error:nil];
    
    NSString *userDetailString = nil;
    for (NSManagedObject *object in resultsArray) {
        userDetailString = [object valueForKey:@"firstname"];
        
        if (userDetailString) {
            firstNameString = userDetailString;
            break;
        }
    }
    
    for (NSManagedObject *object in resultsArray) {
        userDetailString = [object valueForKey:@"lastname"];
        
        if (userDetailString) {
            lastNameString = userDetailString;
            break;
        }
    }
    
    for (NSManagedObject *object in resultsArray) {
        userDetailString = [object valueForKey:@"nickname"];
        
        if (userDetailString) {
            nickNameString = userDetailString;
            break;
        }
    }
    
    for (NSManagedObject *object in resultsArray) {
        userDetailString = [object valueForKey:@"birthday"];
        
        if (userDetailString) {
            birthdayString = userDetailString;
            break;
        }
    }
    
    for (NSManagedObject *object in resultsArray) {
        userDetailString = [object valueForKey:@"gender"];
        
        if (userDetailString) {
            genderString = userDetailString;
            break;
        }
    }
    
    if (firstNameString && lastNameString && nickNameString && birthdayString && genderString) {
        userDetailsDictionary = @{@"fname": firstNameString, @"lname": lastNameString, @"nickname": nickNameString, @"birth_date": birthdayString, @"gender": genderString};
        
        return userDetailsDictionary;
    } else {
        return nil;
    }
}

- (void)saveUserDetailsWithFirstName:(NSString *)firstName
                         andLastName:(NSString *)lastName
                         andNickname:(NSString *)nickName
                         andBirthday:(NSString *)birthday
                           andGender:(NSString *)gender {
    context = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"User"];
    NSArray *resultsArray = [context executeFetchRequest:fetchRequest error:nil];
    
    if ([resultsArray count] != 0) {
        for (NSManagedObject *object in resultsArray) {
            [object setValue:firstName forKey:@"firstname"];
            [object setValue:lastName forKey:@"lastname"];
            [object setValue:nickName forKey:@"nickname"];
            [object setValue:birthday forKey:@"birthday"];
            [object setValue:gender forKey:@"gender"];
        }
    } else {
        NSManagedObject *userObject = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
        [userObject setValue:firstName forKey:@"firstname"];
        [userObject setValue:lastName forKey:@"lastname"];
        [userObject setValue:nickName forKey:@"nickname"];
        [userObject setValue:birthday forKey:@"birthday"];
        [userObject setValue:gender forKey:@"gender"];
    }
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Error saving user details with error %@", error);
    }
}

- (void)deleteAllObjectsInCoreData {
    model = [self managedObjectModel];
    context = [self managedObjectContext];
    
    NSArray *allEntities = model.entities;
    for (NSEntityDescription *entityDescription in allEntities) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entityDescription];
        
        fetchRequest.includesPropertyValues = NO;
        fetchRequest.includesSubentities = NO;
        
        NSError *error;
        NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
        
        if (error) {
            NSLog(@"Error requesting items from Core Data: %@", [error localizedDescription]);
        }
        
        for (NSManagedObject *managedObject in items) {
            [context deleteObject:managedObject];
        }
        
        if (![context save:&error]) {
            NSLog(@"Error deleting %@ - error:%@", entityDescription, [error localizedDescription]);
        }
    }  
}

@end
