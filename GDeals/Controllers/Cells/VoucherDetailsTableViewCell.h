//
//  VoucherDetailsTableViewCell.h
//  GDeals
//
//  Created by Jay Santos on 1/29/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoucherDetailsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *validityView;
@property (weak, nonatomic) IBOutlet UIView *purchasedView;
@property (weak, nonatomic) IBOutlet UIView *redemptionView;

@property (weak, nonatomic) IBOutlet UILabel *codeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *validityLabel;

@property (weak, nonatomic) IBOutlet UITextView *termsText;

@property (weak, nonatomic) IBOutlet UIImageView *dealImage;

@end
