//
//  DealDetailsViewController.h
//  GDeals
//
//  Created by Jay Santos on 1/23/15.
//  Copyright (c) 2015 Globe Telecom, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Deal.h"

@interface DealDetailsViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) Deal *dealModel;

@end
